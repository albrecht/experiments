#include "pico/stdlib.h"
#include <stdio.h>
#include <tusb.h>

#ifndef PICO_DEFAULT_LED_PIN
#warning blink example requires a board with a regular LED
#else
#define LED_PIN PICO_DEFAULT_LED_PIN
#endif

int main() {
    int counter = 0;
    static uint32_t ledState = 0;
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    stdio_init_all(); // Init stdio
    while (!tud_cdc_connected()) {
        printf(".");
        sleep_ms(500);
    }
    // USB takes a bit to startup, so we need to wait for it to come online
    while (!stdio_usb_connected()) {
        printf("-");
        sleep_ms(500);
    }
    printf("\nInitialization complete :)\n");

    while (true) {
        sleep_ms(1000);
        printf("Counter = %d\n", counter);
        counter++;

        ledState = !ledState;
        gpio_put(LED_PIN, ledState);
    }
    return 0;
}
