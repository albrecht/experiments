add_executable(blink-usb
        blink-usb.c
        )

# pull in common dependencies
target_link_libraries(blink-usb pico_stdlib)
add_compile_definitions(PICO_TIME_DEFAULT_ALARM_POOL_DISABLED=0) # Should be default but just to make sure

# Communications setup
pico_enable_stdio_usb(blink-usb 1)
pico_enable_stdio_uart(blink-usb 1)

# create map/bin/hex file etc.
pico_add_extra_outputs(blink-usb)

# add url via pico_set_program_url
example_auto_set_url(blink-usb)
