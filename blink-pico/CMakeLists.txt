cmake_minimum_required(VERSION 3.12)

# Pull in SDK (must be before project)
include(pico_sdk_import.cmake)

include(pico_extras_import_optional.cmake)

project(pico_examples C CXX ASM)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

if (PICO_SDK_VERSION_STRING VERSION_LESS "1.3.0")
    message(FATAL_ERROR "Raspberry Pi Pico SDK version 1.3.0 (or later) required. Your version is ${PICO_SDK_VERSION_STRING}")
endif ()

set(PICO_EXAMPLES_PATH ${PROJECT_SOURCE_DIR})

# Initialize the SDK
pico_sdk_init()

include(example_auto_set_url.cmake)
# Add blink example
add_subdirectory(blink)
# Add blink with serial example
add_subdirectory("blink-uart")
# Add blink with serial via usb example
add_subdirectory("blink-usb")
# Add partial driver to reset and activate the CYW43439
add_subdirectory("CYW43_partial_driver")
# Add interrupt testing projects
add_subdirectory("blink-int")
add_subdirectory("irq-test-sleep")
add_subdirectory("blink-button")
add_subdirectory("dma-simple")
add_subdirectory("dma-repeating")
add_subdirectory("soft-int")

add_custom_target(myall)
add_dependencies(myall blink blink-uart blink_int irq-test-sleep blink-button blink-usb dma-simple dma-repeating soft-int)

