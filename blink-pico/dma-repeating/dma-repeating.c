// Acknowledgement: Based on DMA example of the SDK

#include <stdio.h>
#include "pico/stdlib.h"
#include <string.h>
#include "hardware/dma.h"
#include "hardware/structs/uart.h"

#ifndef uart_default
#error dma/control_blocks example requires a UART
#endif

int dma_chan;
#define BUF_MAX 64
char *stringBuffer = NULL;
int counter = 0;

int64_t dma_reset_handler(alarm_id_t id, void *user_data) {
    printf("DMA resetting...\n");
    snprintf(stringBuffer, BUF_MAX, ">> Current counter value = %d\n\r", counter++);
    dma_channel_set_trans_count(dma_chan, strnlen(stringBuffer, BUF_MAX), false);
    dma_channel_set_read_addr(dma_chan, stringBuffer, true);
    return 0;
}

void dma_handler() {
    // Clear the interrupt request.
    dma_hw->ints0 = 1u << dma_chan;
    printf("DMA complete -- Scheduling dma-reset in 1 second.\n\n");
    add_alarm_in_ms(1000, &dma_reset_handler, stringBuffer, true);
}

int main() {
    stdio_init_all();
    printf("========================== Boot complete ============================\n");
    printf("DMA repeating uart example starting...\n");

    char stringBuf[BUF_MAX] = "This will be update by the reaction to the callback\n";
    stringBuffer = stringBuf;
    int stringBufLen = strnlen(stringBuf, sizeof(stringBuf));

    // Configure a channel to write the same word (32 bits) repeatedly to PIO0
    // SM0's TX FIFO, paced by the data request signal from that peripheral.
    dma_chan = dma_claim_unused_channel(true);
    dma_channel_config c = dma_channel_get_default_config(dma_chan);
    channel_config_set_transfer_data_size(&c, DMA_SIZE_8);
    channel_config_set_read_increment(&c, true);
    channel_config_set_dreq(&c, uart_get_dreq(uart_default, true));

    dma_channel_configure(
            dma_chan,
            &c,
            &uart_get_hw(uart_default)->dr,
            NULL,             // Don't provide a read address yet
            0, // Write the same value many times, then halt and interrupt
            false             // Don't start yet
    );

    // Tell the DMA to raise IRQ line 0 when the channel finishes a block
    dma_channel_set_irq0_enabled(dma_chan, true);

    // Configure the processor to run dma_handler() when DMA IRQ 0 is asserted
    irq_set_exclusive_handler(DMA_IRQ_0, dma_handler);
    irq_set_enabled(DMA_IRQ_0, true);

    // Manually call the handler once, to trigger the first transfer
    dma_reset_handler(0, stringBuf);

    // Everything else from this point is interrupt-driven. The processor has
    // time to sit and think about its early retirement -- maybe open a bakery?
    while (true)
        tight_loop_contents();

}

