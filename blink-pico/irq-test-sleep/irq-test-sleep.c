#include "pico/stdlib.h"
#include <stdio.h>

#ifndef PICO_DEFAULT_LED_PIN
#error blink example requires a board with a regular LED
#endif

#define LED_PIN PICO_DEFAULT_LED_PIN



int main() {
//    volatile int always_false_for_debug = false;
    uint32_t ledState = 0;
    int counter = 0;
    stdio_init_all(); // Init stdio

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    while (true) {
        sleep_ms(1000);
        printf("Counter = %d\n", counter);
        counter++;

        ledState = !ledState;
        gpio_put(LED_PIN, ledState);
    }
    return 0;
}
