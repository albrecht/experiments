add_executable(irq-test-sleep
        irq-test-sleep.c
        )

# pull in common dependencies
target_link_libraries(irq-test-sleep pico_stdlib)

add_compile_definitions(PICO_TIME_DEFAULT_ALARM_POOL_DISABLED=0) # Should be default but just to make sure

# Communications setup
pico_enable_stdio_usb(irq-test-sleep 0)
pico_enable_stdio_uart(irq-test-sleep 1)

# create map/bin/hex file etc.
pico_add_extra_outputs(irq-test-sleep)
