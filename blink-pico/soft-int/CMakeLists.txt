add_executable(soft-int
        soft-int.c
        )

# pull in common dependencies
target_link_libraries(soft-int pico_stdlib)

# Communications setup
pico_enable_stdio_usb(soft-int 0)
pico_enable_stdio_uart(soft-int 1)

# create map/bin/hex file etc.
pico_add_extra_outputs(soft-int)
