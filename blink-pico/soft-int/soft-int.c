#include "pico/stdlib.h"
#include <stdio.h>

#ifndef PICO_DEFAULT_LED_PIN
#error blink example requires a board with a regular LED
#endif

#define LED_PIN PICO_DEFAULT_LED_PIN


static uint32_t ledState = 0;
static uint32_t low_priority_irq_num = 0;

void my_task() {
    ledState = !ledState;
    gpio_put(LED_PIN, ledState);
}

bool my_time_callback(struct repeating_timer *timer) {
    irq_set_pending(low_priority_irq_num);
    return true;
}

int main() {
    int counter = 0;
    stdio_init_all();
    printf("========================== Boot complete ============================\n");

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    struct repeating_timer timer;
    add_repeating_timer_ms(1000, &my_time_callback, NULL, &timer);

    low_priority_irq_num = (uint8_t) user_irq_claim_unused(true);
    irq_set_exclusive_handler(low_priority_irq_num, my_task);
    irq_set_enabled(low_priority_irq_num, true);
    printf("Init complete\n");

    while (true) {
        printf("Counter = %d\n", counter++);
        sleep_ms(5000);
    }
    return 0;
}
