# Pico base firmware samples

These are the basic example firmware samples using the internal peripherals of the RP2040.

## Structure
* `blink` -- Standard blink example but with busy wait -> no interrupts
* `blink-button` -- Blinks LED based on GPIO button interrupt
* `blink-uart` -- Standard blink example but with normal wait -> Timer interrupt
* `blink-usb` -- Standard blink example but with normal wait and USB console -> Timer interrupt + USB stack interrupts
* `dma-{repeating, simple}` -- DMA test cases
* `soft-int` -- Software interrupt example

## To Build

### Requirements:

* [Raspberry Pi Pico SDK](https://github.com/raspberrypi/pico-sdk) 
* Note: sometimes `libstdc++-arm-none-eabi-newlib` needs to be installed additionally

### Build instructions

```shell
$ mkdir build
$ cd build
build/ $ cmake ..
build/ $ make
```