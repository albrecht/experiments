#include "pico/stdlib.h"
#include <stdio.h>

#ifndef PICO_DEFAULT_LED_PIN
#error blink example requires a board with a regular LED
#endif

#define LED_PIN PICO_DEFAULT_LED_PIN
#define IRQ_PIN 22
#define  GPIO_IRQ_EVENT GPIO_IRQ_EDGE_FALL

static uint32_t ledState = 0;

int64_t debounce_callback(alarm_id_t id, void *user_data){
    gpio_set_irq_enabled(IRQ_PIN, GPIO_IRQ_EVENT, true);
    irq_set_enabled(IO_IRQ_BANK0, true);
    return 0;
}

void gpio_callback() {
    irq_set_enabled(IO_IRQ_BANK0, false);
    gpio_set_irq_enabled(IRQ_PIN, GPIO_IRQ_EVENT, false);
    ledState = !ledState;
    gpio_put(LED_PIN, ledState);
    printf("HIT\n");
    add_alarm_in_ms(500, &debounce_callback, NULL, true);
}

int main() {
//    volatile int always_false_for_debug = false;
    int counter = 0;
    stdio_init_all(); // Init stdio

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    // Init IRQ pin
    gpio_init(IRQ_PIN);
    gpio_set_dir(IRQ_PIN, GPIO_IN);
    gpio_pull_up(IRQ_PIN);
    gpio_set_irq_enabled_with_callback(IRQ_PIN, GPIO_IRQ_EVENT, true, &gpio_callback);


    while (true) {
        sleep_ms(1000);
        printf("Counter = %d\n", counter);
        counter++;
    }
    return 0;
}
