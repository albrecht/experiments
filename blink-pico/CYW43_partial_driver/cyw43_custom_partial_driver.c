#include "pico/stdlib.h"
#include <stdio.h>

#define WL_REG_ON 10
#ifndef PICO_DEFAULT_LED_PIN
#error This program requires a board with a regular LED
#endif
#define LED_PIN PICO_DEFAULT_LED_PIN

// Initialise our gpios
void cyw43_spi_gpio_setup(void) {
    // Setup WL_REG_ON (23)
    gpio_init(WL_REG_ON);
    gpio_set_dir(WL_REG_ON, GPIO_OUT);
    gpio_pull_up(WL_REG_ON);

    gpio_init(WL_REG_ON);
    gpio_set_dir(WL_REG_ON, GPIO_OUT);

    // Setup status LED
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

}

// Reset wifi chip
void cyw43_spi_reset(void) {
    gpio_put(WL_REG_ON, false); // off
    sleep_ms(20);
    gpio_put(WL_REG_ON, true); // on
    sleep_ms(250);
}

int main() {
    cyw43_spi_gpio_setup();
    gpio_put(LED_PIN, 1);
    stdio_init_all();
    printf("Initialized \n\n");
    sleep_ms(1000); // Wait for the CYW43 to power up (might be unnecessary but just to be sure)

    gpio_put(LED_PIN, 0);
    printf("Resetting CYW43439...\n");
    cyw43_spi_reset();
    printf("Reset of CYW43439 complete!\n");
    gpio_put(LED_PIN, 1);

    while (true) {
        sleep_ms(1000);
    }
}