
#include "pico/stdlib.h"

#ifndef PICO_DEFAULT_LED_PIN
#error blink example requires a board with a regular LED
#endif
#define LED_PIN PICO_DEFAULT_LED_PIN

int main()
{
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    while (true)
    {
        gpio_put(LED_PIN, 1);
        busy_wait_ms(500);
        // sleep_ms(750);
        gpio_put(LED_PIN, 0);
        busy_wait_ms(500);
        // sleep_ms(750);
    }
    return 0;
}
