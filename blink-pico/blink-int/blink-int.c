#include "pico/stdlib.h"
#include <stdio.h>

#ifndef PICO_DEFAULT_LED_PIN
#error blink example requires a board with a regular LED
#endif

#define LED_PIN PICO_DEFAULT_LED_PIN
#define IRQ_PIN 22


static uint32_t ledState = 0;

void gpio_callback() {
    ledState = !ledState;
    gpio_put(LED_PIN, ledState);
    printf("HIT\n");
}

//static void __not_in_flash_func(debug_stub_for_testing)(void) {
////void debug_stub_for_testing() {
//    asm(
//            "dcscr:   .word 0xe000edf0\n"
//            "outstat: .word 0x00000000\n"
//            "writeme: .word 0x00000000\n"
//
//            "stub_init:\n"   // Load the addresses for later access
//            "ldr r1, =dcscr\n"
//            "ldr r2, =outstat\n"
//            "ldr r3, =writeme\n"
//            "mov r4, #1\n"   // Signal Avatar that the stub initialized
//            "str r4, [r2]\n"
//
//            "loop: b loop\n"  // Wait for something to happen
//            "nop\n"
//
//            "stub_stub:\n"
//            "mov r4, #2\n"   // Signal Avatar that there has been an interrupt
//            "str r4, [r2]\n"
//
//            "intloop:\n"   // Hang in a loop until `writeme` is not 0
//            "ldr r4, [r3]\n"
//            "cmp r4, #0\n"
//            "beq intloop\n"
//
//            "mov r4, #0\n"
//            "str r4, [r3]\n"   // Reset `writeme`
//            "str r4, [r2]\n"   // Reset `outstat`
//            "bx lr\n"  // return to caller ($lr register)
//            );
//}

bool my_time_callback(struct repeating_timer* timer){
    ledState = !ledState;
    gpio_put(LED_PIN, ledState);
    return true;
}

int main() {
    int counter = 0;
    stdio_init_all(); // Init stdio

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    // Init IRQ pin
    gpio_init(IRQ_PIN);
    gpio_set_dir(IRQ_PIN, GPIO_IN);
    gpio_pull_up(IRQ_PIN);
    gpio_set_irq_enabled_with_callback(IRQ_PIN, GPIO_IRQ_EDGE_FALL, true, &gpio_callback);

    struct repeating_timer timer;
    add_repeating_timer_ms(1000, &my_time_callback, NULL, &timer);

    while (true) {
        sleep_ms(1000);
        printf("Counter = %d\n", counter);
        counter++;
    }
    return 0;
}
