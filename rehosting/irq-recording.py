from datetime import datetime

import IPython
import argparse
import ast

from avatar2 import *
from common import util
from common.ipython_startup import *

VECTOR_TABLE_OFFSET_REG = 0xe000ed08

HARD_FAULT_ISR = 0x100001c4


def main(firmware=None, openocd_cfg=None, after_init=0, irq_map={}, save=True, elf_file=None):
    # Configure the location of config and binary files
    firmware = abspath(firmware)
    openocd_config = abspath(openocd_cfg)

    # Initiate the avatar-object
    avatar = Avatar(arch=ARM_CORTEX_M3,
                    output_directory='/tmp/avatar', log_to_stdout=False)
    loggerAvatar, logger = util.setup_loggers()

    logger.info("Setting up targets")
    # Create the target-objects
    hardware = avatar.add_target(
        OpenOCDTarget, openocd_script=openocd_config, name='hardware', binary=elf_file if elf_file else None)

    logger.info("Loading interrupt plugins")
    avatar.load_plugin('arm.armv7m_interupt_recorder')
    avatar.load_plugin('assembler')
    avatar.load_plugin('disassembler')

    # Memory mapping from https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf#_address_map
    # 2MB Flash, Accessing using XIP with cache
    xip_cached = avatar.add_memory_range(
        0x10000000, 0x00200000, file=firmware, name='Flash (XIP) - cache')

    ram = avatar.add_memory_range(
        0x20000000, 0x00042000, name='RAM')  # 264kB of RAM

    # Internal peripherals of the Cortex M0+, esp VTOR
    arm_peripherals = avatar.add_memory_range(0xe0000000, 0x00010000, name='Cortex Peripherals')

    # Initialize the targets
    logger.debug("Initializing targets")
    avatar.init_targets()

    # Set breakpoint right at the start of the loop
    hardware.set_breakpoint(after_init, hardware=True)
    hardware.cont()
    hardware.wait()
    print("breakpoint hit")
    util.printVT(hardware, irq_map)
    vt_offset = util.getVTOR(hardware)

    ###############################################################################################
    # print("\n=================== Dropping in interactive session =========================\n")
    # hwgdb, qegdb = isetup(locals())
    # IPython.embed()
    irq_trace = []

    def record_interrupt_enter(avatar, message, **kwargs):
        isr = message.interrupt_num
        isr_addr = message.isr_addr - 1
        irq = irq_map[isr_addr] if isr_addr in irq_map else 'unknown'
        logger.warning(f">>>>>>>>>>> ENTER IRQ-num={isr} irq={irq}")
        irq_trace.append(
            {'id': message.id, 'event': 'enter', 'isr': isr, 'irq': irq, 'timestamp': datetime.now().isoformat()})

    def record_interrupt_exit(avatar, message, **kwargs):
        isr = message.interrupt_num
        isr_addr = message.isr_addr - 1
        irq = irq_map[isr_addr] if isr_addr in irq_map else 'unknown'
        logger.warning(f">>>>>>>>>>> EXIT  IRQ-num={isr} irq={irq}")
        irq_trace.append(
            {'id': message.id, 'event': 'exit', 'isr': isr, 'irq': irq, 'timestamp': datetime.now().isoformat()})

    logger.debug("Registering interrupt handlers")
    avatar.watchmen.add_watchman('TargetInterruptEnter', 'after', record_interrupt_enter)
    avatar.watchmen.add_watchman('TargetInterruptExit', 'after', record_interrupt_exit)

    logger.debug("Enabling interrupts")
    # NOTE: Here we can edit breakpoints without running into bug hell
    hardware.remove_breakpoint(0)  # Remove our initial breakpoint
    hardware.remove_breakpoint(1)  # Remove our initial breakpoint
    # hardware.set_breakpoint(HARD_FAULT_ISR)  # Break on hardfault

    avatar.enable_interrupt_recording()

    print("\n=================== Finished setting up interrupt recording =========================\n")
    STUB_LOC = hardware.protocols.interrupts._monitor_stub_isr
    # hardware.set_breakpoint(STUB_LOC)  # Break on stub
    irq_map[STUB_LOC] = "STUB"
    util.printVT(hardware, irq_map)

    print("\n=================== Starting to record... =========================\n")
    print(f"HARDWARE: 0x{hardware.read_memory(VECTOR_TABLE_OFFSET_REG, 4):08x}")
    print()

    hardware.cont()

    print("\n=================== Dropping in interactive session =========================\n")
    hwgdb, _ = isetup(locals())
    IPython.embed()

    if save:
        print("\n=================== Storing acquired trace =========================\n")
        prefix = firmware.split('/')[-1].split('.')[0]
        trace_file = f"logs/traces/{prefix}-irq_trace-{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.json"
        logger.info(f"Storing trace to {trace_file}")
        util.writeAsJson(irq_trace, trace_file)

    print("=================== Done =========================")
    if hardware.state == TargetStates.RUNNING:
        hardware.stop()
    avatar.shutdown()
    return


# Example usage
# python irq-recording.py --firmware bins/blink-uart.bin --openocd_cfg common/pico-picoprobe.cfg \
#     --map irq-maps/blink_uart_map.py -I 0x10000354
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Generic interrupt tracing script",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--no-save", action="store_true", help="Do NOT store results in a file", default=False)
    parser.add_argument("-F", "--firmware", type=str, help="Path to the firmware binary", required=True)
    parser.add_argument("-O", "--openocd_cfg", type=str, help="Path to the openocd config", required=True)
    parser.add_argument("-I", "--init", type=str, help="Address after init has finished", required=True)
    parser.add_argument("-M", "--map", type=str, help="Path to file with the IRQ map", required=True)
    parser.add_argument("-E", "--elf_file", type=str, help="Path to ELF file", required=False, default="")

    args = parser.parse_args()
    save = not args.no_save
    firmware = args.firmware
    openocd_cfg = args.openocd_cfg
    after_init = int(args.init, 16)
    with open(args.map, 'r') as f:
        lines = f.readlines()
        map_str = "\n".join(lines)
        irq_map = ast.literal_eval(map_str)
    elf_file = None
    if args.elf_file:
        elf_file = abspath(args.elf_file)
    main(firmware, openocd_cfg, after_init, irq_map, save, elf_file)

# python irq-recording.py --firmware bins/can/send-uart.bin --openocd_cfg common/pico-picoprobe.cfg \
#     --map irq-maps/send_uart_map.py -I 0x1000063c --no-save

# python irq-recording.py --firmware bins/can/recv-uart.bin --openocd_cfg common/pico-picoprobe.cfg \
#     --map irq-maps/recv_uart_map.py -I 0x1000052e --no-save

# python irq-recording.py --firmware bins/wifi/server_counter.bin --openocd_cfg common/pico-picoprobe.cfg \
#     --map irq-maps/server_counter.py -I 0x100007da --no-save

# python irq-recording.py --firmware bins/wifi/picow_blink.bin --openocd_cfg common/pico-picoprobe.cfg \
#     --map irq-maps/picow_blink.py -I 0x1000036e --no-save

# python irq-recording.py --firmware bins/base/dma-simple.bin --openocd_cfg common/pico-picoprobe.cfg \
#     --map irq-maps/send_uart_map.py -I 0x1000043c --no-save

# python irq-recording.py --firmware bins/base/dma-repeating.bin --openocd_cfg common/pico-picoprobe.cfg \
#     --map irq-maps/dma_repeating_map.py -I 0x10000630

# python irq-recording.py --firmware bins/base/soft-int.bin --openocd_cfg common/pico-picoprobe.cfg \
#     --map irq-maps/soft_int_map.py -I 0x1000041e --no-save

# python irq-recording.py --firmware bins/IWatchdog.ino.bin --openocd_cfg common/stm32-stlink.cfg \
#     --map irq-maps/blink_usb_map.py -I 0x8000816 -E bins/IWatchdog.ino.elf