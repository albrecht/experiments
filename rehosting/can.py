from pprint import pprint

import IPython

from avatar2 import *

from common import util
from common.ipython_startup import *

from common.util import printVT, getVTOR, get_timer

VECTOR_TABLE_OFFSET_REG = 0xe000ed08

HARD_FAULT_ISR = 0x100001c4

IRQ_MAP = {
    0x20041fff: "__END_OF_RAM__",
    0x100001f6: "<_reset_handler>",
    0x100001c0: "<isr_invalid>",
    0x100001c2: "<isr_nmi>",
    HARD_FAULT_ISR: "<isr_hardfault>",
    0x100001c6: "<isr_svcall>",
    0x100001c8: "<isr_pendsv>",
    0x100001ca: "<isr_systick>",
    0x100001cc: "<__unhandled_user_irq>",
    0x100001d2: "<unhandled_user_irq_num_in_r0>",
    0x10001bcc: "<hardware_alarm_irq_handler>",
    0x100051d0: "<low_priority_worker_irq>",
    0x200021d4: "<irq_handler_chain_slots>",
    0x200021ec: "<irq_handler_chain_slots+24>",
}

FIRMWARE = "./bins/send.bin"
OPENOCD_CONFIG = "common/pico-jlink.cfg"
AFTER_INIT_LOC = 0x1000067a

def main():
    # Configure the location of config and binary files
    firmware = abspath(FIRMWARE)  # GPIO interrupt blink with debouncing
    bootrom = abspath('bins/base/pico_bootrom.bin')
    openocd_config = abspath(OPENOCD_CONFIG)

    # Initiate the avatar-object
    avatar = Avatar(arch=ARM_CORTEX_M3,
                    output_directory='/tmp/avatar', log_to_stdout=False)
    loggerAvatar, logger = util.setup_loggers()

    logger.info("Setting up targets")
    # Create the target-objects
    hardware = avatar.add_target(
        OpenOCDTarget, openocd_script=openocd_config, name='hardware')

    # Create controlled qemu instance
    qemu = avatar.add_target(QemuTarget, gdb_port=1236,
                             entry_address=0x1000000, name='qemu')

    logger.info("Loading interrupt plugins")
    avatar.load_plugin('arm.armv7m_interrupts')
    avatar.load_plugin('assembler')
    avatar.load_plugin('disassembler')

    # Memory mapping from https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf#_address_map
    rom = avatar.add_memory_range(
        0x00000000, 0x00004000, file=bootrom, name='ROM')  # 16kB ROM with bootloader
    # 2MB Flash, Accessing using XIP with cache
    xip_cached = avatar.add_memory_range(
        0x10000000, 0x00200000, file=firmware, name='Flash (XIP) - cache')

    ram = avatar.add_memory_range(
        0x20000000, 0x00042000, name='RAM')  # 264kB of RAM
    # We will need the apb system registers, esp the timer
    apb_peripherals = avatar.add_memory_range(
        0x40000000, 0x00070000, name='APB', forwarded=True, forwarded_to=hardware)
    # To blink the LED we will need the SIO registers (contains GPIO registers)
    sio = avatar.add_memory_range(
        0xd0000000, 0x0010000, name='SIO', forwarded=True, forwarded_to=hardware)  # SIO registers
    # Internal peripherals of the Cortex M0+, esp VTOR
    # arm_peripherals_1 = avatar.add_memory_range(
    #     0xe0000000, VECTOR_TABLE_OFFSET_REG - 0xe0000000, name='Cortex Peripherals', forwarded=True,
    #     forwarded_to=hardware)
    # arm_peripherals_vtor = avatar.add_memory_range(
    #     VECTOR_TABLE_OFFSET_REG, 4, name='Cortex Peripherals-VTOR', emulate=VTORPeripheral)
    # arm_peripherals_vtor = avatar.add_memory_range(
    #     VECTOR_TABLE_OFFSET_REG, 4, name='Cortex Peripherals-VTOR')
    # arm_peripherals_2 = avatar.add_memory_range(
    #     VECTOR_TABLE_OFFSET_REG + 4, 0xe000eda4 - VECTOR_TABLE_OFFSET_REG - 4,
    #     name='Cortex Peripherals-2', forwarded=True, forwarded_to=hardware)
    arm_peripherals = avatar.add_memory_range(0xe0000000, 0x00010000, name='Cortex Peripherals')

    # Initialize the targets
    logger.debug("Initializing targets")
    avatar.init_targets()

    # Set breakpoint right at the start of the loop
    hardware.set_breakpoint(AFTER_INIT_LOC, hardware=True)
    hardware.cont()
    hardware.wait()
    print("breakpoint hit")
    printVT(hardware, IRQ_MAP)
    vt_offset = getVTOR(hardware)

    logger.debug("Syncing targets")
    avatar.transfer_state(hardware, qemu, sync_regs=True, synced_ranges=[ram])

    ###############################################################################################
    #print("\n=================== Dropping in interactive session =========================\n")
    #hwgdb, qegdb = isetup(locals())
    #IPython.embed()
    # Important setup
    # hardware.ivt_address = vt_offset

    def record_interrupt_enter(avatar, message, **kwargs):
        isr = message.interrupt_num
        logger.warning(f">>>>>>>> ENTER {hex(isr)} {message} {kwargs}")

    def record_interrupt_exit(avatar, message, **kwargs):
        isr = message.interrupt_num
        logger.warning(f">>>>>>>> EXIT {hex(isr)} {message} {kwargs}")
        pprint(get_timer(hardware))

    logger.debug("Registering interrupt handlers")
    avatar.watchmen.add_watchman('RemoteInterruptEnter', 'after',
                                 record_interrupt_enter)
    avatar.watchmen.add_watchman('RemoteInterruptExit', 'after',
                                 record_interrupt_exit)

    logger.debug("Enabling interrupts")
    # NOTE: Here we can edit breakpoints without running into bug hell
    hardware.remove_breakpoint(0)  # Remove our initial breakpoint
    hardware.remove_breakpoint(1)  # Remove our initial breakpoint
    # hardware.set_breakpoint(
    #     0x20001200 + 24)  # TODO: This should not work because the stub should overwrite the breakpoint
    #############################################
    # qemu.set_breakpoint(alarm_pool_alarm_callback_p10, hardware=True)
    # qemu.set_breakpoint(my_time_callback, hardware=True)
    # qemu.set_breakpoint(GPIO_CALLBACK, hardware=True)
    # qemu.set_breakpoint(GPIO_CALLBACK_RAM)
    qemu.set_breakpoint(HARD_FAULT_ISR)  # Break on hardfault

    # qemu.set_breakpoint(add_alarm_under_lock)

    # enabel_dbg(loggerAvatar)
    avatar.transfer_interrupt_state(hardware, qemu)
    avatar.enable_interrupt_forwarding(hardware, qemu)

    STUB_LOC = hardware.protocols.interrupts._monitor_stub_isr
    IRQ_MAP[STUB_LOC] = "STUB"

    # NOTE: FIXME: HACK: DO NOT TOUCH THE BREAKPOINTS !!! => Will result in weird crash
    # disabel_dbg(loggerAvatar)

    print("\n=================== Finished setting up interrupt recording =========================\n")
    printVT(hardware, IRQ_MAP)

    print("\n=================== Continuing... =========================\n")

    print("\n=================== Dropping in interactive session =========================\n")
    print(f"HARDWARE: 0x{hardware.read_memory(VECTOR_TABLE_OFFSET_REG, 4):08x}")
    print(f"QEmu    : 0x{qemu.read_memory(VECTOR_TABLE_OFFSET_REG, 4):08x}")
    print()

    hardware.cont()
    # qemu.cont()
    # sleep(4.0)

    hwgdb, qegdb = isetup(locals())
    asm(qegdb)
    IPython.embed()
    # Executing `qemu.cont(); qemu.wait()` in the ipython shell, will cause the LED
    # to either turn on or off because we step from sleep() to sleep()

    print("=================== Done =========================")
    if hardware.state == TargetStates.RUNNING:
        hardware.stop()
    if qemu.state == TargetStates.RUNNING:
        qemu.stop()
    avatar.shutdown()
    return


if __name__ == '__main__':
    main()
