import avatar2
from avatar2 import Avatar, TargetStates

print("IPython custom init...")


def isetup(env):
    hwgdb = env['hardware'].protocols.execution if 'hardware' in env else None
    qegdb = env['qemu'].protocols.execution if 'qemu' in env else None
    return hwgdb, qegdb


def p(gdb_out):
    print(gdb_out[1].replace('\n\n', '\n'))


def asm(gdb, size=5, address=None):
    if not address:
        p(gdb.console_command(f'display/{size * 2}i $pc-{size}+1'))  # +1 to enforce thumb mode
    else:
        p(gdb.console_command(f'display/{size * 2}i 0x{address:x}'))


def e(gdb, cmd):
    p(gdb.console_command(cmd))


def info_regs(avatar, device):
    # regs = avatar.arch.registers.keys()
    regs = ['r0', 'r1', 'r2', 'r3', 'r4', 'r5', 'r6', 'r7', 'r8', 'r9', 'r10', 'r11', 'r12', 'lr', 'sp', 'pc', 'xpsr']
    for r in regs:
        val = device.read_register(r)
        if isinstance(val, list):
            val = val[0]
        print(f"{r:5}  ==  0x{val:08x}   ({val})")


def mem(gdb, address, count=4, size='w'):
    p(gdb.console_command(f'display/{count}x{size} 0x{address:x}'))


def stack(gdb, count=10, size='w'):
    p(gdb.console_command(f'display/{count}x{size} $sp'))


def bin_str(num):
    str = bin(num)[2:][::-1]
    for i, c in enumerate(str):
        print(f"{i + 1:2} | {c}")


def stop(avatar: Avatar):
    for t in avatar.get_targets():
        if t[1].state == TargetStates.RUNNING:
            t[1].stop()

# %run ipython_startup.py
# hwgdb, qegdb = isetup(locals())
