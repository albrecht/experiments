import ast
import logging
import json

import avatar2
from avatar2.peripherals import AvatarPeripheral

from common import pretty_logging

VECTOR_TABLE_OFFSET_REG = 0xe000ed08


def setup_loggers(avatar=True, script=True, emulated_logger=False):
    res = []
    if avatar:
        logger = logging.getLogger('avatar')
        logger.setLevel(logging.INFO)
        pretty_logging.setup(logger, logging.INFO)
        res.append(logger)

    if script:
        logger = logging.getLogger('script')
        logger.setLevel(logging.DEBUG)
        logger = pretty_logging.setup(logger, logging.DEBUG)
        logger.root.handlers.clear()
        res.append(logger)

    if emulated_logger:
        logger = logging.getLogger('emulated')
        logger.setLevel(logging.DEBUG)
        logger = pretty_logging.setup(logger, logging.DEBUG)
        logger.root.handlers.clear()
        res.append(logger)

    return res


def getVTOR(device: avatar2.Target):
    return device.read_memory(VECTOR_TABLE_OFFSET_REG, 4)


def printVT(device: avatar2.Target, irq_map):
    vt_offset = getVTOR(device)
    print(f"Vector table is at address 0x{vt_offset:x}")

    # The last 4 bytes contain the end of RAM address
    vector_table = device.read_memory(
        vt_offset, size=4, num_words=48)  # 48 = 16 cpu internal + 32 user interrupts
    print(f"Vector table:")
    print(f"  Address      [irq--num] --  target      --  label")
    for i, entry in enumerate(vector_table):
        if entry - 1 in irq_map:
            label = "\x1b[32m" + irq_map[entry - 1] + "\x1b[0m"
        else:
            label = "\x1b[33;1munknown\x1b[0m"
        print(
            f"    0x{vt_offset + i * 4:8x} [{i:2d} / {(i - 16):3d}] --  0x{entry:8x}  --  {label}")


TIMER_BASE = 0x40054000


def get_timer(target: avatar2.Target):
    timer_mem = target.read_memory(address=TIMER_BASE, size=4, num_words=17)[::-1]
    timer_state = {
        "TIMER_TIMEHW": timer_mem[0],
        "TIMER_TIMELW": timer_mem[1],
        "TIMER_TIMEHR": timer_mem[2],
        "TIMER_TIMELR": timer_mem[3],
        "TIMER_ALARM0": timer_mem[4],
        "TIMER_ALARM1": timer_mem[5],
        "TIMER_ALARM2": timer_mem[6],
        "TIMER_ALARM3": timer_mem[7],
        "TIMER_ARMED": timer_mem[8],
        "TIMER_TIMERAWH": timer_mem[9],
        "TIMER_TIMERAWL": timer_mem[10],
        "TIMER_DBGPAUSE": timer_mem[11],
        "TIMER_PAUSE": timer_mem[12],
        "TIMER_INTR": timer_mem[13],
        "TIMER_INTE": timer_mem[14],
        "TIMER_INTF": timer_mem[15],
        "TIMER_INTS": timer_mem[16],
    }
    return timer_state


def enabel_dbg(log):
    log.setLevel(logging.DEBUG)
    for handler in log.handlers:
        handler.setLevel(logging.DEBUG)


def disabel_dbg(log):
    log.setLevel(logging.INFO)
    for handler in log.handlers:
        handler.setLevel(logging.INFO)


# ================================ File IO ====================================

def writeAsJson(data, filename):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)


def load_map_file(map_filename):
    with open(map_filename, 'r') as f:
        lines = f.readlines()
        map_str = "\n".join(lines)
        return ast.literal_eval(map_str)
