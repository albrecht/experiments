import argparse
import datetime
import json


def main(file, head=False, tail=False):
    data = json.load(file)
    r_from = 0
    r_to = len(data)
    if head:
        r_to = 10 if isinstance(head, bool) else head
    if tail:
        r_from = len(data) - 10

    for i in range(r_from, r_to):
        entry = data[i]
        event = entry['event'].upper()
        time = datetime.datetime.fromisoformat(entry['timestamp'])
        print(f"{time} {event} {entry['irq']} ({entry['isr']})")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="IRQ tracer log helper ",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("log", type=str, help="Path to the logfile")
    parser.add_argument("-H", help="Show only the 10 leading rows", action="store_true")
    parser.add_argument("--head", help="Show only the N leading rows")
    parser.add_argument("-T", help="Show only the 10 trailing rows", action="store_true")
    parser.add_argument("--tail", help="Show only the N trailing rows")

    args = parser.parse_args()
    assert not (args.head and args.tail), "Only one of --head and --tail can be specified"
    head = int(args.head) if args.head else args.H
    tail = int(args.tail) if args.tail else args.T
    with open(args.log, 'r') as logfile:
        main(logfile, head=head, tail=tail)
