from pprint import pprint

from avatar2 import Target


class PicoWrapper:
    TIMER_BASE = 0x40054000

    def __init__(self, target: Target):
        self.target = target

    def get_time(self):
        # Reads from TIMERAWL to avoid side effects
        return self.target.read_memory(address=self.TIMER_BASE + 0x28, size=4)

    def get_alarm0(self):
        return self.target.read_memory(address=self.TIMER_BASE + 0x10, size=4)

    def get_alarm1(self):
        return self.target.read_memory(address=self.TIMER_BASE + 0x14, size=4)

    def get_alarm2(self):
        return self.target.read_memory(address=self.TIMER_BASE + 0x18, size=4)

    def get_alarm3(self):
        return self.target.read_memory(address=self.TIMER_BASE + 0x1c, size=4)

    def get_timer(self):
        timer_mem = self.target.read_memory(address=self.TIMER_BASE, size=4, num_words=17)
        timer_state = {
            "TIMER_TIMEHW": timer_mem[0],
            "TIMER_TIMELW": timer_mem[1],
            "TIMER_TIMEHR": timer_mem[2],
            "TIMER_TIMELR": timer_mem[3],
            "TIMER_ALARM0": timer_mem[4],
            "TIMER_ALARM1": timer_mem[5],
            "TIMER_ALARM2": timer_mem[6],
            "TIMER_ALARM3": timer_mem[7],
            "TIMER_ARMED": timer_mem[8],
            "TIMER_TIMERAWH": timer_mem[9],
            "TIMER_TIMERAWL": timer_mem[10],
            "TIMER_DBGPAUSE": timer_mem[11],
            "TIMER_PAUSE": timer_mem[12],
            "TIMER_INTR": timer_mem[13],
            "TIMER_INTE": timer_mem[14],
            "TIMER_INTF": timer_mem[15],
            "TIMER_INTS": timer_mem[16],
        }
        return timer_state

    def get_nvic_state(self, raw=True):
        mem = self.target
        if raw:
            mem = self.target.protocols.memory
        nvic_state = {
            "Enabled Interrupts (ISER)": mem.read_memory(address=0xe000e100, size=4),
            "Pending Interrupts (ISPR)": mem.read_memory(address=0xe000e200, size=4),
            "Interrupt Control and State Register (ICSR)": mem.read_memory(address=0xe000ed04, size=4),
            "Vector Table Offset Register (VTOR)": mem.read_memory(address=0xe000ed08, size=4),
        }
        return nvic_state

    def print_nvic_state(self, raw=True):
        state = self.get_nvic_state(raw)
        if raw:
            print("!!! Accessing raw NVIC state !!!")
        for k, v in state.items():
            print(f">   {k}: {hex(v)}")
        interrupts_pending = state["Pending Interrupts (ISPR)"] & state["Enabled Interrupts (ISER)"]
        for i in range(32):
            if interrupts_pending & (1 << i):
                print(f">   Pending Interrupt {i + 16}")
