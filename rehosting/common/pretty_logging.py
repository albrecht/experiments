import logging


class CustomFormatter(logging.Formatter):

    green = "\x1b[32;20m"
    grey = "\x1b[37;20m"
    yellow = "\x1b[33;20m"
    blue = "\x1b[34;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format_time = "%(asctime)s"
    format_level = "%(levelname)s"
    format_source = "%(name)s"
    format_message = "%(message)s"
    format = f"{format_time} | {format_source} | {format_level} | {format_message}"

    FORMATS = {
        logging.NOTSET: grey + format + reset,
        logging.DEBUG: f"{grey}{format_time} | {format_source}{green} | {format_level} | {grey}{format_message}{reset}",
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS[record.levelno]
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def setup(logger: logging.Logger, level):
    # Create handler which will catch every message
    ch = logging.StreamHandler()
    ch.setLevel(level)
    ch.setFormatter(CustomFormatter())
    # Assign this handler to the logger
    logger.addHandler(ch)

    return logger
