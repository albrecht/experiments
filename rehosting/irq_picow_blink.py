from datetime import datetime

import IPython

from avatar2 import *

from common import util
from common.ipython_startup import *
from common.pico_wrapper import PicoWrapper

VECTOR_TABLE_OFFSET_REG = 0xe000ed08

HARD_FAULT_ISR = 0x100001c4

FIRMWARE = "./bins/wifi/picow_blink.bin"
OPENOCD_CONFIG = "common/pico-picoprobe.cfg"
BOOTROM = "./bins/wifi/picoW_bootrom.bin"
AFTER_INIT_LOC = 0x1000037a  # beginning of loop
IRQ_MAP = util.load_map_file("./irq-maps/picow_blink.py")


def main():
    # Configure the location of config and binary files
    firmware = abspath(FIRMWARE)
    bootrom = abspath(BOOTROM)
    openocd_config = abspath(OPENOCD_CONFIG)
    binary = abspath('./bins/wifi/picow_blink.elf')


    # Initiate the avatar-object
    avatar = Avatar(arch=ARM_CORTEX_M3,
                    output_directory='/tmp/avatar', log_to_stdout=False)
    loggerAvatar, logger, _ = util.setup_loggers(emulated_logger=True)

    logger.info("Setting up targets")
    # Create the target-objects
    hardware = avatar.add_target(
        OpenOCDTarget, openocd_script=openocd_config, name='hardware', binary=binary)
    pico = PicoWrapper(hardware)

    # Create controlled qemu instance
    qemu = avatar.add_target(QemuTarget, gdb_port=1236, gdb_additional_args=['--args', binary],
                             entry_address=0x1000000, name='qemu')
    qpico = PicoWrapper(qemu)

    interrupt_config = {
        'protocol': {
            'irq_interleaving': False,
        },
        'plugin': {},
    }
    logger.info("Loading interrupt plugins")
    avatar.load_plugin('arm.armv7m_interrupts', config=interrupt_config)
    avatar.load_plugin('assembler')
    avatar.load_plugin('disassembler')

    # Memory mapping from https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf#_address_map
    rom = avatar.add_memory_range(
        0x00000000, 0x00004000, file=bootrom, name='ROM')  # 16kB ROM with bootloader
    # 2MB Flash, Accessing using XIP with cache
    xip_cached = avatar.add_memory_range(
        0x10000000, 0x00200000, file=firmware, name='Flash (XIP) - cache')

    ram = avatar.add_memory_range(
        0x20000000, 0x00042000, name='RAM')  # 264kB of RAM
    # We will need the apb system registers, esp the timer
    apb_peripherals = avatar.add_memory_range(
        0x40000000, 0x00070000, name='APB', forwarded=True, forwarded_to=hardware)
    # TODO: Doc
    dma = avatar.add_memory_range(
        0x50000000, 0x00200000, name='dma', forwarded=True, forwarded_to=hardware)
    pio_0 = avatar.add_memory_range(
        0x50200000, 0x00100000, name='pio_0', forwarded=True, forwarded_to=hardware)
    pio_1 = avatar.add_memory_range(
        0x50300000, 0x00100000, name='pio_1', forwarded=True, forwarded_to=hardware)
    # To blink the LED we will need the SIO registers (contains GPIO registers)
    sio = avatar.add_memory_range(
        0xd0000000, 0x0000017c, name='SIO', forwarded=True, forwarded_to=hardware)  # SIO registers
    # Internal peripherals of the Cortex M0+, esp VTOR
    arm_peripherals = avatar.add_memory_range(0xe0000000, 0x00010000, name='Cortex Peripherals')

    # Initialize the targets
    logger.debug("Initializing targets")
    avatar.init_targets()

    # Set breakpoint right at the start of the loop
    hardware.set_breakpoint(AFTER_INIT_LOC, hardware=True)
    hardware.cont()
    hardware.wait()
    print("breakpoint hit")
    util.printVT(hardware, IRQ_MAP)
    vt_offset = util.getVTOR(hardware)

    logger.debug("Syncing targets")
    avatar.transfer_state(hardware, qemu, sync_regs=True, synced_ranges=[ram])

    ###############################################################################################
    # print("\n=================== Dropping in interactive session =========================\n")
    # hwgdb, qegdb = isetup(locals())
    # IPython.embed()
    ###############################################################################################
    irq_trace = []

    def record_interrupt_enter(avatar, message, **kwargs):
        isr = message.interrupt_num
        isr_addr = message.isr_addr - 1
        irq = IRQ_MAP[isr_addr] if isr_addr in IRQ_MAP else 'unknown'
        logger.warning(f">>>>>>>>>>> ENTER IRQ-num={isr} irq={irq}")
        irq_trace.append(
            {'id': message.id, 'event': 'enter', 'isr': isr, 'irq': irq, 'timestamp': datetime.now().isoformat()})

    def record_interrupt_exit(avatar, message, **kwargs):
        isr = message.interrupt_num
        logger.warning(f">>>>>>>>>>> EXIT  IRQ-num={isr}")
        irq_trace.append(
            {'id': message.id, 'event': 'exit', 'isr': isr, 'irq': '__LAST_ENTER__',
             'timestamp': datetime.now().isoformat()})


    logger.debug("Registering interrupt handlers")
    avatar.watchmen.add_watchman('TargetInterruptEnter', 'after', record_interrupt_enter)
    avatar.watchmen.add_watchman('RemoteInterruptExit', 'after', record_interrupt_exit)

    logger.debug("Enabling interrupts")
    # NOTE: Here we can edit breakpoints without running into bug hell
    # hardware.remove_breakpoint(0)  # Remove our initial breakpoint
    # hardware.remove_breakpoint(1)  # Remove our initial breakpoint
    #############################################
    qemu.set_breakpoint(HARD_FAULT_ISR)  # Break on hardfault
    # qemu.set_breakpoint(0x10000376)  # break right before gpio put
    # qemu.set_breakpoint(0x1000037c)  # break right after gpio put
    # qemu.set_breakpoint(0x10006f38)  # <cyw43_arch_poll>
    # qemu.set_breakpoint(0x10006c10)  # <pio_sm_set_pindirs_with_mask>

    # util.enabel_dbg(loggerAvatar)
    avatar.transfer_interrupt_state(hardware, qemu)
    avatar.enable_interrupt_forwarding(hardware, qemu)
    # util.disabel_dbg(loggerAvatar)

    print("\n=================== Finished setting up interrupt rehosting =========================\n")
    STUB_LOC = hardware.protocols.interrupts._monitor_stub_isr
    # hardware.set_breakpoint(STUB_LOC)  # Break on stub
    IRQ_MAP[STUB_LOC] = "STUB"
    util.printVT(hardware, IRQ_MAP)

    print(f"Hardware NVIC")
    pico.print_nvic_state()
    print(f"QEmu NVIC")
    qpico.print_nvic_state()

    print("\n=================== Continuing... =========================\n")
    print(f"HARDWARE: 0x{hardware.read_memory(VECTOR_TABLE_OFFSET_REG, 4):08x}")
    print(f"QEmu    : 0x{qemu.read_memory(VECTOR_TABLE_OFFSET_REG, 4):08x}")
    print()
    # pprint(pico.get_timer())

    qemu.cont()
    sleep(2)
    hardware.cont()

    print("\n=================== Dropping in interactive session =========================\n")

    hwgdb, qegdb = isetup(locals())
    IPython.embed()
    # Executing `qemu.cont(); qemu.wait()` in the ipython shell, will cause the LED
    # to either turn on or off because we step from sleep() to sleep()

    # print("\n=================== Storing acquired trace =========================\n")
    # trace_file = f"logs/traces/rehosted_pricow-blink_trace-{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.json"
    # logger.info(f"Storing trace to {trace_file}")
    # util.writeAsJson(irq_trace, trace_file)

    print("=================== Done =========================")
    if hardware.state == TargetStates.RUNNING:
        hardware.stop()
    if qemu.state == TargetStates.RUNNING:
        qemu.stop()
    avatar.shutdown()
    return


if __name__ == '__main__':
    main()
