# HAL rehosting CAN

## Rehosted send-uart

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)
Tick 0
CAN successfully sent: rc=0, data=1
Tick 1
CAN successfully sent: rc=0, data=2
Tick 2
CAN successfully sent: rc=0, data=3
Tick 3
```

### Answering recv-usb

```
>>>> irq-hit 0x1
Processing RXIF event (0xa012000e)
Received Event 1 
0x004 01000000 (2697705244)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000e)
Received Event 1 
0x004 02000000 (2710118727)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000e)
Received Event 1 
0x004 03000000 (2722514282)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000e)
Received Event 1 
0x004 04000000 (2734728101)
```

## Rehosted recv-uart

```
============================== Boot complete ==============================
Initializing CAN ...
CAN Init finished :)
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Processing RXIF event (0xa012000a)
Processing RXIF event (0xa012000a)
>>>> irq-hit 0x0
Received Event 1 
0x004 73000000 (2892858)
Received Event 1 
0x004 74000000 (12892420)
Received Event 1 
0x004 75000000 (22891993)
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 76000000 (32891557)
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 77000000 (42891124)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 78000000 (52890664)
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
>>>> irq-hit 0x0
Received Event 1 
0x004 79000000 (62890448)
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 7a000000 (72890003)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 7b000000 (82889543)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 7c000000 (92889140)
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
>>>> irq-hit 0x0
Received Event 1 
0x004 7d000000 (102888704)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
>>>> irq-hit 0x0
Received Event 1 
0x004 7e000000 (112888270)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 7f000000 (122887859)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 80000000 (132887429)
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
>>>> irq-hit 0x0
Received Event 1 
0x004 81000000 (142886976)                                                
>>>> irq-hit 0x1                                                          
Processing RXIF event (0xa012000a)                                        
>>>> irq-hit 0x0                                                          
Received Event 1                                                          
0x004 82000000 (152886540)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
>>>> irq-hit 0x0
Received Event 1 
0x004 83000000 (162886129)
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
>>>> irq-hit 0x0
Received Event 1 
0x004 84000000 (172885675)
```

### Answering send-usb

```
Tick 114
CAN successfully sent: rc=0, data=115
Processing TEFIF event (0xa012001c)
Tick 115
CAN successfully sent: rc=0, data=116
Processing TEFIF event (0xa012001c)
Tick 116
CAN successfully sent: rc=0, data=117
Processing TEFIF event (0xa012001c)
Tick 117
CAN successfully sent: rc=0, data=118
Processing TEFIF event (0xa012001c)
Tick 118
CAN successfully sent: rc=0, data=119
Processing TEFIF event (0xa012001c)
Tick 119
CAN successfully sent: rc=0, data=120
Processing TEFIF event (0xa012001c)
Tick 120
CAN successfully sent: rc=0, data=121
Processing TEFIF event (0xa012001c)
Tick 121
CAN successfully sent: rc=0, data=122
Processing TEFIF event (0xa012001c)
Tick 122
CAN successfully sent: rc=0, data=123
Processing TEFIF event (0xa012001c)
Tick 123
CAN successfully sent: rc=0, data=124
Processing TEFIF event (0xa012001c)
Tick 124
CAN successfully sent: rc=0, data=125
Processing TEFIF event (0xa012001c)
Tick 125
CAN successfully sent: rc=0, data=126
Processing TEFIF event (0xa012001c)
Tick 126
CAN successfully sent: rc=0, data=127
Processing TEFIF event (0xa012001c)
Tick 127
CAN successfully sent: rc=0, data=128
Processing TEFIF event (0xa012001c)
Tick 128
CAN successfully sent: rc=0, data=129
Processing TEFIF event (0xa012001c)
Tick 129
CAN successfully sent: rc=0, data=130
Processing TEFIF event (0xa012001c)
Tick 130
CAN successfully sent: rc=0, data=131
Processing TEFIF event (0xa012001c)
Tick 131
CAN successfully sent: rc=0, data=132
Processing TEFIF event (0xa012001c)
```

## Rehosted echo-uart

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)

Tick 0
0x004 00000000 (1411148306)
Updated counter to 1
0x004 00000000 (1413648460)
Updated counter to 1
0x004 00000000 (1416148597)
Updated counter to 1
0x004 00000000 (1418648750)
Updated counter to 1
0x004 00000000 (1421148887)
Updated counter to 1
0x004 00000000 (1423649033)
Updated counter to 1
0x004 00000000 (1426149162)
Updated counter to 1
0x004 00000000 (1428649309)
Updated counter to 1
0x004 00000000 (1431149438)
Updated counter to 1
0x004 00000000 (1433649556)
Updated counter to 1
0x004 00000000 (1436149687)
Updated counter to 1
0x004 00000000 (1438649828)
Updated counter to 1
0x004 00000000 (1441149999)
Updated counter to 1
0x004 00000000 (1443650141)
Updated counter to 1
0x004 00000000 (1446150280)
Updated counter to 1
0x004 00000000 (1448650451)
Updated counter to 1
0x004 00000000 (1451150584)
Updated counter to 1
CAN successfully sent: rc=0, data=1

Tick 1
[WARNING] package was not acknowledged (ACK Error)
[WARNING] package was not acknowledged (ACK Error)
[WARNING] package was not acknowledged (ACK Error)
[WARNING] package was not acknowledged (ACK Error)
[WARNING] package was not acknowledged (ACK Error)
0x004 02000000 (1477983159)
Updated counter to 3
CAN successfully sent: rc=0, data=3

Tick 3
0x004 00000000 (1487983251)
Updated counter to 1
0x004 04000000 (1490483936)
Updated counter to 5
CAN successfully sent: rc=0, data=5

Tick 5
0x004 06000000 (1500484567)
Updated counter to 7
CAN successfully sent: rc=0, data=7

Tick 7
0x004 08000000 (1507985167)
Updated counter to 9
CAN successfully sent: rc=0, data=9

Tick 9
0x004 0a000000 (1515485899)
Updated counter to 11
CAN successfully sent: rc=0, data=11

Tick 11
0x004 0c000000 (1522986459)
Updated counter to 13
CAN successfully sent: rc=0, data=13

Tick 13
0x004 0e000000 (1530487073)
Updated counter to 15
CAN successfully sent: rc=0, data=15

Tick 15
0x004 10000000 (1537987749)
Updated counter to 17
CAN successfully sent: rc=0, data=17

Tick 17
0x004 12000000 (1547988299)
Updated counter to 19
```

### Answering send-usb

```============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)

Tick 0
0x004 00000000 (1411148306)
Updated counter to 1
0x004 00000000 (1413648460)
Updated counter to 1
0x004 00000000 (1416148597)
Updated counter to 1
0x004 00000000 (1418648750)
Updated counter to 1
0x004 00000000 (1421148887)
Updated counter to 1
0x004 00000000 (1423649033)
Updated counter to 1
0x004 00000000 (1426149162)
Updated counter to 1
0x004 00000000 (1428649309)
Updated counter to 1
0x004 00000000 (1431149438)
Updated counter to 1
0x004 00000000 (1433649556)
Updated counter to 1
0x004 00000000 (1436149687)
Updated counter to 1
0x004 00000000 (1438649828)
Updated counter to 1
0x004 00000000 (1441149999)
Updated counter to 1
0x004 00000000 (1443650141)
Updated counter to 1
0x004 00000000 (1446150280)
Updated counter to 1
0x004 00000000 (1448650451)
Updated counter to 1
0x004 00000000 (1451150584)
Updated counter to 1
CAN successfully sent: rc=0, data=1

Tick 1
[WARNING] package was not acknowledged (ACK Error)
[WARNING] package was not acknowledged (ACK Error)
[WARNING] package was not acknowledged (ACK Error)
[WARNING] package was not acknowledged (ACK Error)
[WARNING] package was not acknowledged (ACK Error)
0x004 02000000 (1477983159)
Updated counter to 3
CAN successfully sent: rc=0, data=3

Tick 3
0x004 00000000 (1487983251)
Updated counter to 1
0x004 04000000 (1490483936)
Updated counter to 5
CAN successfully sent: rc=0, data=5

Tick 5
0x004 06000000 (1500484567)
Updated counter to 7
CAN successfully sent: rc=0, data=7

Tick 7
0x004 08000000 (1507985167)
Updated counter to 9
CAN successfully sent: rc=0, data=9

Tick 9
0x004 0a000000 (1515485899)
Updated counter to 11
CAN successfully sent: rc=0, data=11

Tick 11
0x004 0c000000 (1522986459)
Updated counter to 13
CAN successfully sent: rc=0, data=13

Tick 13
0x004 0e000000 (1530487073)
Updated counter to 15
CAN successfully sent: rc=0, data=15

Tick 15
0x004 10000000 (1537987749)
Updated counter to 17
CAN successfully sent: rc=0, data=17

Tick 17
0x004 12000000 (1547988299)
Updated counter to 19
```

### Answering echo-usb

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)

Tick 0
[ERROR] 39024f
0x004 01000000 (1549487757)
Updated counter to 2
CAN successfully sent: rc=0, data=2

Tick 2

Tick 3

Tick 4

Tick 5
CAN successfully sent: rc=0, data=0

Tick 0
0x004 03000000 (1560448467)
Updated counter to 4
CAN successfully sent: rc=0, data=4

Tick 4

Tick 5

Tick 6

Tick 7
0x004 05000000 (1570655318)
Updated counter to 6
CAN successfully sent: rc=0, data=6

Tick 6

Tick 7

Tick 8
0x004 07000000 (1579316054)
Updated counter to 8
CAN successfully sent: rc=0, data=8

Tick 8

Tick 9

Tick 10
0x004 09000000 (1586802852)
Updated counter to 10
CAN successfully sent: rc=0, data=10

Tick 10

Tick 11

Tick 12
0x004 0b000000 (1594412765)
Updated counter to 12
CAN successfully sent: rc=0, data=12

Tick 12

Tick 13

Tick 14
0x004 0d000000 (1602141545)
Updated counter to 14
CAN successfully sent: rc=0, data=14

Tick 14

Tick 15

Tick 16
0x004 0f000000 (1609966531)
Updated counter to 16
CAN successfully sent: rc=0, data=16

Tick 16

Tick 17

Tick 18

Tick 19
0x004 11000000 (1617690535)
Updated counter to 18
CAN successfully sent: rc=0, data=18

Tick 18

Tick 19

Tick 20
0x004 13000000 (1626250855)
Updated counter to 20
CAN successfully sent: rc=0, data=20

Tick 20
```
