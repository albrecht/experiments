# DMA Rehosting


## Rehosting without special handling (with IRQs)

### Comments
* We can see that the original init message never gets' updated
* This is because the `snprintf` is only update the RAM in QEMU
* We need to forward this to the device before starting the DMA transfer
* We can still see that the length of the message is updated because after 10 messages the 'c' character gets printed
* The newline is missing because it is a lot later in the buffer

### UART output
```
========================== Boot complete ============================
DMA repeating uart example starting...
DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reaDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reacDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reacDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reacDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reacDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reacDMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
This will be update by the reacDMA complete -- Scheduling dma-reset in 1 second.
```

## DMA rehosting with interception of DMA trigger for memory sync

### Comments
* Now the application can be rehosted completely
* It needs to stop QEMU before the DMA transfer is started to copy the memory ranges

### UART output
```
========================== Boot complete ============================
DMA repeating uart example starting...
DMA resetting...
>> Current counter value = 0
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 1
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 2
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 3
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 4
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 5
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 6
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 7
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 8
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 9
DMA complete -- Scheduling dma-reset in 1 second.

DMA resetting...
>> Current counter value = 10
DMA complete -- Scheduling dma-reset in 1 second.
```