# USB rehosting

## Comments

* Due to the slow down the USB connection dies because it's async worker get's shadowed by the higher priority
  interrupts

## log

### USB log

```
Initialization complete :)
```

### UART log

```
...
Initialization complete :)
Counter = 0
Counter = 1
Counter = 2
Counter = 3
Counter = 4
Counter = 5
Counter = 6
Counter = 7
Counter = 8
Counter = 9
Counter = 10
Counter = 11
Counter = 12
Counter = 13
Counter = 14
Counter = 15
Counter = 16
Counter = 17
Counter = 18
Counter = 19
Counter = 20
Counter = 21
Counter = 22
Counter = 23
Counter = 24
Counter = 25
Counter = 26
Counter = 27
Counter = 28
Counter = 29
```

### Avatar log

```
❯ python hal-irq_usb_blink.py
IPython custom init...
2023-08-21 14:01:01,617 | script | INFO | Setting up targets
2023-08-21 14:01:01,622 | avatar.targets.qemu | INFO | QEmuTarget using executable '/mnt/dev/thesis/avatar-qemu/build/qemu-system-arm'
2023-08-21 14:01:01,622 | script | INFO | Loading interrupt plugins
2023-08-21 14:01:01,655 | script | DEBUG | Initializing targets
2023-08-21 14:01:03,062 | avatar.targets.hardware | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:01:03,063 | avatar | INFO | Received state update of target hardware to TargetStates.STOPPED
2023-08-21 14:01:03,073 | avatar.targets.hardware | INFO | Successfully connected to OpenOCD target!
2023-08-21 14:01:03,209 | avatar | INFO | Attaching ARMv7 Interrupts protocol to <avatar2.targets.openocd_target.OpenOCDTarget object at 0x7f0024ee9360>
2023-08-21 14:01:03,209 | avatar.protocols.ARMV7InterruptProtocol | INFO | ARMV7InterruptProtocol initialized
2023-08-21 14:01:03,210 | avatar | INFO | Attaching ARMv7 Interrupt-Recorder protocol to <avatar2.targets.openocd_target.OpenOCDTarget object at 0x7f0024ee9360>
2023-08-21 14:01:03,210 | avatar.protocols.ARMV7HALCallerProtocol | INFO | ARMV7HALCallerProtocol initialized
2023-08-21 14:01:03,213 | avatar.targets.qemu | INFO | QEMU process running
2023-08-21 14:01:03,610 | avatar.targets.qemu | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:01:03,615 | avatar | INFO | Received state update of target qemu to TargetStates.STOPPED
2023-08-21 14:01:03,622 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "qmp_capabilities", "id": 0}\r\n'
2023-08-21 14:01:03,623 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 0}\r\n'
2023-08-21 14:01:03,623 | avatar.targets.qemu | INFO | Connected to remote target
2023-08-21 14:01:03,624 | avatar.targets.qemu.RemoteMemoryProtocol | INFO | Successfully connected rmp
2023-08-21 14:01:03,624 | avatar | INFO | Attaching ARMv7 Interrupts protocol to <avatar2.targets.qemu_target.QemuTarget object at 0x7f0024eea4d0>
2023-08-21 14:01:03,624 | avatar | INFO | Attaching ARMv7 Interrupt-Recorder protocol to <avatar2.targets.qemu_target.QemuTarget object at 0x7f0024eea4d0>
2023-08-21 14:01:03,625 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | QemuARMV7HALCallerProtocol initialized
2023-08-21 14:01:03,632 | avatar.targets.hardware.GDBProtocol | INFO | Continuing execution of hardware
2023-08-21 14:01:03,642 | avatar.targets.hardware | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:01:03,642 | avatar | INFO | Received state update of target hardware to TargetStates.RUNNING
2023-08-21 14:01:03,642 | avatar.targets.hardware.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 6, 'stream': 'stdout'}, returning True
2023-08-21 14:01:05,231 | avatar.targets.hardware | INFO | State changed to TargetStates.BREAKPOINT
2023-08-21 14:01:05,232 | avatar | INFO | Breakpoint hit for Target: hardware
2023-08-21 14:01:05,232 | avatar | INFO | Received state update of target hardware to TargetStates.BREAKPOINT
2023-08-21 14:01:05,232 | avatar.targets.hardware | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:01:05,233 | avatar | INFO | Received state update of target hardware to TargetStates.STOPPED
breakpoint hit
Vector table is at address 0x20000000
Vector table:
  Address      [irq--num] --  target      --  label
    0x20000000 [ 0 / -16] --  0x20042000  --  __END_OF_RAM__
    0x20000004 [ 1 / -15] --  0x100001f7  --  <_reset_handler>
    0x20000008 [ 2 / -14] --  0x100001c3  --  <isr_nmi>
    0x2000000c [ 3 / -13] --  0x100001c5  --  <isr_hardfault>
    0x20000010 [ 4 / -12] --  0x100001c1  --  <isr_invalid>
    0x20000014 [ 5 / -11] --  0x100001c1  --  <isr_invalid>
    0x20000018 [ 6 / -10] --  0x100001c1  --  <isr_invalid>
    0x2000001c [ 7 /  -9] --  0x100001c1  --  <isr_invalid>
    0x20000020 [ 8 /  -8] --  0x100001c1  --  <isr_invalid>
    0x20000024 [ 9 /  -7] --  0x100001c1  --  <isr_invalid>
    0x20000028 [10 /  -6] --  0x100001c1  --  <isr_invalid>
    0x2000002c [11 /  -5] --  0x100001c7  --  <isr_svcall>
    0x20000030 [12 /  -4] --  0x100001c1  --  <isr_invalid>
    0x20000034 [13 /  -3] --  0x100001c1  --  <isr_invalid>
    0x20000038 [14 /  -2] --  0x100001c9  --  <isr_pendsv>
    0x2000003c [15 /  -1] --  0x100001cb  --  <isr_systick>
    0x20000040 [16 /   0] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000044 [17 /   1] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000048 [18 /   2] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000004c [19 /   3] --  0x100016dd  --  unknown
    0x20000050 [20 /   4] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000054 [21 /   5] --  0x20000afd  --  <irq_handler_chain_slots> -> <dcd_rp2040_irq>
    0x20000058 [22 /   6] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000005c [23 /   7] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000060 [24 /   8] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000064 [25 /   9] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000068 [26 /  10] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000006c [27 /  11] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000070 [28 /  12] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000074 [29 /  13] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000078 [30 /  14] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000007c [31 /  15] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000080 [32 /  16] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000084 [33 /  17] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000088 [34 /  18] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000008c [35 /  19] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000090 [36 /  20] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000094 [37 /  21] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000098 [38 /  22] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000009c [39 /  23] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000a0 [40 /  24] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000a4 [41 /  25] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000a8 [42 /  26] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000ac [43 /  27] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000b0 [44 /  28] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000b4 [45 /  29] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000b8 [46 /  30] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000bc [47 /  31] --  0x10004c8d  --  unknown
2023-08-21 14:01:05,262 | script | DEBUG | Syncing targets
2023-08-21 14:01:05,678 | avatar | INFO | Synchronized Registers
2023-08-21 14:01:27,104 | avatar | INFO | Synchronized Memory Range: RAM
2023-08-21 14:01:27,104 | script | DEBUG | Registering interrupt handlers
2023-08-21 14:01:27,105 | script | DEBUG | Enabling interrupts
2023-08-21 14:01:27,124 | avatar.protocols.ARMV7HALCallerProtocol | INFO | Enabling ARMv7 HAL catching
2023-08-21 14:01:27,125 | avatar.protocols.ARMV7HALCallerProtocol | WARNING | Injecting HAL caller stub into hardware at address 0x20012000.
2023-08-21 14:01:27,125 | avatar.protocols.ARMV7HALCallerProtocol | INFO | _stub_base              = 0x20012000
2023-08-21 14:01:27,125 | avatar.protocols.ARMV7HALCallerProtocol | INFO | _stub_func_ptr          = 0x20012000
2023-08-21 14:01:27,125 | avatar.protocols.ARMV7HALCallerProtocol | INFO | _stub_entry             = 0x20012000
2023-08-21 14:01:27,125 | avatar.protocols.ARMV7HALCallerProtocol | INFO | _stub_end               = 0x2001200a
2023-08-21 14:01:27,125 | avatar.protocols.ARMV7HALCallerProtocol | INFO | Injecting the stub ...
2023-08-21 14:01:27,125 | avatar | WARNING | Injecting assembly into address 0x20012000
2023-08-21 14:01:27,131 | avatar.protocols.ARMV7HALCallerProtocol | INFO | Starting ARMv7 HAL catching thread
2023-08-21 14:01:27,132 | avatar.protocols.ARMV7HALCallerProtocol | INFO | Starting ARMV7HALCallerProtocol thread
2023-08-21 14:01:27,132 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Enabling QEmu HAL catching
2023-08-21 14:01:27,132 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Setting breakpoint at 0x20000450
2023-08-21 14:01:27,155 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Setting breakpoint at 0x10005dd0
2023-08-21 14:01:27,175 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Starting QemuARMV7HALCallerProtocol thread
2023-08-21 14:01:27,176 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Starting QEmu HAL catching thread
2023-08-21 14:01:27,179 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Setting vector table base to 0x20000000
2023-08-21 14:01:27,180 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-set-vector-table-base", "arguments": {"base": 536870912, "num-cpu": 0}, "id": 1}\r\n'
2023-08-21 14:01:27,184 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 1}\r\n'
2023-08-21 14:01:27,188 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Setting enabled interrupts to 0x80000028
2023-08-21 14:01:27,189 | emulated | DEBUG | pc=0x0 NOT forwarded memory write at 0xe000e100 with size 4 and value 2147483688 (0x80000028) of QemuTarget
2023-08-21 14:01:27,196 | avatar.protocols.ARMV7InterruptProtocol | INFO | Enabling interrupts
2023-08-21 14:01:27,203 | avatar.protocols.ARMV7InterruptProtocol | WARNING | Injecting monitor stub into hardware. (IVT: 0x20000000, 0x20000000, 0x20011000)
2023-08-21 14:01:27,203 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_addr     = 0x20010000
2023-08-21 14:01:27,203 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_base     = 0x20010200
2023-08-21 14:01:27,203 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_loop     = 0x20010208
2023-08-21 14:01:27,203 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_isr      = 0x2001020c
2023-08-21 14:01:27,203 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_writeme  = 0x20010204
2023-08-21 14:01:27,206 | avatar.protocols.ARMV7InterruptProtocol | WARNING | Changing VTOR location to 0x20011000
2023-08-21 14:01:27,207 | emulated | DEBUG | pc=0x0 NOT forwarded memory write at 0xe000ed08 with size 4 and value 536940544 (0x20011000) of OpenOCDTarget
2023-08-21 14:01:27,214 | avatar.protocols.ARMV7InterruptProtocol | INFO | Validate new VTOR address 0x20011000
2023-08-21 14:01:27,214 | avatar.protocols.ARMV7InterruptProtocol | INFO | Inserting the stub ...
2023-08-21 14:01:27,214 | avatar | WARNING | Injecting assembly into address 0x20010000
2023-08-21 14:01:27,242 | avatar.protocols.ARMV7InterruptProtocol | INFO | Setting up IVT...
2023-08-21 14:01:27,494 | avatar.protocols.ARMV7InterruptProtocol | WARNING | Updated PC to 0x20010208
2023-08-21 14:01:27,494 | avatar.protocols.ARMV7InterruptProtocol | INFO | Starting interrupt thread
2023-08-21 14:01:27,494 | avatar.protocols.ARMV7InterruptProtocol | INFO | Starting ARMV7InterruptProtocol thread
2023-08-21 14:01:27,495 | avatar | INFO | ISR is at 0x2001020b
2023-08-21 14:01:27,495 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-enable-irq", "arguments": {"irq-rx-queue-name": "/avatar_v7m_irq_tx_queue", "irq-tx-queue-name": "/avatar_v7m_irq_rx_queue", "rmem-rx-queue-name": "/qemu_rx_queue", "rmem-tx-queue-name": "/qemu_tx_queue"}, "id": 2}\r\n'
2023-08-21 14:01:27,501 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 2}\r\n'
2023-08-21 14:01:27,503 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Enabled Interrupt Forwarding for <avatar2.targets.qemu_target.QemuTarget object at 0x7f0024eea4d0>

=================== Finished setting up interrupt rehosting =========================

Vector table is at address 0x20011000
Vector table:
  Address      [irq--num] --  target      --  label
    0x20011000 [ 0 / -16] --  0x20042000  --  __END_OF_RAM__
    0x20011004 [ 1 / -15] --  0x2001020d  --  STUB
    0x20011008 [ 2 / -14] --  0x2001020d  --  STUB
    0x2001100c [ 3 / -13] --  0x2001020d  --  STUB
    0x20011010 [ 4 / -12] --  0x2001020d  --  STUB
    0x20011014 [ 5 / -11] --  0x2001020d  --  STUB
    0x20011018 [ 6 / -10] --  0x2001020d  --  STUB
    0x2001101c [ 7 /  -9] --  0x2001020d  --  STUB
    0x20011020 [ 8 /  -8] --  0x2001020d  --  STUB
    0x20011024 [ 9 /  -7] --  0x2001020d  --  STUB
    0x20011028 [10 /  -6] --  0x2001020d  --  STUB
    0x2001102c [11 /  -5] --  0x2001020d  --  STUB
    0x20011030 [12 /  -4] --  0x2001020d  --  STUB
    0x20011034 [13 /  -3] --  0x2001020d  --  STUB
    0x20011038 [14 /  -2] --  0x2001020d  --  STUB
    0x2001103c [15 /  -1] --  0x2001020d  --  STUB
    0x20011040 [16 /   0] --  0x2001020d  --  STUB
    0x20011044 [17 /   1] --  0x2001020d  --  STUB
    0x20011048 [18 /   2] --  0x2001020d  --  STUB
    0x2001104c [19 /   3] --  0x2001020d  --  STUB
    0x20011050 [20 /   4] --  0x2001020d  --  STUB
    0x20011054 [21 /   5] --  0x2001020d  --  STUB
    0x20011058 [22 /   6] --  0x2001020d  --  STUB
    0x2001105c [23 /   7] --  0x2001020d  --  STUB
    0x20011060 [24 /   8] --  0x2001020d  --  STUB
    0x20011064 [25 /   9] --  0x2001020d  --  STUB
    0x20011068 [26 /  10] --  0x2001020d  --  STUB
    0x2001106c [27 /  11] --  0x2001020d  --  STUB
    0x20011070 [28 /  12] --  0x2001020d  --  STUB
    0x20011074 [29 /  13] --  0x2001020d  --  STUB
    0x20011078 [30 /  14] --  0x2001020d  --  STUB
    0x2001107c [31 /  15] --  0x2001020d  --  STUB
    0x20011080 [32 /  16] --  0x2001020d  --  STUB
    0x20011084 [33 /  17] --  0x2001020d  --  STUB
    0x20011088 [34 /  18] --  0x2001020d  --  STUB
    0x2001108c [35 /  19] --  0x2001020d  --  STUB
    0x20011090 [36 /  20] --  0x2001020d  --  STUB
    0x20011094 [37 /  21] --  0x2001020d  --  STUB
    0x20011098 [38 /  22] --  0x2001020d  --  STUB
    0x2001109c [39 /  23] --  0x2001020d  --  STUB
    0x200110a0 [40 /  24] --  0x2001020d  --  STUB
    0x200110a4 [41 /  25] --  0x2001020d  --  STUB
    0x200110a8 [42 /  26] --  0x2001020d  --  STUB
    0x200110ac [43 /  27] --  0x2001020d  --  STUB
    0x200110b0 [44 /  28] --  0x2001020d  --  STUB
    0x200110b4 [45 /  29] --  0x2001020d  --  STUB
    0x200110b8 [46 /  30] --  0x2001020d  --  STUB
    0x200110bc [47 /  31] --  0x2001020d  --  STUB
Hardware NVIC
!!! Accessing raw NVIC state !!!
>   Enabled Interrupts (ISER): 0x80000028
>   Pending Interrupts (ISPR): 0x18060
>   Interrupt Control and State Register (ICSR): 0xc15000
>   Vector Table Offset Register (VTOR): 0x20011000
>   Pending Interrupt 21
QEmu NVIC
!!! Accessing raw NVIC state !!!
>   Enabled Interrupts (ISER): 0x80000028
>   Pending Interrupts (ISPR): 0x0
>   Interrupt Control and State Register (ICSR): 0x800
>   Vector Table Offset Register (VTOR): 0x20000000

=================== Continuing... =========================

HARDWARE: 0x20011000
2023-08-21 14:01:27,797 | emulated | DEBUG | pc=0x0 NOT forwarded memory read at 0xe000ed08 with size 4 of QemuTarget
QEmu    : 0x20000000

2023-08-21 14:01:27,804 | avatar.targets.qemu.GDBProtocol | INFO | Continuing execution of qemu
2023-08-21 14:01:27,814 | avatar.targets.qemu | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:01:27,814 | avatar.targets.qemu.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1091, 'stream': 'stdout'}, returning True
2023-08-21 14:01:27,816 | avatar | INFO | Received state update of target qemu to TargetStates.RUNNING
2023-08-21 14:01:27,817 | avatar.targets.hardware.GDBProtocol | INFO | Continuing execution of hardware
2023-08-21 14:01:27,827 | avatar.targets.hardware.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1086, 'stream': 'stdout'}, returning True
2023-08-21 14:01:27,828 | avatar.targets.hardware | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:01:27,830 | avatar | INFO | Received state update of target hardware to TargetStates.RUNNING

=================== Dropping in interactive session =========================

2023-08-21 14:01:27,834 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:01:27,835 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:01:27,839 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:01:27,840 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:27,840 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:01:27,841 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 3}\r\n'
2023-08-21 14:01:27,844 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 3}\r\n'
2023-08-21 14:01:27,846 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:01:27,846 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:01:27,847 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:27,848 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 0
2023-08-21 14:01:27,878 | avatar.targets.qemu | INFO | State changed to TargetStates.BREAKPOINT
2023-08-21 14:01:27,879 | avatar | INFO | Breakpoint hit for Target: qemu
2023-08-21 14:01:27,880 | avatar | INFO | Received state update of target qemu to TargetStates.BREAKPOINT
2023-08-21 14:01:27,880 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Dispatching HALEnterMessage for function at 0x20000450
2023-08-21 14:01:27,916 | avatar.plugins.HALCaller | WARNING | hal_enter called with HALEnterMessage from qemu returning to 0x20000b01
2023-08-21 14:01:27,916 | avatar.protocols.ARMV7InterruptProtocol | WARNING | IRQ protocol paused
2023-08-21 14:01:27,926 | avatar.protocols.ARMV7HALCallerProtocol | WARNING | _do_hal_call (func=0x20000450, args = [])...
2023-08-21 14:01:27,925 | avatar.targets.qemu | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:01:27,936 | avatar | INFO | Received state update of target qemu to TargetStates.STOPPED
2023-08-21 14:01:28,073 | avatar.targets.hardware | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:01:28,109 | avatar | INFO | Received state update of target hardware to TargetStates.STOPPED
2023-08-21 14:01:28,445 | avatar.targets.hardware.GDBProtocol | INFO | Continuing execution of hardware
2023-08-21 14:01:28,463 | avatar.targets.hardware.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1088, 'stream': 'stdout'}, returning True
2023-08-21 14:01:28,463 | avatar.targets.hardware | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:01:28,482 | avatar | INFO | Received state update of target hardware to TargetStates.RUNNING
Python 3.10.9 (main, Jan 11 2023, 15:21:40) [GCC 11.2.0]
Type 'copyright', 'credits' or 'license' for more information
IPython 8.8.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: 2023-08-21 14:01:28,568 | avatar.targets.hardware | INFO | State changed to TargetStates.BREAKPOINT
2023-08-21 14:01:28,568 | avatar | INFO | Breakpoint hit for Target: hardware
2023-08-21 14:01:28,568 | avatar | INFO | Received state update of target hardware to TargetStates.BREAKPOINT
2023-08-21 14:01:28,636 | avatar.plugins.HALCaller | WARNING | hal_exit called with return val 21 to 0x20000b01
2023-08-21 14:01:28,636 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Continuing QEmu, injecting return value 21 and continuing at 0x20000b01
2023-08-21 14:01:28,636 | avatar.protocols.QemuARMV7HALCallerProtocol | WARNING | Return value of function is void, skipping return value injection
2023-08-21 14:01:28,636 | avatar.targets.hardware.GDBProtocol | INFO | Continuing execution of hardware
2023-08-21 14:01:28,647 | avatar.targets.hardware.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1089, 'stream': 'stdout'}, returning True
2023-08-21 14:01:28,657 | avatar.protocols.ARMV7InterruptProtocol | WARNING | IRQ protocol resuming...
2023-08-21 14:01:28,658 | avatar.targets.hardware | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:01:28,658 | avatar.targets.qemu.GDBProtocol | INFO | Continuing execution of qemu
2023-08-21 14:01:28,660 | avatar | INFO | Received state update of target hardware to TargetStates.STOPPED
2023-08-21 14:01:28,659 | avatar.targets.hardware | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:01:28,661 | avatar | INFO | Received state update of target hardware to TargetStates.RUNNING
2023-08-21 14:01:28,664 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:01:28,667 | avatar.targets.qemu | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:01:28,668 | avatar | INFO | Received state update of target qemu to TargetStates.RUNNING
2023-08-21 14:01:28,668 | avatar.targets.qemu.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1095, 'stream': 'stdout'}, returning True
2023-08-21 14:01:28,669 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:01:28,669 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:28,669 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:01:28,673 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 2
2023-08-21 14:01:28,673 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:01:28,675 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:28,676 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:28,676 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:28,676 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:28,676 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:28,676 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 4}\r\n'
2023-08-21 14:01:28,688 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 4}\r\n'
2023-08-21 14:01:28,689 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:28,689 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:28,690 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:28,690 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 3
2023-08-21 14:01:28,759 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:01:28,798 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:28,799 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:28,799 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:28,803 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 5
2023-08-21 14:01:28,803 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:28,805 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:01:28,806 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:01:28,806 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:01:28,806 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:28,806 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:01:28,807 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 5}\r\n'
2023-08-21 14:01:28,811 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 5}\r\n'
2023-08-21 14:01:28,811 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:01:28,873 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:01:28,873 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:28,873 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 6
2023-08-21 14:01:28,891 | avatar.targets.qemu | INFO | State changed to TargetStates.BREAKPOINT
2023-08-21 14:01:28,891 | avatar | INFO | Breakpoint hit for Target: qemu
2023-08-21 14:01:28,892 | avatar | INFO | Received state update of target qemu to TargetStates.BREAKPOINT
2023-08-21 14:01:28,892 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Dispatching HALEnterMessage for function at 0x20000450
2023-08-21 14:01:28,902 | avatar.plugins.HALCaller | WARNING | hal_enter called with HALEnterMessage from qemu returning to 0x20000b01
2023-08-21 14:01:28,902 | avatar.protocols.ARMV7InterruptProtocol | WARNING | IRQ protocol paused
2023-08-21 14:01:28,902 | avatar.targets.qemu | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:01:28,903 | avatar.protocols.ARMV7HALCallerProtocol | WARNING | _do_hal_call (func=0x20000450, args = [])...
2023-08-21 14:01:28,904 | avatar | INFO | Received state update of target qemu to TargetStates.STOPPED
2023-08-21 14:01:28,931 | avatar.targets.hardware | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:01:28,931 | avatar | INFO | Received state update of target hardware to TargetStates.STOPPED
2023-08-21 14:01:28,985 | avatar.targets.hardware.GDBProtocol | INFO | Continuing execution of hardware
2023-08-21 14:01:28,992 | avatar.targets.hardware.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1091, 'stream': 'stdout'}, returning True
2023-08-21 14:01:28,993 | avatar.targets.hardware | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:01:28,994 | avatar | INFO | Received state update of target hardware to TargetStates.RUNNING
2023-08-21 14:01:29,044 | avatar.targets.hardware | INFO | State changed to TargetStates.BREAKPOINT
2023-08-21 14:01:29,044 | avatar | INFO | Breakpoint hit for Target: hardware
2023-08-21 14:01:29,044 | avatar | INFO | Received state update of target hardware to TargetStates.BREAKPOINT
2023-08-21 14:01:29,094 | avatar.plugins.HALCaller | WARNING | hal_exit called with return val 21 to 0x20000b01
2023-08-21 14:01:29,094 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Continuing QEmu, injecting return value 21 and continuing at 0x20000b01
2023-08-21 14:01:29,095 | avatar.protocols.QemuARMV7HALCallerProtocol | WARNING | Return value of function is void, skipping return value injection
2023-08-21 14:01:29,095 | avatar.targets.hardware.GDBProtocol | INFO | Continuing execution of hardware
2023-08-21 14:01:29,105 | avatar.targets.hardware.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1092, 'stream': 'stdout'}, returning True
2023-08-21 14:01:29,127 | avatar.protocols.ARMV7InterruptProtocol | WARNING | IRQ protocol resuming...
2023-08-21 14:01:29,128 | avatar.targets.hardware | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:01:29,129 | avatar.targets.hardware | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:01:29,127 | avatar.targets.qemu.GDBProtocol | INFO | Continuing execution of qemu
2023-08-21 14:01:29,129 | avatar | INFO | Received state update of target hardware to TargetStates.STOPPED
2023-08-21 14:01:29,130 | avatar | INFO | Received state update of target hardware to TargetStates.RUNNING
2023-08-21 14:01:29,133 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:01:29,136 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:01:29,137 | avatar.targets.qemu.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1099, 'stream': 'stdout'}, returning True
2023-08-21 14:01:29,137 | avatar.targets.qemu | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:01:29,138 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:29,138 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:01:29,138 | avatar | INFO | Received state update of target qemu to TargetStates.RUNNING
2023-08-21 14:01:29,142 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 8
2023-08-21 14:01:29,142 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:01:29,145 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:01:29,145 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:01:29,146 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:01:29,146 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:29,146 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:01:29,146 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 6}\r\n'
2023-08-21 14:01:29,153 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 6}\r\n'
2023-08-21 14:01:29,153 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=unknown
2023-08-21 14:01:29,153 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:01:29,154 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:29,154 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 9
2023-08-21 14:01:29,187 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:01:29,188 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:29,188 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:01:29,192 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 10
2023-08-21 14:01:29,192 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:01:29,744 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:29,744 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:29,744 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:29,744 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:29,745 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:29,745 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 7}\r\n'
2023-08-21 14:01:29,758 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 7}\r\n'
2023-08-21 14:01:29,760 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:29,758 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:29,761 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:29,761 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 11
2023-08-21 14:01:29,868 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:29,869 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:29,869 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:29,873 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 12
2023-08-21 14:01:29,873 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:31,365 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:31,366 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:31,366 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:31,366 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:31,366 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:31,366 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 8}\r\n'
2023-08-21 14:01:31,373 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 8}\r\n'
2023-08-21 14:01:31,374 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:31,373 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:31,374 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:31,375 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 13
2023-08-21 14:01:31,469 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:31,470 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:31,470 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:31,474 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 14
2023-08-21 14:01:31,475 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:32,942 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:32,942 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:32,942 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:32,943 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:32,943 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:32,943 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 9}\r\n'
2023-08-21 14:01:32,969 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 9}\r\n'
2023-08-21 14:01:32,970 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:32,987 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:32,988 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:32,988 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 15
2023-08-21 14:01:33,090 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:33,090 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:33,090 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:33,095 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 16
2023-08-21 14:01:33,096 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:34,531 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:34,531 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:34,531 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:34,531 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:34,531 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:34,532 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 10}\r\n'
2023-08-21 14:01:34,543 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 10}\r\n'
2023-08-21 14:01:34,544 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:34,549 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:34,549 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:34,549 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 17
2023-08-21 14:01:34,646 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:34,646 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:34,647 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:34,651 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 18
2023-08-21 14:01:34,651 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:36,106 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:36,107 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:36,107 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:36,107 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:36,107 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:36,108 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 11}\r\n'
2023-08-21 14:01:36,115 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 11}\r\n'
2023-08-21 14:01:36,115 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:36,115 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:36,116 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:36,116 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 19
2023-08-21 14:01:36,212 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:36,212 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:36,212 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:36,218 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 20
2023-08-21 14:01:36,218 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:37,686 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:37,687 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:37,687 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:37,687 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:37,687 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:37,687 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 12}\r\n'
2023-08-21 14:01:37,694 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 12}\r\n'
2023-08-21 14:01:37,694 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:37,699 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:37,699 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:37,699 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 21
2023-08-21 14:01:37,794 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:37,795 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:37,795 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:37,799 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 22
2023-08-21 14:01:37,800 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:39,248 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:39,249 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:39,249 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:39,249 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:39,249 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:39,249 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 13}\r\n'
2023-08-21 14:01:39,271 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 13}\r\n'
2023-08-21 14:01:39,272 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:39,287 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:39,287 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:39,287 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 23
2023-08-21 14:01:39,383 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:39,383 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:39,383 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:39,387 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 24
2023-08-21 14:01:39,388 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:40,821 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:40,822 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:40,822 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:40,822 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:40,822 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:40,823 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 14}\r\n'
2023-08-21 14:01:40,832 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 14}\r\n'
2023-08-21 14:01:40,834 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:40,838 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:40,839 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:40,839 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 25
2023-08-21 14:01:40,955 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:40,956 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:40,956 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:40,960 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 26
2023-08-21 14:01:40,960 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:42,436 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:42,436 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:42,436 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:42,437 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:42,437 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:42,437 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 15}\r\n'
2023-08-21 14:01:42,448 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 15}\r\n'
2023-08-21 14:01:42,449 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:42,463 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:42,463 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:42,463 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 27
2023-08-21 14:01:42,556 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:42,557 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:42,557 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:42,561 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 28
2023-08-21 14:01:42,561 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:43,985 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:43,985 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:43,985 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:43,985 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:43,985 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:43,986 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 16}\r\n'
2023-08-21 14:01:43,997 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 16}\r\n'
2023-08-21 14:01:43,998 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:44,002 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:44,002 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:44,003 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 29
2023-08-21 14:01:44,095 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:44,096 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:44,096 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:44,100 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 30
2023-08-21 14:01:44,101 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:45,544 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:45,545 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:45,545 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:45,545 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:45,545 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:45,546 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 17}\r\n'
2023-08-21 14:01:45,552 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 17}\r\n'
2023-08-21 14:01:45,553 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:45,553 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:45,554 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:45,554 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 31
2023-08-21 14:01:45,645 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:45,646 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:45,646 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:45,650 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 32
2023-08-21 14:01:45,650 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:47,172 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:47,172 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:47,173 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:47,173 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:47,173 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:47,173 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 18}\r\n'
2023-08-21 14:01:47,185 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 18}\r\n'
2023-08-21 14:01:47,186 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:47,190 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:47,190 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:47,190 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 33
2023-08-21 14:01:47,283 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:47,283 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:47,283 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:47,288 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 34
2023-08-21 14:01:47,288 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:48,789 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:48,789 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:48,789 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:48,790 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:48,790 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:48,790 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 19}\r\n'
2023-08-21 14:01:48,802 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 19}\r\n'
2023-08-21 14:01:48,802 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:48,806 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:48,807 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:48,807 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 35
2023-08-21 14:01:48,900 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:48,901 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:48,901 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:48,905 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 36
2023-08-21 14:01:48,905 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:50,416 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:50,416 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:50,416 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:50,416 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:50,416 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:50,417 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 20}\r\n'
2023-08-21 14:01:50,440 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 20}\r\n'
2023-08-21 14:01:50,441 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:50,455 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:50,455 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:50,455 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 37
2023-08-21 14:01:50,549 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:50,549 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:50,549 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:50,554 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 38
2023-08-21 14:01:50,554 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:52,056 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:52,056 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:52,056 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:52,056 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:52,057 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:52,057 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 21}\r\n'
2023-08-21 14:01:52,074 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 21}\r\n'
2023-08-21 14:01:52,074 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:52,089 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:52,089 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:52,090 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 39
2023-08-21 14:01:52,186 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:52,186 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:52,186 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:52,190 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 40
2023-08-21 14:01:52,191 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:53,685 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:53,685 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:53,685 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:53,685 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:53,685 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:53,686 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 22}\r\n'
2023-08-21 14:01:53,708 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 22}\r\n'
2023-08-21 14:01:53,708 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:53,725 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:53,725 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:53,726 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 41
2023-08-21 14:01:53,818 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:53,819 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:53,819 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:53,825 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 42
2023-08-21 14:01:53,826 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:55,318 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:55,318 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:55,318 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:55,318 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:55,318 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:55,319 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 23}\r\n'
2023-08-21 14:01:55,328 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 23}\r\n'
2023-08-21 14:01:55,328 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:55,333 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:55,333 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:55,333 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 43
2023-08-21 14:01:55,431 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:55,432 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:55,432 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:55,436 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 44
2023-08-21 14:01:55,437 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:56,958 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:56,958 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:56,959 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:56,959 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:56,959 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:56,959 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 24}\r\n'
2023-08-21 14:01:57,073 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 24}\r\n'
2023-08-21 14:01:57,073 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:57,078 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:57,078 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:57,078 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 45
2023-08-21 14:01:57,175 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:57,176 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:57,176 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:57,180 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 46
2023-08-21 14:01:57,180 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:01:58,589 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:01:58,589 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:01:58,589 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:01:58,589 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:01:58,589 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:01:58,589 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 25}\r\n'
2023-08-21 14:01:58,611 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 25}\r\n'
2023-08-21 14:01:58,611 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:01:58,628 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:01:58,629 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:01:58,629 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 47
2023-08-21 14:01:58,725 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:01:58,726 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:01:58,726 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:01:58,730 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 48
2023-08-21 14:01:58,731 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:00,632 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:00,632 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:00,632 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:00,632 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:00,633 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:00,633 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 26}\r\n'
2023-08-21 14:02:00,644 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 26}\r\n'
2023-08-21 14:02:00,645 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:00,659 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:00,659 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:00,659 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 49
2023-08-21 14:02:00,757 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:00,757 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:00,758 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:00,762 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 50
2023-08-21 14:02:00,762 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:02,185 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:02,185 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:02,186 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:02,186 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:02,186 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:02,186 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 27}\r\n'
2023-08-21 14:02:02,193 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 27}\r\n'
2023-08-21 14:02:02,193 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:02,208 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:02,209 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:02,209 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 51
2023-08-21 14:02:02,305 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:02,306 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:02,306 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:02,310 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 52
2023-08-21 14:02:02,311 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:03,735 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:03,736 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:03,736 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:03,736 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:03,736 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:03,736 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 28}\r\n'
2023-08-21 14:02:03,753 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 28}\r\n'
2023-08-21 14:02:03,754 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:03,768 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:03,768 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:03,769 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 53
2023-08-21 14:02:03,866 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:03,867 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:03,867 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:03,871 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 54
2023-08-21 14:02:03,872 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:05,294 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:05,295 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:05,295 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:05,295 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:05,295 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:05,295 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 29}\r\n'
2023-08-21 14:02:05,317 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 29}\r\n'
2023-08-21 14:02:05,317 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:05,333 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:05,333 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:05,334 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 55
2023-08-21 14:02:05,430 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:05,430 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:05,431 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:05,435 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 56
2023-08-21 14:02:05,435 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:06,865 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:06,865 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:06,865 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:06,865 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:06,865 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:06,866 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 30}\r\n'
2023-08-21 14:02:06,913 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 30}\r\n'
2023-08-21 14:02:06,913 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:06,918 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:06,918 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:06,918 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 57
2023-08-21 14:02:07,017 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:07,017 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:07,017 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:07,022 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 58
2023-08-21 14:02:07,022 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:08,423 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:08,423 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:08,424 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:08,424 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:08,424 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:08,424 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 31}\r\n'
2023-08-21 14:02:08,452 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 31}\r\n'
2023-08-21 14:02:08,453 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:08,467 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:08,467 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:08,467 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 59
2023-08-21 14:02:08,563 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:08,563 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:08,563 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:08,567 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 60
2023-08-21 14:02:08,568 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:09,970 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:09,970 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:09,971 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:09,971 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:09,971 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:09,971 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 32}\r\n'
2023-08-21 14:02:10,019 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 32}\r\n'
2023-08-21 14:02:10,019 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:10,023 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:10,024 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:10,024 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 61
2023-08-21 14:02:10,150 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:10,150 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:10,151 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:10,154 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 62
2023-08-21 14:02:10,155 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:11,548 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:11,548 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:11,548 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:11,549 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:11,549 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:11,549 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 33}\r\n'
2023-08-21 14:02:11,606 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 33}\r\n'
2023-08-21 14:02:11,607 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:11,611 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:11,611 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:11,612 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 63
2023-08-21 14:02:11,712 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:11,712 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:11,713 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:11,717 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 64
2023-08-21 14:02:11,717 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:13,121 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:13,122 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:13,122 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:13,122 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:13,122 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:13,122 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 34}\r\n'
2023-08-21 14:02:13,136 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 34}\r\n'
2023-08-21 14:02:13,137 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:13,141 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:13,141 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:13,142 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 65
2023-08-21 14:02:13,239 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:13,239 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:13,239 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:13,244 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 66
2023-08-21 14:02:13,244 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:14,692 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:14,693 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:14,693 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:14,693 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:14,693 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:14,693 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 35}\r\n'
2023-08-21 14:02:14,722 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 35}\r\n'
2023-08-21 14:02:14,722 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:14,741 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:14,742 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:14,742 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 67
2023-08-21 14:02:14,848 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:14,849 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:14,851 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:14,855 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 68
2023-08-21 14:02:14,855 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:02:16,292 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:02:16,293 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:02:16,293 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:02:16,293 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:02:16,293 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:02:16,293 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 36}\r\n'
2023-08-21 14:02:16,315 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 36}\r\n'
2023-08-21 14:02:16,316 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=unknown
2023-08-21 14:02:16,334 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:02:16,334 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:02:16,334 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 69
2023-08-21 14:02:16,433 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:02:16,434 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:02:16,434 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:02:16,438 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 70
2023-08-21 14:02:16,438 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
```

## Rehosting with only `tud_task_ext` in HWRunner

### Comments

* If the `dcd_rp2040_irq` is not deferred into the hardware the performance is good enough to prevent the shadowing ->
  it works

### USB log

```
Initialization complete :)
Counter = 0
Counter = 1
Counter = 2
Counter = 3
Counter = 4
Counter = 5
```

### UART log

```
..............
Initialization complete :)
Counter = 0
Counter = 1
Counter = 2
Counter = 3
Counter = 4
Counter = 5
```

### Avatar log

```
❯ openocd -f interface/cmsis-dap.cfg -c "adapter speed 6000" -f target/rp2040.cfg -c "init; reset; exit"
Open On-Chip Debugger 0.12.0+dev-01098-gb6b4f9d46 (2023-04-14-16:58)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
adapter speed: 6000 kHz
Info : Hardware thread awareness created
Info : Hardware thread awareness created
Info : Using CMSIS-DAPv2 interface with VID:PID=0x2e8a:0x000c, serial=E66058388369682C
Info : CMSIS-DAP: SWD supported
Info : CMSIS-DAP: Atomic commands supported
Info : CMSIS-DAP: Test domain timer supported
Info : CMSIS-DAP: FW Version = 2.0.0
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 0 SWDIO/TMS = 0 TDI = 0 TDO = 0 nTRST = 0 nRESET = 0
Info : CMSIS-DAP: Interface ready
Info : clock speed 6000 kHz
Info : SWD DPIDR 0x0bc12477, DLPIDR 0x00000001
Info : SWD DPIDR 0x0bc12477, DLPIDR 0x10000001
Info : [rp2040.core0] Cortex-M0+ r0p1 processor detected
Info : [rp2040.core0] target has 4 breakpoints, 2 watchpoints
Info : [rp2040.core1] Cortex-M0+ r0p1 processor detected
Info : [rp2040.core1] target has 4 breakpoints, 2 watchpoints
Info : starting gdb server for rp2040.core0 on 3333
Info : Listening on port 3333 for gdb connections
❯ openocd -f interface/cmsis-dap.cfg -c "adapter speed 6000" -f target/rp2040.cfg -c "init; reset; exit"
Open On-Chip Debugger 0.12.0+dev-01098-gb6b4f9d46 (2023-04-14-16:58)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
adapter speed: 6000 kHz
Info : Hardware thread awareness created
Info : Hardware thread awareness created
Info : Using CMSIS-DAPv2 interface with VID:PID=0x2e8a:0x000c, serial=E66058388369682C
Info : CMSIS-DAP: SWD supported
Info : CMSIS-DAP: Atomic commands supported
Info : CMSIS-DAP: Test domain timer supported
Info : CMSIS-DAP: FW Version = 2.0.0
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 0 SWDIO/TMS = 0 TDI = 0 TDO = 0 nTRST = 0 nRESET = 0
Info : CMSIS-DAP: Interface ready
Info : clock speed 6000 kHz
Info : SWD DPIDR 0x0bc12477, DLPIDR 0x00000001
Info : SWD DPIDR 0x0bc12477, DLPIDR 0x10000001
Info : [rp2040.core0] Cortex-M0+ r0p1 processor detected
Info : [rp2040.core0] target has 4 breakpoints, 2 watchpoints
Info : [rp2040.core1] Cortex-M0+ r0p1 processor detected
Info : [rp2040.core1] target has 4 breakpoints, 2 watchpoints
Info : starting gdb server for rp2040.core0 on 3333
Info : Listening on port 3333 for gdb connections
❯ python hal-irq_usb_blink.py
IPython custom init...
2023-08-21 14:09:46,758 | script | INFO | Setting up targets
2023-08-21 14:09:46,761 | avatar.targets.qemu | INFO | QEmuTarget using executable '/mnt/dev/thesis/avatar-qemu/build/qemu-system-arm'
2023-08-21 14:09:46,761 | script | INFO | Loading interrupt plugins
2023-08-21 14:09:46,788 | script | DEBUG | Initializing targets
2023-08-21 14:09:48,175 | avatar.targets.hardware | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:09:48,176 | avatar | INFO | Received state update of target hardware to TargetStates.STOPPED
2023-08-21 14:09:48,197 | avatar.targets.hardware | INFO | Successfully connected to OpenOCD target!
2023-08-21 14:09:48,334 | avatar | INFO | Attaching ARMv7 Interrupts protocol to <avatar2.targets.openocd_target.OpenOCDTarget object at 0x7f724c495210>
2023-08-21 14:09:48,334 | avatar.protocols.ARMV7InterruptProtocol | INFO | ARMV7InterruptProtocol initialized
2023-08-21 14:09:48,335 | avatar | INFO | Attaching ARMv7 Interrupt-Recorder protocol to <avatar2.targets.openocd_target.OpenOCDTarget object at 0x7f724c495210>
2023-08-21 14:09:48,335 | avatar.protocols.ARMV7HALCallerProtocol | INFO | ARMV7HALCallerProtocol initialized
2023-08-21 14:09:48,338 | avatar.targets.qemu | INFO | QEMU process running
2023-08-21 14:09:48,726 | avatar.targets.qemu | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:09:48,726 | avatar | INFO | Received state update of target qemu to TargetStates.STOPPED
2023-08-21 14:09:48,737 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "qmp_capabilities", "id": 0}\r\n'
2023-08-21 14:09:48,738 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 0}\r\n'
2023-08-21 14:09:48,739 | avatar.targets.qemu | INFO | Connected to remote target
2023-08-21 14:09:48,739 | avatar.targets.qemu.RemoteMemoryProtocol | INFO | Successfully connected rmp
2023-08-21 14:09:48,740 | avatar | INFO | Attaching ARMv7 Interrupts protocol to <avatar2.targets.qemu_target.QemuTarget object at 0x7f724c496380>
2023-08-21 14:09:48,740 | avatar | INFO | Attaching ARMv7 Interrupt-Recorder protocol to <avatar2.targets.qemu_target.QemuTarget object at 0x7f724c496380>
2023-08-21 14:09:48,740 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | QemuARMV7HALCallerProtocol initialized
2023-08-21 14:09:48,753 | avatar.targets.hardware.GDBProtocol | INFO | Continuing execution of hardware
2023-08-21 14:09:48,763 | avatar.targets.hardware.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 6, 'stream': 'stdout'}, returning True
2023-08-21 14:09:48,763 | avatar.targets.hardware | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:09:48,764 | avatar | INFO | Received state update of target hardware to TargetStates.RUNNING
2023-08-21 14:09:50,367 | avatar.targets.hardware | INFO | State changed to TargetStates.BREAKPOINT
2023-08-21 14:09:50,367 | avatar | INFO | Breakpoint hit for Target: hardware
2023-08-21 14:09:50,367 | avatar | INFO | Received state update of target hardware to TargetStates.BREAKPOINT
2023-08-21 14:09:50,367 | avatar.targets.hardware | INFO | State changed to TargetStates.STOPPED
breakpoint hit
2023-08-21 14:09:50,368 | avatar | INFO | Received state update of target hardware to TargetStates.STOPPED
Vector table is at address 0x20000000
Vector table:
  Address      [irq--num] --  target      --  label
    0x20000000 [ 0 / -16] --  0x20042000  --  __END_OF_RAM__
    0x20000004 [ 1 / -15] --  0x100001f7  --  <_reset_handler>
    0x20000008 [ 2 / -14] --  0x100001c3  --  <isr_nmi>
    0x2000000c [ 3 / -13] --  0x100001c5  --  <isr_hardfault>
    0x20000010 [ 4 / -12] --  0x100001c1  --  <isr_invalid>
    0x20000014 [ 5 / -11] --  0x100001c1  --  <isr_invalid>
    0x20000018 [ 6 / -10] --  0x100001c1  --  <isr_invalid>
    0x2000001c [ 7 /  -9] --  0x100001c1  --  <isr_invalid>
    0x20000020 [ 8 /  -8] --  0x100001c1  --  <isr_invalid>
    0x20000024 [ 9 /  -7] --  0x100001c1  --  <isr_invalid>
    0x20000028 [10 /  -6] --  0x100001c1  --  <isr_invalid>
    0x2000002c [11 /  -5] --  0x100001c7  --  <isr_svcall>
    0x20000030 [12 /  -4] --  0x100001c1  --  <isr_invalid>
    0x20000034 [13 /  -3] --  0x100001c1  --  <isr_invalid>
    0x20000038 [14 /  -2] --  0x100001c9  --  <isr_pendsv>
    0x2000003c [15 /  -1] --  0x100001cb  --  <isr_systick>
    0x20000040 [16 /   0] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000044 [17 /   1] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000048 [18 /   2] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000004c [19 /   3] --  0x100016dd  --  <hardware_alarm_irq_handler>
    0x20000050 [20 /   4] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000054 [21 /   5] --  0x20000afd  --  <irq_handler_chain_slots> -> <dcd_rp2040_irq>
    0x20000058 [22 /   6] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000005c [23 /   7] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000060 [24 /   8] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000064 [25 /   9] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000068 [26 /  10] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000006c [27 /  11] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000070 [28 /  12] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000074 [29 /  13] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000078 [30 /  14] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000007c [31 /  15] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000080 [32 /  16] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000084 [33 /  17] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000088 [34 /  18] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000008c [35 /  19] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000090 [36 /  20] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000094 [37 /  21] --  0x100001cd  --  <__unhandled_user_irq>
    0x20000098 [38 /  22] --  0x100001cd  --  <__unhandled_user_irq>
    0x2000009c [39 /  23] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000a0 [40 /  24] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000a4 [41 /  25] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000a8 [42 /  26] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000ac [43 /  27] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000b0 [44 /  28] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000b4 [45 /  29] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000b8 [46 /  30] --  0x100001cd  --  <__unhandled_user_irq>
    0x200000bc [47 /  31] --  0x10004c8d  --  <low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:09:50,397 | script | DEBUG | Syncing targets
2023-08-21 14:09:50,797 | avatar | INFO | Synchronized Registers
2023-08-21 14:10:12,252 | avatar | INFO | Synchronized Memory Range: RAM
2023-08-21 14:10:12,253 | script | DEBUG | Registering interrupt handlers
2023-08-21 14:10:12,253 | script | DEBUG | Enabling interrupts
2023-08-21 14:10:12,273 | avatar.protocols.ARMV7HALCallerProtocol | INFO | Enabling ARMv7 HAL catching
2023-08-21 14:10:12,273 | avatar.protocols.ARMV7HALCallerProtocol | WARNING | Injecting HAL caller stub into hardware at address 0x20012000.
2023-08-21 14:10:12,273 | avatar.protocols.ARMV7HALCallerProtocol | INFO | _stub_base              = 0x20012000
2023-08-21 14:10:12,273 | avatar.protocols.ARMV7HALCallerProtocol | INFO | _stub_func_ptr          = 0x20012000
2023-08-21 14:10:12,273 | avatar.protocols.ARMV7HALCallerProtocol | INFO | _stub_entry             = 0x20012000
2023-08-21 14:10:12,274 | avatar.protocols.ARMV7HALCallerProtocol | INFO | _stub_end               = 0x2001200a
2023-08-21 14:10:12,274 | avatar.protocols.ARMV7HALCallerProtocol | INFO | Injecting the stub ...
2023-08-21 14:10:12,274 | avatar | WARNING | Injecting assembly into address 0x20012000
2023-08-21 14:10:12,290 | avatar.protocols.ARMV7HALCallerProtocol | INFO | Starting ARMv7 HAL catching thread
2023-08-21 14:10:12,290 | avatar.protocols.ARMV7HALCallerProtocol | INFO | Starting ARMV7HALCallerProtocol thread
2023-08-21 14:10:12,290 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Enabling QEmu HAL catching
2023-08-21 14:10:12,291 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Setting breakpoint at 0x10005dd0
2023-08-21 14:10:12,303 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Starting QemuARMV7HALCallerProtocol thread
2023-08-21 14:10:12,304 | avatar.protocols.QemuARMV7HALCallerProtocol | INFO | Starting QEmu HAL catching thread
2023-08-21 14:10:12,306 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Setting vector table base to 0x20000000
2023-08-21 14:10:12,306 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-set-vector-table-base", "arguments": {"base": 536870912, "num-cpu": 0}, "id": 1}\r\n'
2023-08-21 14:10:12,309 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 1}\r\n'
2023-08-21 14:10:12,312 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Setting enabled interrupts to 0x80000028
2023-08-21 14:10:12,313 | emulated | DEBUG | pc=0x0 NOT forwarded memory write at 0xe000e100 with size 4 and value 2147483688 (0x80000028) of QemuTarget
2023-08-21 14:10:12,323 | avatar.protocols.ARMV7InterruptProtocol | INFO | Enabling interrupts
2023-08-21 14:10:12,330 | avatar.protocols.ARMV7InterruptProtocol | WARNING | Injecting monitor stub into hardware. (IVT: 0x20000000, 0x20000000, 0x20011000)
2023-08-21 14:10:12,330 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_addr     = 0x20010000
2023-08-21 14:10:12,330 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_base     = 0x20010200
2023-08-21 14:10:12,330 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_loop     = 0x20010208
2023-08-21 14:10:12,330 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_isr      = 0x2001020c
2023-08-21 14:10:12,330 | avatar.protocols.ARMV7InterruptProtocol | INFO | _monitor_stub_writeme  = 0x20010204
2023-08-21 14:10:12,332 | avatar.protocols.ARMV7InterruptProtocol | WARNING | Changing VTOR location to 0x20011000
2023-08-21 14:10:12,332 | emulated | DEBUG | pc=0x0 NOT forwarded memory write at 0xe000ed08 with size 4 and value 536940544 (0x20011000) of OpenOCDTarget
2023-08-21 14:10:12,337 | avatar.protocols.ARMV7InterruptProtocol | INFO | Validate new VTOR address 0x20011000
2023-08-21 14:10:12,337 | avatar.protocols.ARMV7InterruptProtocol | INFO | Inserting the stub ...
2023-08-21 14:10:12,337 | avatar | WARNING | Injecting assembly into address 0x20010000
2023-08-21 14:10:12,361 | avatar.protocols.ARMV7InterruptProtocol | INFO | Setting up IVT...
2023-08-21 14:10:12,611 | avatar.protocols.ARMV7InterruptProtocol | WARNING | Updated PC to 0x20010208
2023-08-21 14:10:12,611 | avatar.protocols.ARMV7InterruptProtocol | INFO | Starting interrupt thread
2023-08-21 14:10:12,612 | avatar.protocols.ARMV7InterruptProtocol | INFO | Starting ARMV7InterruptProtocol thread
2023-08-21 14:10:12,612 | avatar | INFO | ISR is at 0x2001020b
2023-08-21 14:10:12,612 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-enable-irq", "arguments": {"irq-rx-queue-name": "/avatar_v7m_irq_tx_queue", "irq-tx-queue-name": "/avatar_v7m_irq_rx_queue", "rmem-rx-queue-name": "/qemu_rx_queue", "rmem-tx-queue-name": "/qemu_tx_queue"}, "id": 2}\r\n'
2023-08-21 14:10:12,617 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 2}\r\n'
2023-08-21 14:10:12,618 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Enabled Interrupt Forwarding for <avatar2.targets.qemu_target.QemuTarget object at 0x7f724c496380>

=================== Finished setting up interrupt rehosting =========================

Vector table is at address 0x20011000
Vector table:
  Address      [irq--num] --  target      --  label
    0x20011000 [ 0 / -16] --  0x20042000  --  __END_OF_RAM__
    0x20011004 [ 1 / -15] --  0x2001020d  --  STUB
    0x20011008 [ 2 / -14] --  0x2001020d  --  STUB
    0x2001100c [ 3 / -13] --  0x2001020d  --  STUB
    0x20011010 [ 4 / -12] --  0x2001020d  --  STUB
    0x20011014 [ 5 / -11] --  0x2001020d  --  STUB
    0x20011018 [ 6 / -10] --  0x2001020d  --  STUB
    0x2001101c [ 7 /  -9] --  0x2001020d  --  STUB
    0x20011020 [ 8 /  -8] --  0x2001020d  --  STUB
    0x20011024 [ 9 /  -7] --  0x2001020d  --  STUB
    0x20011028 [10 /  -6] --  0x2001020d  --  STUB
    0x2001102c [11 /  -5] --  0x2001020d  --  STUB
    0x20011030 [12 /  -4] --  0x2001020d  --  STUB
    0x20011034 [13 /  -3] --  0x2001020d  --  STUB
    0x20011038 [14 /  -2] --  0x2001020d  --  STUB
    0x2001103c [15 /  -1] --  0x2001020d  --  STUB
    0x20011040 [16 /   0] --  0x2001020d  --  STUB
    0x20011044 [17 /   1] --  0x2001020d  --  STUB
    0x20011048 [18 /   2] --  0x2001020d  --  STUB
    0x2001104c [19 /   3] --  0x2001020d  --  STUB
    0x20011050 [20 /   4] --  0x2001020d  --  STUB
    0x20011054 [21 /   5] --  0x2001020d  --  STUB
    0x20011058 [22 /   6] --  0x2001020d  --  STUB
    0x2001105c [23 /   7] --  0x2001020d  --  STUB
    0x20011060 [24 /   8] --  0x2001020d  --  STUB
    0x20011064 [25 /   9] --  0x2001020d  --  STUB
    0x20011068 [26 /  10] --  0x2001020d  --  STUB
    0x2001106c [27 /  11] --  0x2001020d  --  STUB
    0x20011070 [28 /  12] --  0x2001020d  --  STUB
    0x20011074 [29 /  13] --  0x2001020d  --  STUB
    0x20011078 [30 /  14] --  0x2001020d  --  STUB
    0x2001107c [31 /  15] --  0x2001020d  --  STUB
    0x20011080 [32 /  16] --  0x2001020d  --  STUB
    0x20011084 [33 /  17] --  0x2001020d  --  STUB
    0x20011088 [34 /  18] --  0x2001020d  --  STUB
    0x2001108c [35 /  19] --  0x2001020d  --  STUB
    0x20011090 [36 /  20] --  0x2001020d  --  STUB
    0x20011094 [37 /  21] --  0x2001020d  --  STUB
    0x20011098 [38 /  22] --  0x2001020d  --  STUB
    0x2001109c [39 /  23] --  0x2001020d  --  STUB
    0x200110a0 [40 /  24] --  0x2001020d  --  STUB
    0x200110a4 [41 /  25] --  0x2001020d  --  STUB
    0x200110a8 [42 /  26] --  0x2001020d  --  STUB
    0x200110ac [43 /  27] --  0x2001020d  --  STUB
    0x200110b0 [44 /  28] --  0x2001020d  --  STUB
    0x200110b4 [45 /  29] --  0x2001020d  --  STUB
    0x200110b8 [46 /  30] --  0x2001020d  --  STUB
    0x200110bc [47 /  31] --  0x2001020d  --  STUB
Hardware NVIC
!!! Accessing raw NVIC state !!!
>   Enabled Interrupts (ISER): 0x80000028
>   Pending Interrupts (ISPR): 0x18060
>   Interrupt Control and State Register (ICSR): 0xc15000
>   Vector Table Offset Register (VTOR): 0x20011000
>   Pending Interrupt 21
QEmu NVIC
!!! Accessing raw NVIC state !!!
>   Enabled Interrupts (ISER): 0x80000028
>   Pending Interrupts (ISPR): 0x0
>   Interrupt Control and State Register (ICSR): 0x800
>   Vector Table Offset Register (VTOR): 0x20000000

=================== Continuing... =========================

HARDWARE: 0x20011000
2023-08-21 14:10:12,826 | emulated | DEBUG | pc=0x0 NOT forwarded memory read at 0xe000ed08 with size 4 of QemuTarget
QEmu    : 0x20000000

2023-08-21 14:10:12,831 | avatar.targets.qemu.GDBProtocol | INFO | Continuing execution of qemu
2023-08-21 14:10:12,841 | avatar.targets.qemu.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1090, 'stream': 'stdout'}, returning True
2023-08-21 14:10:12,841 | avatar.targets.qemu | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:10:12,842 | avatar | INFO | Received state update of target qemu to TargetStates.RUNNING
2023-08-21 14:10:12,842 | avatar.targets.hardware.GDBProtocol | INFO | Continuing execution of hardware
2023-08-21 14:10:12,843 | avatar.targets.hardware | INFO | State changed to TargetStates.RUNNING
2023-08-21 14:10:12,844 | avatar.targets.hardware.GDBProtocol | INFO | Attempted to continue execution on the target. Received response:{'type': 'result', 'message': 'running', 'payload': None, 'token': 1086, 'stream': 'stdout'}, returning True
2023-08-21 14:10:12,853 | avatar | INFO | Received state update of target hardware to TargetStates.RUNNING

=================== Dropping in interactive session =========================

2023-08-21 14:10:12,858 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:12,859 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:12,860 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:12,860 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:12,860 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:12,860 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 3}\r\n'
2023-08-21 14:10:12,865 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 3}\r\n'
2023-08-21 14:10:12,866 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:12,865 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:12,867 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:12,868 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 0
2023-08-21 14:10:13,141 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:13,172 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:13,172 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:13,178 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:13,186 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 2
2023-08-21 14:10:13,186 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:13,202 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:10:13,202 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:10:13,202 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:10:13,202 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:13,203 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:10:13,203 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 4}\r\n'
Python 3.10.9 (main, Jan 11 2023, 15:21:40) [GCC 11.2.0]
Type 'copyright', 'credits' or 'license' for more information
IPython 8.8.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: 2023-08-21 14:10:13,298 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 4}\r\n'
2023-08-21 14:10:13,299 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=<hardware_alarm_irq_handler>
2023-08-21 14:10:13,298 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:10:13,299 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:13,300 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 3
2023-08-21 14:10:13,366 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:13,407 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:10:13,408 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:13,408 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:10:13,412 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 5
2023-08-21 14:10:13,412 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:10:13,415 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:13,415 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:13,415 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:13,415 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:13,416 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:13,416 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 5}\r\n'
2023-08-21 14:10:13,419 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 5}\r\n'
2023-08-21 14:10:13,419 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:13,483 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:13,484 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:13,484 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 6
2023-08-21 14:10:13,504 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:13,510 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:13,510 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:13,511 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:13,515 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 8
2023-08-21 14:10:13,515 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:13,518 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:13,519 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:13,519 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:13,519 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:13,519 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:13,519 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 6}\r\n'
2023-08-21 14:10:13,526 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 6}\r\n'
2023-08-21 14:10:13,527 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:13,526 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:13,527 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:13,528 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 9
2023-08-21 14:10:13,680 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:13,681 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:13,682 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:13,685 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 10
2023-08-21 14:10:13,685 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:13,687 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:13,688 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:13,688 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:13,688 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:13,688 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:13,688 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 7}\r\n'
2023-08-21 14:10:13,695 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 7}\r\n'
2023-08-21 14:10:13,696 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:13,695 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:13,696 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:13,696 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 11
2023-08-21 14:10:13,747 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:13,752 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:13,752 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:13,752 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:13,756 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 13
2023-08-21 14:10:13,757 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:13,759 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:13,759 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:13,760 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:13,760 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:13,760 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:13,760 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 8}\r\n'
2023-08-21 14:10:13,767 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 8}\r\n'
2023-08-21 14:10:13,767 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:13,777 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:13,777 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:13,777 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 14
2023-08-21 14:10:13,797 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:13,802 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:13,802 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:13,802 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:13,807 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 16
2023-08-21 14:10:13,808 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:13,811 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:13,812 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:13,812 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:13,812 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:13,812 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:13,813 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 9}\r\n'
2023-08-21 14:10:13,832 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 9}\r\n'
2023-08-21 14:10:13,832 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:13,832 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:13,833 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:13,833 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 17
2023-08-21 14:10:13,878 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:13,879 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:13,879 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:13,883 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 18
2023-08-21 14:10:13,883 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:13,886 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:10:13,886 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:10:13,886 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:10:13,886 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:13,886 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:10:13,886 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 10}\r\n'
2023-08-21 14:10:13,893 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 10}\r\n'
2023-08-21 14:10:13,894 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=<hardware_alarm_irq_handler>
2023-08-21 14:10:13,893 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:10:13,894 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:13,894 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 19
2023-08-21 14:10:13,992 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:10:13,993 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:13,993 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:10:13,997 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 20
2023-08-21 14:10:13,998 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:10:14,370 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:14,370 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:14,370 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:14,370 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,370 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:14,371 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 11}\r\n'
2023-08-21 14:10:14,378 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 11}\r\n'
2023-08-21 14:10:14,378 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:14,378 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:14,379 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,379 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 21
2023-08-21 14:10:14,393 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,398 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:14,398 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,398 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:14,403 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 23
2023-08-21 14:10:14,403 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:14,405 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:14,406 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:14,406 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:14,406 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,407 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:14,407 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 12}\r\n'
2023-08-21 14:10:14,410 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:14,410 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 12}\r\n'
2023-08-21 14:10:14,411 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:14,411 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,412 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 24
2023-08-21 14:10:14,427 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,432 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:14,432 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,432 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:14,436 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 26
2023-08-21 14:10:14,437 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:14,439 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:14,439 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:14,440 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:14,440 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,440 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:14,440 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 13}\r\n'
2023-08-21 14:10:14,449 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 13}\r\n'
2023-08-21 14:10:14,449 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:14,449 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:14,450 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,450 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 27
2023-08-21 14:10:14,466 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,470 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:14,471 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,471 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:14,475 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 29
2023-08-21 14:10:14,475 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:14,478 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:14,478 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:14,478 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:14,478 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,478 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:14,478 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 14}\r\n'
2023-08-21 14:10:14,486 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 14}\r\n'
2023-08-21 14:10:14,487 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:14,487 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:14,487 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,488 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 30
2023-08-21 14:10:14,502 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,507 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:14,507 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,508 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:14,512 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 32
2023-08-21 14:10:14,512 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:14,514 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:14,514 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:14,514 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:14,515 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,515 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:14,515 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 15}\r\n'
2023-08-21 14:10:14,520 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 15}\r\n'
2023-08-21 14:10:14,520 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:14,521 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:14,521 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,521 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 33
2023-08-21 14:10:14,536 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,540 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:14,541 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,541 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:14,545 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 35
2023-08-21 14:10:14,545 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:14,548 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:14,548 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:14,548 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:14,548 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,548 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:14,548 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 16}\r\n'
2023-08-21 14:10:14,551 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 16}\r\n'
2023-08-21 14:10:14,552 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:14,552 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:14,553 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,553 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 36
2023-08-21 14:10:14,568 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,573 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:14,573 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,573 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:14,578 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 38
2023-08-21 14:10:14,578 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:14,580 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:14,581 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:14,581 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:14,581 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,581 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:14,581 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 17}\r\n'
2023-08-21 14:10:14,589 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 17}\r\n'
2023-08-21 14:10:14,589 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:14,589 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:14,590 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,590 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 39
2023-08-21 14:10:14,604 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,609 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:14,609 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,609 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:14,614 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 41
2023-08-21 14:10:14,614 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:14,618 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:14,619 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:14,620 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:14,620 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,620 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:14,621 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 18}\r\n'
2023-08-21 14:10:14,638 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 18}\r\n'
2023-08-21 14:10:14,638 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:14,638 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:14,639 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,639 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 42
2023-08-21 14:10:14,691 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,696 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:14,696 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,696 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:14,700 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 44
2023-08-21 14:10:14,701 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:14,703 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:14,703 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:14,703 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:14,704 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,704 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:14,704 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 19}\r\n'
2023-08-21 14:10:14,711 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 19}\r\n'
2023-08-21 14:10:14,711 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:14,711 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:14,712 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,712 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 45
2023-08-21 14:10:14,733 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,737 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:14,738 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,738 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:14,743 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 47
2023-08-21 14:10:14,743 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:14,745 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:14,745 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:14,746 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:14,746 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,746 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:14,746 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 20}\r\n'
2023-08-21 14:10:14,758 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 20}\r\n'
2023-08-21 14:10:14,759 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:14,774 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:14,775 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,775 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 48
2023-08-21 14:10:14,849 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,855 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:14,855 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,855 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:14,859 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 50
2023-08-21 14:10:14,860 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:14,862 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:14,862 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:14,862 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:14,862 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,862 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:14,862 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 21}\r\n'
2023-08-21 14:10:14,880 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 21}\r\n'
2023-08-21 14:10:14,880 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:14,890 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:14,891 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,891 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 51
2023-08-21 14:10:14,965 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:14,970 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:14,970 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:14,971 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:14,975 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 53
2023-08-21 14:10:14,975 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:14,977 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:14,978 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:14,978 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:14,978 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:14,978 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:14,978 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 22}\r\n'
2023-08-21 14:10:14,980 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 22}\r\n'
2023-08-21 14:10:14,981 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:14,985 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:14,985 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:14,985 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 54
2023-08-21 14:10:15,057 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:15,061 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:15,062 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:15,062 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:15,066 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 56
2023-08-21 14:10:15,066 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:15,069 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:15,069 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:15,069 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:15,069 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:15,069 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:15,070 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 23}\r\n'
2023-08-21 14:10:15,072 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 23}\r\n'
2023-08-21 14:10:15,072 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:15,077 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:15,077 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:15,077 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 57
2023-08-21 14:10:15,151 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:15,155 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:15,156 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:15,156 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:15,160 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 59
2023-08-21 14:10:15,161 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:15,163 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:15,163 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:15,163 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:15,163 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:15,163 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:15,164 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 24}\r\n'
2023-08-21 14:10:15,205 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 24}\r\n'
2023-08-21 14:10:15,205 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:15,209 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:15,209 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:15,210 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 60
2023-08-21 14:10:15,284 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:15,289 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:15,289 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:15,289 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:15,294 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 62
2023-08-21 14:10:15,294 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:15,297 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:15,297 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:15,297 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:15,297 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:15,297 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:15,297 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 25}\r\n'
2023-08-21 14:10:15,310 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 25}\r\n'
2023-08-21 14:10:15,311 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:15,315 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:15,315 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:15,316 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 63
2023-08-21 14:10:15,387 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:15,392 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:15,392 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:15,393 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:15,397 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 65
2023-08-21 14:10:15,397 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:15,399 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:15,400 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:15,400 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:15,400 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:15,400 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:15,400 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 26}\r\n'
2023-08-21 14:10:15,407 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 26}\r\n'
2023-08-21 14:10:15,408 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:15,412 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:15,412 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:15,413 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 66
2023-08-21 14:10:15,429 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:15,433 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:15,434 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:15,434 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:15,438 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 68
2023-08-21 14:10:15,439 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:15,441 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:15,442 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:15,442 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:15,442 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:15,442 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:15,443 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 27}\r\n'
2023-08-21 14:10:15,455 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 27}\r\n'
2023-08-21 14:10:15,456 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:15,460 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:15,460 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:15,460 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 69
2023-08-21 14:10:15,513 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:15,518 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:15,519 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:15,519 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:15,523 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 71
2023-08-21 14:10:15,523 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:15,526 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:15,526 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:15,526 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:15,526 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:15,526 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:15,527 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 28}\r\n'
2023-08-21 14:10:15,534 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 28}\r\n'
2023-08-21 14:10:15,534 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:15,534 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:15,534 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:15,535 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 72
2023-08-21 14:10:15,554 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:15,558 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:15,559 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:15,559 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:15,563 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 74
2023-08-21 14:10:15,563 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:15,565 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:15,566 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:15,566 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:15,566 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:15,566 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:15,566 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 29}\r\n'
2023-08-21 14:10:15,589 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 29}\r\n'
2023-08-21 14:10:15,590 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:15,599 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:15,600 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:15,600 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 75
2023-08-21 14:10:15,637 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:15,637 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:15,638 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:15,642 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 76
2023-08-21 14:10:15,642 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:16,669 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:10:16,669 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:10:16,670 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:10:16,670 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:16,670 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:10:16,670 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 30}\r\n'
2023-08-21 14:10:16,703 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 30}\r\n'
2023-08-21 14:10:16,703 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=<hardware_alarm_irq_handler>
2023-08-21 14:10:16,719 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:10:16,720 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:16,720 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 77
2023-08-21 14:10:16,820 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:10:16,820 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:16,820 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:10:16,825 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 78
2023-08-21 14:10:16,825 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:10:17,149 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:17,149 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:17,149 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:17,149 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,149 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:17,149 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 31}\r\n'
2023-08-21 14:10:17,162 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 31}\r\n'
2023-08-21 14:10:17,162 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:17,173 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:17,173 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,173 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 79
2023-08-21 14:10:17,189 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:17,194 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:17,194 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:17,195 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:17,200 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 81
2023-08-21 14:10:17,200 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:17,202 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:17,202 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:17,202 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:17,202 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,202 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:17,203 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 32}\r\n'
2023-08-21 14:10:17,215 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 32}\r\n'
2023-08-21 14:10:17,216 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:17,222 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:17,223 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,223 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 82
2023-08-21 14:10:17,239 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:17,243 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:17,244 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:17,244 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:17,248 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 84
2023-08-21 14:10:17,249 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:17,250 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:17,251 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:17,251 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:17,251 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,251 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:17,251 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 33}\r\n'
2023-08-21 14:10:17,263 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 33}\r\n'
2023-08-21 14:10:17,264 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:17,280 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:17,281 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,281 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 85
2023-08-21 14:10:17,338 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:17,343 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:17,343 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:17,343 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:17,348 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 87
2023-08-21 14:10:17,348 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:17,350 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:17,351 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:17,351 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:17,351 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,351 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:17,351 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 34}\r\n'
2023-08-21 14:10:17,358 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 34}\r\n'
2023-08-21 14:10:17,359 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:17,374 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:17,375 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,375 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 88
2023-08-21 14:10:17,395 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:17,400 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:17,401 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:17,401 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:17,405 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 90
2023-08-21 14:10:17,407 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:17,408 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:17,409 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:17,409 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:17,409 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,409 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:17,410 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 35}\r\n'
2023-08-21 14:10:17,424 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 35}\r\n'
2023-08-21 14:10:17,425 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:17,440 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:17,440 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,440 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 91
2023-08-21 14:10:17,511 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:17,516 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:17,516 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:17,516 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:17,523 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 93
2023-08-21 14:10:17,523 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:17,525 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:17,526 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:17,526 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:17,526 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,526 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:17,526 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 36}\r\n'
2023-08-21 14:10:17,528 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 36}\r\n'
2023-08-21 14:10:17,529 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:17,538 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:17,538 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,538 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 94
2023-08-21 14:10:17,608 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:17,613 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:17,613 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:17,613 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:17,617 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 96
2023-08-21 14:10:17,618 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:17,621 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:17,621 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:17,621 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:17,622 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,622 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:17,622 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 37}\r\n'
2023-08-21 14:10:17,629 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 37}\r\n'
2023-08-21 14:10:17,629 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:17,638 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:17,639 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,639 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 97
2023-08-21 14:10:17,709 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:17,714 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:17,714 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:17,715 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:17,721 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 99
2023-08-21 14:10:17,722 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:17,724 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:17,724 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:17,724 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:17,724 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,724 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:17,725 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 38}\r\n'
2023-08-21 14:10:17,763 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 38}\r\n'
2023-08-21 14:10:17,763 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:17,767 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:17,768 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,768 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 100
2023-08-21 14:10:17,842 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:17,847 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:17,847 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:17,847 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:17,852 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 102
2023-08-21 14:10:17,852 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:17,854 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:17,855 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:17,855 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:17,855 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,855 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:17,855 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 39}\r\n'
2023-08-21 14:10:17,884 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 39}\r\n'
2023-08-21 14:10:17,885 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:17,890 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:17,890 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,890 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 103
2023-08-21 14:10:17,967 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:17,972 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:17,972 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:17,972 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:17,977 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 105
2023-08-21 14:10:17,977 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:17,979 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:17,980 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:17,980 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:17,980 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:17,980 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:17,980 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 40}\r\n'
2023-08-21 14:10:17,993 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 40}\r\n'
2023-08-21 14:10:17,994 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:17,999 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:17,999 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:17,999 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 106
2023-08-21 14:10:18,055 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:18,059 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:18,060 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:18,060 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:18,065 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 108
2023-08-21 14:10:18,066 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:18,068 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:18,068 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:18,068 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:18,069 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:18,069 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:18,069 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 41}\r\n'
2023-08-21 14:10:18,075 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 41}\r\n'
2023-08-21 14:10:18,076 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:18,086 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:18,086 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:18,086 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 109
2023-08-21 14:10:18,107 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:18,111 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:18,112 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:18,112 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:18,116 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 111
2023-08-21 14:10:18,117 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:18,122 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:18,122 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:18,122 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:18,122 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:18,122 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:18,123 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 42}\r\n'
2023-08-21 14:10:18,135 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 42}\r\n'
2023-08-21 14:10:18,135 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:18,145 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:18,146 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:18,146 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 112
2023-08-21 14:10:18,182 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:18,182 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:18,182 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:18,187 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 113
2023-08-21 14:10:18,187 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:19,214 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:10:19,214 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:10:19,215 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:10:19,215 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:19,215 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:10:19,215 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 43}\r\n'
2023-08-21 14:10:19,223 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 43}\r\n'
2023-08-21 14:10:19,224 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=<hardware_alarm_irq_handler>
2023-08-21 14:10:19,228 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:10:19,229 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:19,229 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 114
2023-08-21 14:10:19,325 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:10:19,325 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:19,325 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:10:19,329 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 115
2023-08-21 14:10:19,330 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:10:19,681 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:19,681 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:19,681 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:19,681 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:19,681 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:19,682 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 44}\r\n'
2023-08-21 14:10:19,689 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 44}\r\n'
2023-08-21 14:10:19,689 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:19,694 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:19,694 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:19,694 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 116
2023-08-21 14:10:19,709 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:19,714 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:19,715 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:19,715 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:19,719 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 118
2023-08-21 14:10:19,719 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:19,723 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:19,723 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:19,723 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:19,723 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:19,723 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:19,724 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 45}\r\n'
2023-08-21 14:10:19,767 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 45}\r\n'
2023-08-21 14:10:19,767 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:19,782 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:19,783 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:19,783 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 119
2023-08-21 14:10:19,839 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:19,844 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:19,844 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:19,844 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:19,849 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 121
2023-08-21 14:10:19,849 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:19,851 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:19,852 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:19,852 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:19,852 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:19,852 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:19,852 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 46}\r\n'
2023-08-21 14:10:19,886 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 46}\r\n'
2023-08-21 14:10:19,886 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:19,891 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:19,892 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:19,892 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 122
2023-08-21 14:10:19,913 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:19,918 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:19,919 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:19,919 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:19,925 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 124
2023-08-21 14:10:19,926 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:19,928 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:19,928 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:19,928 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:19,928 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:19,928 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:19,929 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 47}\r\n'
2023-08-21 14:10:19,947 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 47}\r\n'
2023-08-21 14:10:19,948 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:19,958 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:19,958 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:19,958 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 125
2023-08-21 14:10:20,031 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:20,036 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:20,036 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:20,036 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:20,041 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 127
2023-08-21 14:10:20,041 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:20,044 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:20,044 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:20,044 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:20,044 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:20,044 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:20,044 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 48}\r\n'
2023-08-21 14:10:20,057 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 48}\r\n'
2023-08-21 14:10:20,058 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:20,062 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:20,063 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:20,063 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 128
2023-08-21 14:10:20,138 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:20,143 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:20,144 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:20,144 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:20,148 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 130
2023-08-21 14:10:20,148 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:20,151 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:20,151 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:20,151 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:20,151 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:20,151 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:20,151 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 49}\r\n'
2023-08-21 14:10:20,164 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 49}\r\n'
2023-08-21 14:10:20,165 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:20,180 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:20,180 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:20,181 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 131
2023-08-21 14:10:20,253 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:20,257 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:20,258 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:20,258 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:20,262 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 133
2023-08-21 14:10:20,263 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:20,265 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:20,265 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:20,265 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:20,265 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:20,265 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:20,266 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 50}\r\n'
2023-08-21 14:10:20,273 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 50}\r\n'
2023-08-21 14:10:20,273 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:20,278 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:20,278 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:20,278 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 134
2023-08-21 14:10:20,351 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:20,356 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:20,356 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:20,356 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:20,361 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 136
2023-08-21 14:10:20,361 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:20,363 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:20,363 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:20,364 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:20,364 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:20,364 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:20,364 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 51}\r\n'
2023-08-21 14:10:20,376 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 51}\r\n'
2023-08-21 14:10:20,378 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:20,382 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:20,382 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:20,382 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 137
2023-08-21 14:10:20,456 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:20,461 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:20,462 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:20,462 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:20,466 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 139
2023-08-21 14:10:20,467 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:20,469 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:20,469 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:20,469 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:20,469 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:20,469 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:20,470 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 52}\r\n'
2023-08-21 14:10:20,477 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 52}\r\n'
2023-08-21 14:10:20,477 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:20,481 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:20,482 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:20,482 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 140
2023-08-21 14:10:20,498 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:20,502 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:20,502 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:20,503 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:20,507 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 142
2023-08-21 14:10:20,507 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:20,510 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:20,511 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:20,511 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:20,511 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:20,511 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:20,511 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 53}\r\n'
2023-08-21 14:10:20,535 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 53}\r\n'
2023-08-21 14:10:20,536 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:20,541 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:20,542 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:20,542 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 143
2023-08-21 14:10:20,592 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:20,597 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:20,598 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:20,598 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:20,602 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 145
2023-08-21 14:10:20,602 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:20,605 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:20,605 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:20,605 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:20,605 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:20,605 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:20,605 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 54}\r\n'
2023-08-21 14:10:20,630 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 54}\r\n'
2023-08-21 14:10:20,631 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:20,641 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:20,641 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:20,641 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 146
2023-08-21 14:10:20,662 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:20,667 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:20,667 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:20,668 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:20,672 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 148
2023-08-21 14:10:20,673 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:20,675 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:20,675 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:20,675 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:20,675 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:20,675 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:20,676 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 55}\r\n'
2023-08-21 14:10:20,714 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 55}\r\n'
2023-08-21 14:10:20,714 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:20,777 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:20,777 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:20,777 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 149
2023-08-21 14:10:20,826 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:20,827 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:20,827 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:20,831 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 150
2023-08-21 14:10:20,832 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:21,690 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:10:21,690 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:10:21,690 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:10:21,690 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:21,690 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:10:21,691 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 56}\r\n'
2023-08-21 14:10:21,698 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 56}\r\n'
2023-08-21 14:10:21,698 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=<hardware_alarm_irq_handler>
2023-08-21 14:10:21,703 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:10:21,704 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:21,704 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 151
2023-08-21 14:10:21,802 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:10:21,802 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:21,802 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:10:21,806 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 152
2023-08-21 14:10:21,807 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:10:22,164 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:22,164 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:22,164 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:22,164 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,165 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:22,165 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 57}\r\n'
2023-08-21 14:10:22,172 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 57}\r\n'
2023-08-21 14:10:22,172 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:22,177 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:22,177 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:22,177 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 153
2023-08-21 14:10:22,192 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:22,197 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:22,197 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:22,198 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:22,202 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 155
2023-08-21 14:10:22,203 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:22,205 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:22,205 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:22,205 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:22,206 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,206 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:22,206 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 58}\r\n'
2023-08-21 14:10:22,219 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 58}\r\n'
2023-08-21 14:10:22,220 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:22,225 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:22,225 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:22,226 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 156
2023-08-21 14:10:22,242 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:22,247 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:22,247 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:22,247 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:22,251 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 158
2023-08-21 14:10:22,252 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:22,254 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:22,254 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:22,254 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:22,254 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,254 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:22,254 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 59}\r\n'
2023-08-21 14:10:22,257 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 59}\r\n'
2023-08-21 14:10:22,257 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:22,261 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:22,262 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:22,262 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 159
2023-08-21 14:10:22,277 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:22,282 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:22,282 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:22,282 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:22,286 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 161
2023-08-21 14:10:22,287 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:22,289 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:22,289 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:22,289 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:22,289 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,290 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:22,290 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 60}\r\n'
2023-08-21 14:10:22,318 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 60}\r\n'
2023-08-21 14:10:22,319 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:22,331 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:22,332 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:22,332 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 162
2023-08-21 14:10:22,384 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:22,389 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:22,389 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:22,389 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:22,394 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 164
2023-08-21 14:10:22,394 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:22,396 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:22,396 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:22,397 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:22,397 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,397 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:22,397 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 61}\r\n'
2023-08-21 14:10:22,427 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 61}\r\n'
2023-08-21 14:10:22,427 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:22,443 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:22,443 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:22,443 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 165
2023-08-21 14:10:22,464 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:22,469 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:22,470 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:22,470 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:22,474 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 167
2023-08-21 14:10:22,475 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:22,477 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:22,477 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:22,477 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:22,477 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,477 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:22,478 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 62}\r\n'
2023-08-21 14:10:22,506 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 62}\r\n'
2023-08-21 14:10:22,507 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:22,511 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:22,512 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:22,512 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 168
2023-08-21 14:10:22,586 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:22,591 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:22,591 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:22,591 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:22,596 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 170
2023-08-21 14:10:22,597 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:22,598 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:22,599 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:22,599 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:22,599 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,599 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:22,599 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 63}\r\n'
2023-08-21 14:10:22,617 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 63}\r\n'
2023-08-21 14:10:22,617 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:22,628 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:22,629 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:22,629 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 171
2023-08-21 14:10:22,701 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:22,706 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:22,707 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:22,707 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:22,711 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 173
2023-08-21 14:10:22,711 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:22,714 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:22,714 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:22,714 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:22,714 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,714 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:22,714 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 64}\r\n'
2023-08-21 14:10:22,783 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 64}\r\n'
2023-08-21 14:10:22,784 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:22,793 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:22,794 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:22,794 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 174
2023-08-21 14:10:22,867 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:22,871 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:22,872 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:22,872 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:22,876 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 176
2023-08-21 14:10:22,877 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:22,879 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:22,879 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:22,879 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:22,879 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,879 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:22,880 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 65}\r\n'
2023-08-21 14:10:22,892 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 65}\r\n'
2023-08-21 14:10:22,893 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:22,902 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:22,903 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:22,903 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 177
2023-08-21 14:10:22,956 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:22,961 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:22,961 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:22,961 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:22,966 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 179
2023-08-21 14:10:22,966 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:22,968 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:22,968 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:22,968 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:22,969 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:22,969 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:22,969 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 66}\r\n'
2023-08-21 14:10:22,996 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 66}\r\n'
2023-08-21 14:10:22,997 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:23,013 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:23,013 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:23,013 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 180
2023-08-21 14:10:23,037 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:23,041 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:23,042 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:23,042 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:23,046 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 182
2023-08-21 14:10:23,047 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:23,049 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:23,049 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:23,049 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:23,049 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:23,049 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:23,049 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 67}\r\n'
2023-08-21 14:10:23,051 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 67}\r\n'
2023-08-21 14:10:23,052 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:23,118 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:23,118 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:23,119 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 183
2023-08-21 14:10:23,168 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:23,168 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:23,168 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:23,173 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 184
2023-08-21 14:10:23,173 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:23,992 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:10:23,992 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:10:23,992 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:10:23,992 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:23,992 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:10:23,993 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 68}\r\n'
2023-08-21 14:10:24,015 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 68}\r\n'
2023-08-21 14:10:24,015 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=<hardware_alarm_irq_handler>
2023-08-21 14:10:24,034 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:10:24,034 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:24,034 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 185
2023-08-21 14:10:24,133 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:10:24,133 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:24,133 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:10:24,138 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 186
2023-08-21 14:10:24,138 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:10:24,475 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:24,476 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:24,476 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:24,476 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:24,476 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:24,476 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 69}\r\n'
2023-08-21 14:10:24,519 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 69}\r\n'
2023-08-21 14:10:24,519 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:24,525 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:24,525 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:24,525 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 187
2023-08-21 14:10:24,541 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:24,546 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:24,546 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:24,547 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:24,551 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 189
2023-08-21 14:10:24,552 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:24,554 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:24,554 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:24,554 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:24,554 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:24,554 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:24,555 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 70}\r\n'
2023-08-21 14:10:24,557 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 70}\r\n'
2023-08-21 14:10:24,557 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:24,562 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:24,562 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:24,562 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 190
2023-08-21 14:10:24,613 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:24,619 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:24,619 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:24,619 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:24,625 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 192
2023-08-21 14:10:24,626 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:24,628 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:24,628 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:24,629 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:24,629 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:24,629 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:24,629 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 71}\r\n'
2023-08-21 14:10:24,641 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 71}\r\n'
2023-08-21 14:10:24,642 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:24,651 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:24,652 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:24,652 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 193
2023-08-21 14:10:24,672 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:24,677 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:24,677 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:24,677 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:24,682 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 195
2023-08-21 14:10:24,682 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:24,684 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:24,685 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:24,685 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:24,685 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:24,685 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:24,685 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 72}\r\n'
2023-08-21 14:10:24,713 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 72}\r\n'
2023-08-21 14:10:24,714 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:24,731 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:24,731 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:24,731 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 196
2023-08-21 14:10:24,802 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:24,807 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:24,807 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:24,807 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:24,811 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 198
2023-08-21 14:10:24,812 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:24,814 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:24,814 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:24,814 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:24,814 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:24,815 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:24,815 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 73}\r\n'
2023-08-21 14:10:24,843 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 73}\r\n'
2023-08-21 14:10:24,844 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:24,848 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:24,848 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:24,849 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 199
2023-08-21 14:10:24,919 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:24,925 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:24,926 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:24,926 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:24,930 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 201
2023-08-21 14:10:24,930 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:24,933 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:24,933 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:24,933 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:24,933 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:24,933 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:24,934 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 74}\r\n'
2023-08-21 14:10:24,946 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 74}\r\n'
2023-08-21 14:10:24,947 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:24,962 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:24,962 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:24,962 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 202
2023-08-21 14:10:25,034 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:25,038 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:25,039 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:25,039 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:25,043 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 204
2023-08-21 14:10:25,043 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:25,046 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:25,046 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:25,046 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:25,046 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:25,046 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:25,046 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 75}\r\n'
2023-08-21 14:10:25,064 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 75}\r\n'
2023-08-21 14:10:25,064 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:25,068 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:25,069 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:25,069 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 205
2023-08-21 14:10:25,143 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:25,148 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:25,148 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:25,148 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:25,153 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 207
2023-08-21 14:10:25,153 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:25,155 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:25,155 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:25,156 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:25,156 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:25,156 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:25,156 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 76}\r\n'
2023-08-21 14:10:25,169 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 76}\r\n'
2023-08-21 14:10:25,169 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:25,173 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:25,173 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:25,174 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 208
2023-08-21 14:10:25,189 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:25,194 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:25,194 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:25,195 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:25,199 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 210
2023-08-21 14:10:25,200 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:25,202 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:25,202 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:25,202 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:25,203 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:25,203 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:25,203 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 77}\r\n'
2023-08-21 14:10:25,216 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 77}\r\n'
2023-08-21 14:10:25,216 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:25,223 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:25,223 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:25,224 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 211
2023-08-21 14:10:25,239 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:25,244 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:25,244 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:25,244 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:25,249 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 213
2023-08-21 14:10:25,249 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:25,251 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:25,251 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:25,252 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:25,252 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:25,252 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:25,252 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 78}\r\n'
2023-08-21 14:10:25,259 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 78}\r\n'
2023-08-21 14:10:25,259 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:25,264 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:25,264 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:25,265 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 214
2023-08-21 14:10:25,317 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:25,323 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:25,324 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:25,324 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:25,328 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 216
2023-08-21 14:10:25,328 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:25,331 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:25,331 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:25,331 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:25,331 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:25,331 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:25,332 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 79}\r\n'
2023-08-21 14:10:25,344 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 79}\r\n'
2023-08-21 14:10:25,344 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:25,354 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:25,354 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:25,354 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 217
2023-08-21 14:10:25,375 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:25,381 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:25,382 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:25,382 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:25,386 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 219
2023-08-21 14:10:25,386 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:25,388 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:25,389 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:25,389 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:25,389 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:25,389 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:25,389 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 80}\r\n'
2023-08-21 14:10:25,391 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 80}\r\n'
2023-08-21 14:10:25,391 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:25,396 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:25,396 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:25,396 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 220
2023-08-21 14:10:25,443 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:25,443 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:25,444 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:25,448 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 221
2023-08-21 14:10:25,448 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:26,467 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 19
2023-08-21 14:10:26,467 | avatar | INFO | forwarding interrupt 19
2023-08-21 14:10:26,467 | avatar | INFO | Injecting IRQ 0x13
2023-08-21 14:10:26,467 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:26,467 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 19
2023-08-21 14:10:26,468 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 19, "num-cpu": 0}, "id": 81}\r\n'
2023-08-21 14:10:26,501 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 81}\r\n'
2023-08-21 14:10:26,501 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=19 irq=<hardware_alarm_irq_handler>
2023-08-21 14:10:26,505 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 19 (0)
2023-08-21 14:10:26,506 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:26,506 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 222
2023-08-21 14:10:26,603 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 19 (fffffff9)
2023-08-21 14:10:26,603 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:26,604 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 19.
2023-08-21 14:10:26,608 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 223
2023-08-21 14:10:26,608 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=19
2023-08-21 14:10:26,925 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:26,926 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:26,926 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:26,926 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:26,926 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:26,926 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 82}\r\n'
2023-08-21 14:10:26,939 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 82}\r\n'
2023-08-21 14:10:26,940 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:26,949 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:26,950 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:26,950 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 224
2023-08-21 14:10:26,966 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:26,970 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:26,971 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:26,971 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:26,976 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 226
2023-08-21 14:10:26,977 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:26,978 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:26,979 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:26,979 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:26,979 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:26,979 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:26,979 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 83}\r\n'
2023-08-21 14:10:27,030 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 83}\r\n'
2023-08-21 14:10:27,030 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:27,040 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:27,040 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,040 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 227
2023-08-21 14:10:27,092 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,097 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:27,098 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,098 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:27,102 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 229
2023-08-21 14:10:27,102 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:27,105 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:27,105 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:27,105 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:27,105 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,105 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:27,105 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 84}\r\n'
2023-08-21 14:10:27,119 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 84}\r\n'
2023-08-21 14:10:27,120 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:27,134 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:27,135 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,135 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 230
2023-08-21 14:10:27,156 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,161 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:27,162 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,162 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:27,166 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 232
2023-08-21 14:10:27,167 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:27,169 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:27,169 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:27,169 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:27,169 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,169 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:27,170 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 85}\r\n'
2023-08-21 14:10:27,183 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 85}\r\n'
2023-08-21 14:10:27,184 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:27,200 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:27,200 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,200 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 233
2023-08-21 14:10:27,277 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,281 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:27,282 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,282 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:27,286 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 235
2023-08-21 14:10:27,287 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:27,289 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:27,289 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:27,289 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:27,289 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,289 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:27,290 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 86}\r\n'
2023-08-21 14:10:27,302 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 86}\r\n'
2023-08-21 14:10:27,303 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:27,308 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:27,308 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,308 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 236
2023-08-21 14:10:27,381 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,386 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:27,387 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,387 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:27,391 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 238
2023-08-21 14:10:27,391 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:27,394 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:27,394 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:27,394 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:27,394 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,394 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:27,394 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 87}\r\n'
2023-08-21 14:10:27,407 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 87}\r\n'
2023-08-21 14:10:27,408 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:27,424 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:27,424 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,425 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 239
2023-08-21 14:10:27,496 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,501 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:27,501 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,501 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:27,506 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 241
2023-08-21 14:10:27,506 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:27,508 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:27,508 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:27,508 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:27,508 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,509 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:27,509 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 88}\r\n'
2023-08-21 14:10:27,516 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 88}\r\n'
2023-08-21 14:10:27,516 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:27,521 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:27,521 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,521 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 242
2023-08-21 14:10:27,593 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,598 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:27,598 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,598 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:27,603 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 244
2023-08-21 14:10:27,603 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:27,605 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:27,605 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:27,605 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:27,606 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,606 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:27,606 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 89}\r\n'
2023-08-21 14:10:27,618 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 89}\r\n'
2023-08-21 14:10:27,619 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:27,624 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:27,625 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,625 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 245
2023-08-21 14:10:27,697 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,702 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 47 (fffffff9)
2023-08-21 14:10:27,702 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,702 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 47.
2023-08-21 14:10:27,707 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 247
2023-08-21 14:10:27,707 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=47
2023-08-21 14:10:27,709 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:27,710 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:27,710 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:27,710 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,710 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:27,710 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 90}\r\n'
2023-08-21 14:10:27,725 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 90}\r\n'
2023-08-21 14:10:27,725 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:27,729 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:27,730 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,730 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 248
2023-08-21 14:10:27,746 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,750 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:27,751 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,751 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:27,756 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 250
2023-08-21 14:10:27,756 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:27,758 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:27,759 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:27,759 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:27,759 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,759 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:27,759 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 91}\r\n'
2023-08-21 14:10:27,772 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 91}\r\n'
2023-08-21 14:10:27,773 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:27,782 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:27,782 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,782 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 251
2023-08-21 14:10:27,838 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,843 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:27,843 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,843 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:27,848 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 253
2023-08-21 14:10:27,848 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:27,850 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 21
2023-08-21 14:10:27,850 | avatar | INFO | forwarding interrupt 21
2023-08-21 14:10:27,851 | avatar | INFO | Injecting IRQ 0x15
2023-08-21 14:10:27,851 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,851 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 21
2023-08-21 14:10:27,851 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 21, "num-cpu": 0}, "id": 92}\r\n'
2023-08-21 14:10:27,873 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 92}\r\n'
2023-08-21 14:10:27,874 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=21 irq=<irq_handler_chain_slots> -> <dcd_rp2040_irq>
2023-08-21 14:10:27,884 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 21 (0)
2023-08-21 14:10:27,884 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:27,884 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 254
2023-08-21 14:10:27,904 | emulated | DEBUG | pc=0x0 Forwarding write at 0xe000e200 with value 2147483648 (0x80000000)
2023-08-21 14:10:27,910 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptExitRequest for irq 21 (fffffff9)
2023-08-21 14:10:27,910 | avatar | WARNING | _handle_remote_interrupt_exit_message RemoteInterruptExitMessage from qemu)
2023-08-21 14:10:27,910 | avatar.protocols.ARMV7InterruptProtocol | INFO | Returning from interrupt 21.
2023-08-21 14:10:27,915 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptExitResponse with id 256
2023-08-21 14:10:27,915 | script | WARNING | >>>>>>>>>>> EXIT  IRQ-num=21
2023-08-21 14:10:27,917 | avatar.protocols.ARMV7InterruptProtocol | INFO | IRQ event 47
2023-08-21 14:10:27,917 | avatar | INFO | forwarding interrupt 47
2023-08-21 14:10:27,918 | avatar | INFO | Injecting IRQ 0x2f
2023-08-21 14:10:27,918 | avatar | WARNING | forward_interrupt True)
2023-08-21 14:10:27,918 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Injecting interrupt 47
2023-08-21 14:10:27,918 | avatar.targets.qemu.QMPProtocol | INFO | Sending command: b'{"execute": "avatar-armv7m-inject-irq", "arguments": {"num-irq": 47, "num-cpu": 0}, "id": 93}\r\n'
In [1]:                                                                                                                                                                                                 
Do you really want to exit ([y]/n)? 

=================== Done =========================
2023-08-21 14:10:27,968 | avatar.targets.qemu.QMPProtocol | INFO | b'{"return": {}, "id": 93}\r\n'
2023-08-21 14:10:27,969 | script | WARNING | >>>>>>>>>>> ENTER IRQ-num=47 irq=<low_priority_worker_irq> -> <tud_task_ext>
2023-08-21 14:10:28,039 | avatar.targets.hardware | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:10:28,041 | avatar | INFO | Received state update of target hardware to TargetStates.STOPPED
2023-08-21 14:10:28,046 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | WARNING | Received an InterruptEnterRequest for irq 47 (0)
2023-08-21 14:10:28,047 | avatar | WARNING | _handle_remote_interrupt_enter_message True)
2023-08-21 14:10:28,047 | avatar.targets.qemu.protocols.QEmuARMV7MInterruptProtocol | INFO | Send RemoteInterruptEnterResponse with id 257
2023-08-21 14:10:28,075 | avatar.targets.qemu | INFO | State changed to TargetStates.STOPPED
2023-08-21 14:10:28,076 | avatar | INFO | Received state update of target qemu to TargetStates.STOPPED
2023-08-21 14:10:28,077 | avatar.protocols.ARMV7InterruptProtocol | INFO | Interrupt thread exiting...
2023-08-21 14:10:28,548 | avatar.targets.qemu.RemoteMemoryProtocol | WARNING | Tried to close/unlink non existent rx_queue
2023-08-21 14:10:28,548 | avatar.targets.qemu.RemoteMemoryProtocol | WARNING | Tried to close/unlink non existent tx_queue

```