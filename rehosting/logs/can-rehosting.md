# CAN rehosting 20.07.2023

## Summary

Experiment 1 - Send-UART Rehosting:
The "send-uart" firmware was successfully rehosted, enabling package transmission between two CANPico devices.
Though the rehosted firmware operated slower, sending only one package every 10 seconds, it maintained its
functionality.
No further measures seem to be necessary for this application, as there were no strict time constraints.
It should be noted, that this example only sends packages, and receives only to catch errors during transmission.

Experiment 2 - Receive-UART Rehosting:
The "recv-uart" firmware for receiving packages from another CANPico was rehosted successfully.
However, the rehosted firmware was running significantly slower, not impacting the functionality/correctness in this
use case but enough to probably cause problems at higher package rates.
Still, the slowdown caused packages to accumulate and therefore created a different interrupt trace than during irq
recording.
This issues could be tackled with a HALucinator approach where the time critical code is executed in the hardware.
The danger here is that the relevant memory changes made by this handler need to be transferred to QEmu.

Experiment 3 - Echo-UART Rehosting:
The "echo-uart" firmware for ping-ponging an increasing counter via a CAN package between two applications was rehosted
successfully.
However, the significant slowdown created by the lifting into QEmu and many MMIO memory forwards made the rehosting
unreliable.
Especially the initialization required the other end to be halted (eg. pulling the reset pin to GND) until the init and
state transfer of the rehosted device was complete.
This is a bit too unreliable than what we want to a HAL approach should be considered to speed up the rehosting process.
Of course this will come with the same challenges as for experiment 2.

## Experiment setup

One uncontrolled CANPico running either the recv-usb or send-usb firmware, exactly
opposite of the controlled CANPico running the other firmware.
The can bus is fully connected with `High-High, Low-Low, Gnd-Gnd`.
Both CANPicos **MUST** be powered via usb, debug probes (eg. picoprobe) can be attached
but only the ground pins must be connected, no other power pins.

__To flash the firmwares:__  
`$ openocd -f interface/cmsis-dap.cfg -c "adapter speed 6000" -f target/rp2040.cfg -c "program ./bins/can/recv-usb.elf reset exit"`

### Used files

#### Git commits:

__[albrecht/experiments](https://gitlab.eurecom.fr/albrecht/experiments)__

```git
commit 59f3a86830ede3b0eb8cefeda2fb375e0b952cb0 (HEAD -> main, origin/main)
Author: Florian Albrecht <florian.albrecht@eurecom.fr>
Date:   Mon Jul 24 11:10:21 2023 +0200

    Echo CAN example rehosting documentation
```

__[albrecht-flo/avatar2#interrupts](https://github.com/albrecht-flo/avatar2/tree/interrupts)__

```git
commit bb018ac5981ae86ac5f134ece1b96db264187c17 (HEAD -> interrupts, origin/interrupts)
Author: Florian Albrecht <florian.albrecht@eurecom.fr>
Date:   Wed Jul 19 18:12:44 2023 +0200

    Fixed wrong program counter
```

__[albrecht-flo/avatar-qemu](https://github.com/albrecht-flo/avatar-qemu)__

```git
commit 7763c0a3a4be256e5396aaf7002e8bb6d8b19356 (HEAD -> dev/qemu-6.2, origin/dev/qemu-6.2, origin/HEAD)
Author: Florian Albrecht <florian.albrecht.public@outlook.de>
Date:   Fri Jun 30 17:16:20 2023 +0200

    Remove temporary hack for nic enable irq
```

#### Files:

| File          | SHA256                                                           |
|---------------|------------------------------------------------------------------|
| recv-uart.bin | 0eb364c238fbab4ce05005633a3e50848c7bc972b21bf4b2019069b4ab6a59bb |
| recv-uart.dis | 3b4d899b85c0f35669e28cb7369dff3c1ad66b6f3f6189b2111f190c58ee3e9a |
| recv-uart.elf | eb98a717cf02b868e2f310d6ca8ddacb809dadf6fcacaed856a4651f0c89ad1f |
| recv-usb.elf  | 665f0d4efc12f95be680ab253ef4b2273a6c570f379506597425deeb096eb833 |
| send-uart.bin | 9dcdb6b45c26ccad731261e659068ca1300c75169253cd3bd0d36cb8ad18b333 |
| send-uart.dis | 1eaf9444971e245b16a20e3fc6d88702b10609199ade91260e1ce68deb101ccd |
| send-uart.elf | 8683985d48869950757d2af89241e794fcf16d817b1534a19f8e2853b8ebd5ec |
| send-usb.elf  | e1c9e54745d728d72d92c29adbded628cc84cb3dded4e7672eeda3534e740cfd |
| echo-uart.bin | 2637174453ad43380b7de3dab805b5e8d613f1805490fde61a755f57816c4ed5 |
| echo-uart.dis | dc5bea7591ed6286f86817546b22d7f75d27c427dc985bbf0e39f57dccf7d094 |
| echo-uart.elf | cebdf40aa7b73d3d467362da1ecd8d2dc7a64a22bc0913d031639e25d66cad17 |

__Hardware-Setup:__
![](imgs/can-experiment-setup.jpg)

## Experiment log

### Rehosting `send-uart`

__Targets:__

| Target            | Firmware  | Debugger  | Comment         |
|-------------------|:---------:|:---------:|-----------------|
| Rehosted CANPico  | send-uart | picoprobe | powered via usb |
| Answering CANPico | recv-usb  |   none    | powered via usb |

#### Comments

* send-uart works rehosted
    * Note: this is a very slow firmware only sending one package every 10s
* The transmitted packages are received by the other CANPico
* The firmware is running significantly slower
    * This did not show any significant impact on the functionality of the rehosted firmware
* No further measures seem to be necessary for this type of applications
    * No harsh time constraints
    * Only outgoing communication, receive is only used to catch errors (internal packages) during transmission

#### Tracing run

* Command `python irq-recording.py --firmware bins/can/send-uart.bin --openocd_cfg common/pico-picoprobe.cfg
  --map irq-maps/send_uart_map.py -I 0x1000063c`
* [JSON trace](traces/send-uart-irq_trace-2023-07-20_15-50-22.json)

__Recv-USB output:__

```
============================== Boot complete ==============================
Initializing CAN ...
CAN Init finished :)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 01000000 (9120729)
>>>> irq-hit 0x1                                                          
Processing RXIF event (0xa012000a)                                        
Received Event 1                                                          
0x004 02000000 (19124256)                                                 
>>>> irq-hit 0x1                                                          
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 03000000 (29127793)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 04000000 (39131328)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 05000000 (49134865)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 06000000 (59138399)
```

__Recorded send-uart output:__

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)
Tick 0
CAN successfully sent: rc=0Processing TEFIF event (0xa0120018)
, data=1
Tick 1
CAN successfully sent: rc=0Processing TEFIF event (0xa0120018)
, data=2
Tick 2
CAN successfully sent: rc=0Processing TEFIF event (0xa0120018)
, data=3
Tick 3
CAN successfully sent: rc=0Processing TEFIF event (0xa0120018)
, data=4
Tick 4
CAN successfully sent: rc=0Processing TEFIF event (0xa0120018)
, data=5
Tick 5
CAN successfully sent: rc=0Processing TEFIF event (0xa0120018)
, data=6

```

#### Rehosted run

* [JSON trace](traces/rehosted_recv-uart_trace-2023-07-20_15-04-18.json)
* [Logfile](rehosting_send-uart.log)
* Command `python irq_send_uart.py`

__Recv-USB output:__

```
============================== Boot complete ==============================
Initializing CAN ...
CAN Init finished :)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 01000000 (31252269)
>>>> irq-hit 0x1                                                          
Processing RXIF event (0xa012000a)                                        
Received Event 1                                                          
0x004 02000000 (43481698)                                                 
>>>> irq-hit 0x1                                                          
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 03000000 (57158962)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 04000000 (71042211)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 05000000 (84686427)
```

__Rehosted send-uart output:__

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)
Tick 0
CAN successfully sent: rc=0, data=1
Processing TEFIF event (0xa0120018)
Tick 1
Processing TEFIF event (0xa0120018)
CAN successfully sent: rc=0, data=2
Tick 2
Processing TEFIF event (0xa0120018)
CAN successfully sent: rc=0, data=3
Tick 3
Processing TEFIF event (0xa0120018)
CAN successfully sent: rc=0, data=4
Tick 4
Processing TEFIF event (0xa0120018)
CAN successfully sent: rc=0, data=5
```

### Rehosting `recv-uart`

__Targets:__

| Target            | Firmware  | Debugger  | Comment         |
|-------------------|:---------:|:---------:|-----------------|
| Rehosted CANPico  | recv-uart | picoprobe | powered via usb |
| Answering CANPico | send-usb  |   none    | powered via usb |

#### Comments

* recv-uart works rehosted
    * The firmware is running significantly slower to the point that additional measures should be considered
    * Note: this firmware only receives and never sends any packages
* The packages, sent by the other CANPico, are received correctly
* The firmware is running significantly slower
    * There was no observable problem with this but if the package rate would increase this will cause problems
    * This also caused interrupts to fire in an alternate order than in the recorded sample
        * Due to the packages accumulating before being processed
        * Yet this did not produce any invalid results/behavior
* Due to the extreme slow down additional measures should be considered to rehost this more reliably in more
  time constrained environments with a higher package rate
    * The package receiving part seems to be the really time critical and heavily impacted (by the rehosting) part
        * Potentially this could be executed in the hardware (eg. HALucinator approach)
            * Would require significant memory synchronization in RAM
                * The incoming packages get stored in a buffer in RAM
                * The changes to this buffer would need to be transfered to QEmu after the interrupt handler has
                  finished`

#### Tracing run

* Command `python irq-recording.py --firmware bins/can/recv-uart.bin --openocd_cfg common/pico-picoprobe.cfg
  --map irq-maps/recv_uart_map.py -I 0x1000052e`
* [JSON trace](traces/recv-uart-irq_trace-2023-07-20_15-11-03.json)

__Send-USB output:__

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)
Tick 0
CAN successfully sent: rc=0, data=1
Processing TEFIF event (0xa0120018)
Tick 1                                                                    
CAN successfully sent: rc=0, data=2                                       
Processing TEFIF event (0xa0120018)                                       
Tick 2                                                                    
CAN successfully sent: rc=0, data=3                                       
Processing TEFIF event (0xa0120018)
Tick 3
CAN successfully sent: rc=0, data=4
Processing TEFIF event (0xa0120018)
Tick 4
CAN successfully sent: rc=0, data=5
Processing TEFIF event (0xa0120018)
```

__Recorded recv-uart output:__

```
============================== Boot complete ==============================
Initializing CAN ...
CAN Init finished :)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 02000000 (5604036)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 03000000 (15603613)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 04000000 (25603188)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 05000000 (35602726)
```

#### Rehosted run

* [JSON trace](traces/rehosted_recv-uart_trace-2023-07-20_15-04-18.json)
* [Logfile](rehosting_recv-uart.log)
* Command `python irq_recv_uart.py`

__Send-USB output:__

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)
Tick 0
CAN successfully sent: rc=0, data=1
Processing TEFIF event (0xa0120018)
Tick 1                                                                    
CAN successfully sent: rc=0, data=2                                       
Tick 2                                                                    
CAN successfully sent: rc=0, data=3                                       
Processing TEFIF event (0xa0120018)                                       
Tick 3
CAN successfully sent: rc=0, data=4
Processing TEFIF event (0xa0120018)
Tick 4
CAN successfully sent: rc=0, data=5
Processing TEFIF event (0xa0120018)
Tick 5
CAN successfully sent: rc=0, data=6
Processing TEFIF event (0xa0120018)
Tick 6
CAN successfully sent: rc=0, data=7
Processing TEFIF event (0xa0120018)
```

__Rehosted recv-uart output:__

```
============================== Boot complete ==============================
Initializing CAN ...
CAN Init finished :)
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
Processing RXIF event (0xa012000a)
Processing RXIF event (0xa012000a)
Received Event 1 
0x004 02000000>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
 (4898154)
Received Event 1 
0x004 03000000 (14897871)
Received Event 1 
0x004 04000000 (24897403)
Received Event 1 
0x004 05000000 (34897168)
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
>>>> irq-hit 0x0
Received Event 1 
0x004 06000000 (44896731)
>>>> irq-hit 0x0
>>>> irq-hit 0x0
>>>> irq-hit 0x1
Processing RXIF event (0xa012000a)
>>>> irq-hit 0x0
Received Event 1 
0x004 07000000 (54896270)
```

### Rehosting `recv-uart`

__Targets:__

| Target            | Firmware  | Debugger  | Comment         |
|-------------------|:---------:|:---------:|-----------------|
| Rehosted CANPico  | recv-uart | picoprobe | powered via usb |
| Answering CANPico | send-usb  |   none    | powered via usb |

##### Comments

* echo-uart works
    * BUT only with very careful timing due to massive slowdown
    * The recv stage seems to be very slow resulting in a timeout at the other end of the CAN bus
        * This can be seen in the multiple `Ticks` happening on the echo-usb side, while the echo-uart is processing and
          does not have any waiting ticks
        * We can also see that the initialization phase is very wonky, in this run the init got restarted by the
          echo-usb side while in the previous trace this was not the case
* Still the ping pong counter gets incremented between the two applications so the functionality was rehosted
  successfully
* The significant slowdown causes this example to be very time dependent on its surroundings which is not what we want
  from a rehosting point of view because it makes rehosting firmware in a complex environment very unreliable and
  cumbersome
* Potentially using HAL approach could fix this
    * Same challenges as in the recv-uart example

#### Tracing run

* Command `python irq-recording.py --firmware bins/can/echo-uart.bin --openocd_cfg common/pico-picoprobe.cfg
  --map irq-maps/recv_uart_map.py -I 0x10000744`
* [JSON trace](traces/echo-uart-irq_trace-2023-07-21_17-56-24.json)

__Echo-USB output:__

```============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)

Tick 0

Tick 1

Tick 2

Tick 3

Tick 4
CAN successfully sent: rc=0, data=0

Tick 0
0x004 01000000 (1954092458)
Updated counter to 2
CAN successfully sent: rc=0, data=2

Tick 2
0x004 03000000 (1956597915)
Updated counter to 4
CAN successfully sent: rc=0, data=4

Tick 4
0x004 05000000 (1959103388)
Updated counter to 6
CAN successfully sent: rc=0, data=6

Tick 6
0x004 07000000 (1961608859)
Updated counter to 8
CAN successfully sent: rc=0, data=8

Tick 8
0x004 09000000 (1964114337)
Updated counter to 10
CAN successfully sent: rc=0, data=10

Tick 10
0x004 0b000000 (1966619908)
Updated counter to 12
CAN successfully sent: rc=0, data=12

Tick 12
0x004 0d000000 (1969125641)
Updated counter to 14
CAN successfully sent: rc=0, data=14

Tick 14
0x004 0f000000 (1971631376)
Updated counter to 16
CAN successfully sent: rc=0, data=16

Tick 16

Tick 17
0x004 11000000 (1974137110)
Updated counter to 18
CAN successfully sent: rc=0, data=18

Tick 18
0x004 13000000 (1976642847)
Updated counter to 20
CAN successfully sent: rc=0, data=20

Tick 20
0x004 15000000 (1979148581)
Updated counter to 22
CAN successfully sent: rc=0, data=22

```

__Echo-UART output:__

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)

Tick 0

Tick 1

Tick 2

Tick 3

Tick 4
0x004 00000000 (1954878644)
Updated counter to 1
CAN successfully sent: rc=0, data=1

Tick 1
0x004 02000000 (1957379406)
Updated counter to 3
CAN successfully sent: rc=0, data=3

Tick 3
0x004 04000000 (1959880148)
Updated counter to 5
CAN successfully sent: rc=0, data=5

Tick 5
0x004 06000000 (1962380800)
Updated counter to 7
CAN successfully sent: rc=0, data=7

Tick 7
0x004 08000000 (1964881528)
Updated counter to 9
CAN successfully sent: rc=0, data=9

Tick 9
0x004 0a000000 (1967382178)
Updated counter to 11
CAN successfully sent: rc=0, data=11

Tick 11
0x004 0c000000 (1969882892)
Updated counter to 13
CAN successfully sent: rc=0, data=13

Tick 13
0x004 0e000000 (1972383562)
Updated counter to 15
CAN successfully sent: rc=0, data=15

Tick 15
0x004 10000000 (1974884202)
Updated counter to 17
CAN successfully sent: rc=0, data=17

Tick 17
0x004 12000000 (1979884889)
Updated counter to 19
CAN successfully sent: rc=0, data=19

Tick 19
0x004 14000000 (1982385533)
Updated counter to 21
CAN successfully sent: rc=0, data=21

```

#### Rehosted run

* Command `python irq_echo_uart.py`
* Note: Very time sensitive, the USB pico needs to be held in reset until the UART pico has booted and the interrupt
  plugin has finished initializing
* [JSON Logfile](traces/echo-uart-irq_trace-2023-07-21_17-56-24.json)
* [Logfile](rehosting_echo-uart.log)

__Echo-USB output:__

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)

Tick 0

Tick 1                                                                    
                                                                          
Tick 2                                                                    
0x004 00000000 (231564827)                                                
Updated counter to 1                                                      
CAN successfully sent: rc=0, data=1

Tick 1

Tick 2

Tick 3
0x004 02000000 (239894227)
Updated counter to 3
CAN successfully sent: rc=0, data=3

Tick 3

Tick 4

Tick 5

Tick 6
0x004 04000000 (248518617)
Updated counter to 5
CAN successfully sent: rc=0, data=5

Tick 5

Tick 6
0x004 06000000 (255591495)
Updated counter to 7
CAN successfully sent: rc=0, data=7

Tick 7

Tick 8

Tick 9

Tick 10
0x004 08000000 (263648277)
Updated counter to 9
CAN successfully sent: rc=0, data=9

Tick 9

Tick 10

Tick 11
0x004 0a000000 (271015682)
Updated counter to 11
CAN successfully sent: rc=0, data=11

Tick 11

Tick 12
```

__Rehosted echo-uart output:__

```
============================== Boot complete ==============================
Initializing CAN ...
[init] 0x0
CAN Init finished :)

Tick 0

Tick 1

Tick 2

Tick 3

Tick 4
CAN successfully sent: rc=0, data=0

Tick 0
0x004 01000000 (1962622190)
Updated counter to 2
CAN successfully sent: rc=0, data=2

Tick 2
0x004 03000000 (1970122851)
Updated counter to 4
CAN successfully sent: rc=0, data=4

Tick 4
0x004 05000000 (1980123526)
Updated counter to 6
CAN successfully sent: rc=0, data=6

Tick 6
0x004 07000000 (1985124201)
Updated counter to 8
CAN successfully sent: rc=0, data=8

Tick 8
0x004 09000000 (1995124720)
Updated counter to 10
CAN successfully sent: rc=0, data=10

```