# WiFi rehosting

## Experiments

### PicoW-Blink

#### IRQ Trace
* Command `python irq-recording.py --firmware bins/wifi/picow_blink.bin --openocd_cfg common/pico-picoprobe.cfg --map irq-maps/picow_blink.py -I 0x1000036e`
* [JSON Trace](traces/picow_blink-irq_trace-2023-07-21_14-40-02.json)

##### Serial output
```
Version: 7.95.49 (2271bb6 CY) CRC: b7a28ef3 Date: Mon 2021-11-29 22:50:27 PST Ucode Ver: 1043.2162 FWID 01-c51d940
cyw43 loaded ok, mac 28:cd:c1:0c:dd:7e
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
Pulling LED high through CYW43
Pulling LED low  through CYW43
```

### Server-Counter

#### IRQ Trace
* Command `python irq-recording.py --firmware bins/wifi/server_counter.bin --openocd_cfg common/pico-picoprobe.cfg --map irq-maps/server_counter.py -I 0x100007da`
* [JSON Trace](traces/server_counter-irq_trace-2023-07-21_14-10-00.json)


```
Version: 7.95.49 (2271bb6 CY) CRC: b7a28ef3 Date: Mon 2021-11-29 22:50:27 PST Ucode Ver: 1043.2162 FWID 01-c51d940
cyw43 loaded ok, mac 28:cd:c1:0c:dd:7e
API: 12.2
Data: RaspberryPi.PicoW
Compiler: 1.29.4
ClmImport: 1.47.1
Customization: v5 22/06/24
Creation: 2022-06-24 06:55:08

Connecting to Wi-Fi...
connect status: joining
connect status: no ip
connect status: link up
Connected :)

Starting server...
Starting server at 192.168.1.48 on port 80
Running in polling mode.

Client connected from 192.168.1.27 (0x2000BD90)
tcp_server_recv 76 err 0
Received data from client:
> GET / HTTP/1.1
> Host: 192.168.1.48
> User-Agent: curl/7.88.1
> Accept: */*
> 
> 
Writing 184 bytes to client
tcp_server_sent 184/184
Closing connection to 0x2000BD90 bye :)

Client connected from 192.168.1.27 (0x2000BD90)
tcp_server_recv 76 err 0
Received data from client:
> GET / HTTP/1.1
> Host: 192.168.1.48
> User-Agent: curl/7.88.1
> Accept: */*
> 
> 
Writing 184 bytes to client
tcp_server_sent 184/184
Closing connection to 0x2000BD90 bye :)

Client connected from 192.168.1.27 (0x2000BD90)
tcp_server_recv 76 err 0
Received data from client:
> GET / HTTP/1.1
> Host: 192.168.1.48
> User-Agent: curl/7.88.1
> Accept: */*
> 
> 
Writing 184 bytes to client
tcp_server_sent 184/184
Closing connection to 0x2000BD90 bye :)
```