import IPython

import common.pretty_logging as pretty_logging

from avatar2 import *
from avatar2.peripherals import AvatarPeripheral
from common.ipython_startup import *
import logging

from common.util import printVT, getVTOR

VECTOR_TABLE_OFFSET_REG = 0xe000ed08

MAIN_A = 0x100003b8
LOOP_A = 0x1000040c
my_time_callback = 0x10000354
alarm_pool_alarm_callback_p10 = 0x100010e6
add_alarm_under_lock = 0x10000f68
# TIMER_CALLBACK = 0x10001988
GPIO_CALLBACK_RAM = 0x2000023c
HARD_FAULT_ISR = 0x100001c4

IRQ_MAP = {
    0x100001f6: "<_reset_handler>",
    0x100001c0: "<isr_invalid>",
    0x100001c2: "<isr_nmi>",
    0x100001c4: "<isr_hardfault>",
    0x100001c6: "<isr_svcall>",
    0x100001c8: "<isr_pendsv>",
    0x100001ca: "<isr_systick>",
    0x100001cc: "<__unhandled_user_irq>",
    0x100001d2: "<unhandled_user_irq_num_in_r0>",
    0x10001988: "<hardware_alarm_irq_handler>",
    0x10000380: "<gpio_callback>",
    0x2000121a: "STUB"
}


class HWTimerInspectionPeripheral(AvatarPeripheral):
    """AvatarPeripheral to log all accesses to the timer and return the actual values from the hardware"""

    def dispatch_read(self, offset, size):
        if self.hw is not None:
            val = self.hw.read_memory(self.address + offset, size)
            self.log.critical(f"Memory read at 0x{self.address:x} 0x{offset:x} with size {size} and value {val}")
            self.event_log.append((self.event_id, 'read', offset, val))
            self.event_id += 1
            return val
        else:
            return 0x00

    def dispatch_write(self, offset, size, value):

        if self.hw is not None:
            self.log.critical(f"Memory write at 0x{self.address:x} 0x{offset:x} with size {size} and value 0x{value:x}")
            self.event_log.append((self.event_id, 'write', offset, value))
            self.event_id += 1
            return self.hw.protocols.memory.write_memory(self.address + offset, value=value, size=size, num_words=1)
        else:
            self.log.critical(f"Memory write at 0x{self.address:x} 0x{offset:x} failed, NO hardware present!!!")
            return False

    def __init__(self, name, address, size, **kwargs):
        super().__init__(name, address, size)
        self.read_handler[0:size] = self.dispatch_read
        self.write_handler[0:size] = self.dispatch_write
        self.event_log = []
        self.event_id = 0

        self.log = logging.getLogger('emulated')
        self.hw: avatar2.Target | None = None

    def set_hw(self, hw):
        self.hw = hw


def setup_loggers():
    loggerAvatar = logging.getLogger('avatar')
    loggerAvatar.setLevel(logging.INFO)
    pretty_logging.setup(loggerAvatar, logging.INFO)

    logger = logging.getLogger('my')
    logger.setLevel(logging.DEBUG)
    logger = pretty_logging.setup(logger, logging.DEBUG)
    logger.root.handlers.clear()

    emlogger = logging.getLogger('emulated')
    emlogger.setLevel(logging.DEBUG)
    emlogger = pretty_logging.setup(emlogger, logging.DEBUG)
    emlogger.root.handlers.clear()


def main():
    # Configure the location of config and binary files
    firmware = abspath('bins/base/blink_int.bin')
    bootrom = abspath('bins/base/pico_bootrom.bin')
    openocd_config = abspath('common/pico-picoprobe.cfg')

    # Initiate the avatar-object
    avatar = Avatar(arch=ARM_CORTEX_M3,
                    output_directory='/tmp/avatar', log_to_stdout=False)
    setup_loggers()
    loggerAvatar = logging.getLogger('avatar')
    logger = logging.getLogger('my')

    logger.debug("Setting up targets")
    # Create the target-objects
    hardware = avatar.add_target(
        OpenOCDTarget, openocd_script=openocd_config, name='hardware')

    # Create controlled qemu instance
    qemu = avatar.add_target(QemuTarget, gdb_port=1236,
                             entry_address=0x1000000, name='qemu')

    logger.debug("Loading interrupt plugins")
    avatar.load_plugin('arm.armv7m_interrupts')
    avatar.load_plugin('assembler')
    avatar.load_plugin('disassembler')

    # Memory mapping from https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf#_address_map
    rom = avatar.add_memory_range(
        0x00000000, 0x00004000, file=bootrom, name='ROM')  # 16kB ROM with bootloader
    # 2MB Flash, Accessing using XIP with cache
    xip_cached = avatar.add_memory_range(
        0x10000000, 0x00200000, file=firmware, name='Flash (XIP) - cache')

    ram = avatar.add_memory_range(
        0x20000000, 0x00042000, name='RAM')  # 264kB of RAM
    # We will need the apb system registers, esp the timer
    apb_peripherals = avatar.add_memory_range(
        0x40000000, 0x00053000, name='APB-1', forwarded=True, forwarded_to=hardware)
    timer_peripheral = avatar.add_memory_range(0x40054000, 0x4000, name='TIMER', emulate=HWTimerInspectionPeripheral)
    apb_peripherals_2 = avatar.add_memory_range(0x40058000, 0x40070000 - 0x40058000, name='APB-2',
                                                forwarded=True, forwarded_to=hardware)
    # apb_peripherals = avatar.add_memory_range(
    #     0x40000000, 0x40070000, name='APB', forwarded=True, forwarded_to=hardware)
    # To blink the LED we will need the SIO registers (contains GPIO registers)
    # sio_cpuid = avatar.add_memory_range(
    #     0xd0000030, 0x00000004, name='SIO', forwarded=True, forwarded_to=hardware)  # SIO registers
    # sio_gpio = avatar.add_memory_range(0xd0000004, 0x00000004c - 0x4, name='SIO', emulate=MonitorPeripheral)  # Log GPIO
    # sio = avatar.add_memory_range(
    #     0xd0000030, 0x0000017c - 0x4c, name='SIO', forwarded=True, forwarded_to=hardware)  # SIO registers
    sio = avatar.add_memory_range(
        0xd0000000, 0x0000017c - 0x00, name='SIO', forwarded=True, forwarded_to=hardware)  # SIO registers
    # Internal peripherals of the Cortex M0+, esp VTOR
    # arm_peripherals_1 = avatar.add_memory_range(
    #     0xe0000000, VECTOR_TABLE_OFFSET_REG - 0xe0000000, name='Cortex Peripherals', forwarded=True,
    #     forwarded_to=hardware)
    # arm_peripherals_vtor = avatar.add_memory_range(
    #     VECTOR_TABLE_OFFSET_REG, 4, name='Cortex Peripherals-VTOR', emulate=VTORPeripheral)
    # arm_peripherals_vtor = avatar.add_memory_range(
    #     VECTOR_TABLE_OFFSET_REG, 4, name='Cortex Peripherals-VTOR')
    # arm_peripherals_2 = avatar.add_memory_range(
    #     VECTOR_TABLE_OFFSET_REG + 4, 0xe000eda4 - VECTOR_TABLE_OFFSET_REG - 4,
    #     name='Cortex Peripherals-2', forwarded=True, forwarded_to=hardware)
    arm_peripherals = avatar.add_memory_range(0xe0000000, 0x00010000, name='Cortex Peripherals')

    # Initialize the targets
    logger.debug("Initializing targets")
    avatar.init_targets()

    # Set breakpoint right at the start of the loop
    hardware.set_breakpoint(LOOP_A, hardware=True)
    hardware.cont()
    hardware.wait()
    print("breakpoint hit")
    printVT(hardware, IRQ_MAP)
    vt_offset = getVTOR(hardware)

    logger.debug("Syncing targets")
    avatar.transfer_state(hardware, qemu, sync_regs=True, synced_ranges=[ram])

    ###############################################################################################
    # print("\n=================== Dropping in interactive session =========================\n")
    # hwgdb, qegdb = isetup(locals())
    # IPython.embed()
    # Important setup
    # hardware.ivt_address = vt_offset

    def record_interrupt_enter(avatar, message, **kwargs):
        isr = message.interrupt_num
        logger.warning(f">>>>>>>> ENTER {hex(isr)} {message} {kwargs}")

    def record_interrupt_exit(avatar, message, **kwargs):
        isr = message.interrupt_num
        logger.warning(f">>>>>>>> EXIT {hex(isr)} {message} {kwargs}")
        # pprint(get_timer(hardware))

    logger.debug("Registering interrupt handlers")
    avatar.watchmen.add_watchman('RemoteInterruptEnter', 'after',
                                 record_interrupt_enter)
    avatar.watchmen.add_watchman('RemoteInterruptExit', 'after',
                                 record_interrupt_exit)

    logger.debug("Enabling interrupts")
    # NOTE: Here we can edit breakpoints without running into bug hell
    hardware.remove_breakpoint(0)  # Remove our initial breakpoint
    hardware.remove_breakpoint(1)  # Remove our initial breakpoint
    # hardware.set_breakpoint(
    #     0x20001200 + 24)  # TODO: This should not work because the stub should overwrite the breakpoint
    #############################################
    # qemu.set_breakpoint(alarm_pool_alarm_callback_p10, hardware=True)
    # qemu.set_breakpoint(my_time_callback, hardware=True)
    # qemu.set_breakpoint(GPIO_CALLBACK, hardware=True)
    # qemu.set_breakpoint(GPIO_CALLBACK_RAM)
    qemu.set_breakpoint(HARD_FAULT_ISR)  # Break on hardfault

    # qemu.set_breakpoint(add_alarm_under_lock)

    # enabel_dbg(loggerAvatar)
    avatar.transfer_interrupt_state(hardware, qemu)
    avatar.enable_interrupt_forwarding(hardware, qemu)

    timer_p : HWTimerInspectionPeripheral = timer_peripheral.forwarded_to
    timer_p.set_hw(hardware)

    # NOTE: FIXME: HACK: DO NOT TOUCH THE BREAKPOINTS !!! => Will result in weird crash
    # disabel_dbg(loggerAvatar)

    print("\n=================== Finished setting up interrupt recording =========================\n")
    printVT(hardware, IRQ_MAP)

    print("\n=================== Continuing... =========================\n")

    print("\n=================== Dropping in interactive session =========================\n")
    print(f"HARDWARE: 0x{hardware.read_memory(VECTOR_TABLE_OFFSET_REG, 4):08x}")
    print(f"QEmu    : 0x{qemu.read_memory(VECTOR_TABLE_OFFSET_REG, 4):08x}")
    print()

    qemu.cont()
    hardware.cont()

    hwgdb, qegdb = isetup(locals())
    IPython.embed()
    # Executing `qemu.cont(); qemu.wait()` in the ipython shell, will cause the LED
    # to either turn on or off because we step from sleep() to sleep()

    print("=================== Done =========================")
    if hardware.state == TargetStates.RUNNING:
        hardware.stop()
    if qemu.state == TargetStates.RUNNING:
        qemu.stop()
    avatar.shutdown()
    return


if __name__ == '__main__':
    main()
