import datetime

import IPython

from avatar2 import *
from common.ipython_startup import *

import common.util as util
from common.util import printVT, getVTOR

VECTOR_TABLE_OFFSET_REG = 0xe000ed08

HARD_FAULT_ISR = 0x100001c4

IRQ_MAP = {
    0x20041fff: "__END_OF_RAM__",
    0x100001f6: "<_reset_handler>",
    0x100001c0: "<isr_invalid>",
    0x100001c2: "<isr_nmi>",
    HARD_FAULT_ISR: "<isr_hardfault>",
    0x100001c6: "<isr_svcall>",
    0x100001c8: "<isr_pendsv>",
    0x100001ca: "<isr_systick>",
    0x100001cc: "<__unhandled_user_irq>",
    0x100001d2: "<unhandled_user_irq_num_in_r0>",
    0x10001bcc: "<hardware_alarm_irq_handler>",
    0x100051d0: "<low_priority_worker_irq>",
    0x200021d4: "<irq_handler_chain_slots>",
    0x200021ec: "<irq_handler_chain_slots+24>",
}

FIRMWARE = "./bins/send.bin"
OPENOCD_CONFIG = "common/pico-jlink.cfg"
AFTER_INIT_LOC = 0x1000067a


def main():
    # Configure the location of config and binary files
    firmware = abspath(FIRMWARE)
    bootrom = abspath('bins/base/pico_bootrom.bin')
    openocd_config = abspath(OPENOCD_CONFIG)

    # Initiate the avatar-object
    avatar = Avatar(arch=ARM_CORTEX_M3,
                    output_directory='/tmp/avatar', log_to_stdout=False)
    loggerAvatar, logger = util.setup_loggers()

    logger.info("Setting up targets")
    # Create the target-objects
    hardware = avatar.add_target(
        OpenOCDTarget, openocd_script=openocd_config, name='hardware')

    # Memory mapping from https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf#_address_map
    # 16kB ROM with bootloader
    rom = avatar.add_memory_range(
        0x00000000, 0x00004000, file=bootrom, name='ROM')
    # 2MB Flash, Accessing using XIP with cache
    xip_cached = avatar.add_memory_range(
        0x10000000, 0x00200000, file=firmware, name='Flash (XIP) - cache')
    # 264kB of RAM
    ram = avatar.add_memory_range(0x20000000, 0x00042000, name='RAM')

    logger.info("Loading interrupt plugins")
    avatar.load_plugin('arm.armv7m_interupt_recorder')
    avatar.load_plugin('assembler')
    avatar.load_plugin('disassembler')

    # Initialize the targets
    logger.debug("Initializing targets")
    avatar.init_targets()

    # Set breakpoint right at the start of the loop
    hardware.set_breakpoint(AFTER_INIT_LOC, hardware=True)
    hardware.cont()
    hardware.wait()
    print("breakpoint hit")
    printVT(hardware, IRQ_MAP)
    vt_offset = getVTOR(hardware)

    ###############################################################################################
    # print("\n=================== Dropping in interactive session =========================\n")
    # hwgdb, qegdb = isetup(locals())
    # IPython.embed()
    irq_trace = []

    def record_interrupt_enter(avatar, message: TargetInterruptEnterMessage, **kwargs):
        isr = message.interrupt_num
        logger.warning(f">>>>>>>>>>> ENTER IRQ-num={isr}")
        irq_trace.append(
            {'id': message.id, 'event': 'enter', 'isr': isr, 'timestamp': datetime.datetime.now().isoformat()})

    def record_interrupt_exit(avatar, message: TargetInterruptExitMessage, **kwargs):
        isr = message.interrupt_num
        logger.warning(f">>>>>>>>>>> EXIT  IRQ-num={isr}")
        irq_trace.append(
            {'id': message.id, 'event': 'exit', 'isr': isr, 'timestamp': datetime.datetime.now().isoformat()})

    logger.debug("Registering interrupt handlers")
    avatar.watchmen.add_watchman('TargetInterruptEnter', 'after', record_interrupt_enter)
    avatar.watchmen.add_watchman('TargetInterruptExit', 'after', record_interrupt_exit)

    logger.debug("Enabling interrupt recording")
    # NOTE: Here we can edit breakpoints without running into bug hell
    hardware.remove_breakpoint(0)  # Remove our initial breakpoint
    hardware.remove_breakpoint(1)  # Remove our initial breakpoint
    # hardware.set_breakpoint(STUB_LOC)
    hardware.set_breakpoint(HARD_FAULT_ISR)

    avatar.enable_interrupt_recording(hardware)

    print("\n=================== Finished setting up interrupt recording =========================\n")
    STUB_LOC = hardware.protocols.interrupts._monitor_stub_isr
    IRQ_MAP[STUB_LOC] = "STUB"
    printVT(hardware, IRQ_MAP)

    print("\n=================== Starting to record... =========================\n")
    print(f"HARDWARE: 0x{hardware.read_memory(VECTOR_TABLE_OFFSET_REG, 4):08x}")
    print()

    hardware.cont()

    print("\n=================== Dropping in interactive session =========================\n")
    hwgdb, qegdb = isetup(locals())
    IPython.embed()

    print("\n=================== Storing acquired trace =========================\n")
    trace_file = f"logs/traces/irq_trace-{datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.json"
    logger.info(f"Storing trace to {trace_file}")
    util.writeAsJson(irq_trace, trace_file)

    print("=================== Done =========================")
    if hardware.state == TargetStates.RUNNING:
        hardware.stop()
    avatar.shutdown()
    return


if __name__ == '__main__':
    main()
