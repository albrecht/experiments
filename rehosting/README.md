# Rehosting

All scripts in this folder are used to rehost the evaluation experiments of the INTForwarder and HWRunner plugins.

They can all be executed with `python3 <scriptname>`.

## HW Setup
The corresponding binaries are mentioned in the configuration headers of the scripts.
They need to be flashed onto the RPI Pico before rehosting them, for example with OpenOCD
```shell
openocd -f interface/cmsis-dap.cfg -c 'adapter speed 6000' -f target/rp2040.cfg -c "program ./bins/can/echo-uart.elf reset exit"
```

## Requirements
* Python 3
* Avatar2 with INTForwarder and HWRunner, recommended from this fork and branch [https://github.com/albrecht-flo/avatar2/tree/interrupts](https://github.com/albrecht-flo/avatar2/tree/interrupts) 
* QEMU recommended from [here](https://github.com/albrecht-flo/avatar-qemu)

## Structure
* `./bins` -- All the firmware binaries
* `./common` -- Common scripts and peripherals, alongside utilit scripts
* `./irq-maps` -- Vector table mappings for the interrupt traces
* `./logs` -- All the results and logs aqcuired during evaluation

## Tracer
The `irq-recording.py` script is used to acquire the interrupt traces.
It can be executed as follows:
```shell
python irq-recording.py --firmware bins/IWatchdog.ino.bin \
  --openocd_cfg common/stm32-stlink.cfg --map irq-maps/blink_usb_map.py \
  -I 0x8000816 -E bins/IWatchdog.ino.elf --no-save
```