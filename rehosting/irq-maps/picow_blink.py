{
    0x100001f6: "<_reset_handler>",
    0x100001c0: "<isr_invalid>",
    0x100001c2: "<isr_nmi>",
    0x100001c4: "<isr_hardfault>",
    0x100001c6: "<isr_svcall>",
    0x100001c8: "<isr_pendsv>",
    0x100001ca: "<isr_systick>",
    0x100001cc: "<__unhandled_user_irq>",
    0x1000179c: "<hardware_alarm_irq_handler>",
    0x100078f8: "<low_priority_irq_handler> -> <cyw43_do_poll>",
    0x2000057c: "<irq_handler_chain_slots> -> <cyw43_gpio_irq_handler>",
    0x20041fff: "__END_OF_RAM__"
}
