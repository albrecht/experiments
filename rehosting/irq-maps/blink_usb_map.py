{
    0x100001f6: "<_reset_handler>",
    0x100001c0: "<isr_invalid>",
    0x100001c2: "<isr_nmi>",
    0x100001c4: "<isr_hardfault>",
    0x100001c6: "<isr_svcall>",
    0x100001c8: "<isr_pendsv>",
    0x100001ca: "<isr_systick>",
    0x100001cc: "<__unhandled_user_irq>",
    0x100016dc: "<hardware_alarm_irq_handler>",
    0x10004c8c: "<low_priority_worker_irq> -> <tud_task_ext>",
    0x20000afc: "<irq_handler_chain_slots> -> <dcd_rp2040_irq>",
    0x20041fff: "__END_OF_RAM__"
}