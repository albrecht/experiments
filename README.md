# Evaluation for INTForwarder and HWRunner

## Structure
* `./rehosting` -- All the experiment rehosting scripts
* `./blink-pico` -- Basic firmware samples
* `./can-echo` -- CAN firmware samples
* `./pico-wifi` -- Pico W firmware samples
* `./STM32` -- STM32 firmware samples

## Hardware setup
2 CANPicos and 2 Picos with the `picoprobe` firmware wired as follows:

![Wiring setup](./CANPicoWiringSetup.png)

## Software setup
* Python 3
* Avatar2 with INTForwarder and HWRunner, recommended from this fork and branch [https://github.com/albrecht-flo/avatar2/tree/interrupts](https://github.com/albrecht-flo/avatar2/tree/interrupts) 
* [Raspberry Pi Pico C SDK](https://www.raspberrypi.com/documentation/microcontrollers/c_sdk.html)


