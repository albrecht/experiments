#include "pico/stdlib.h"
#include "canapi.h"
#include <stdio.h>

#ifndef PICO_DEFAULT_LED_PIN
#error requires a board with a regular LED
#endif
#define LED_PIN PICO_DEFAULT_LED_PIN

static can_controller_t controller;

// Utility function to print a CAN frame to stdout
void print_frame(can_frame_t *f, uint32_t timestamp) {
    uint8_t len = can_frame_get_data_len(f);
    printf(can_frame_is_extended(f) ? "0x%08x " : "0x%03x ", can_frame_get_arbitration_id(f));
    if (can_frame_is_remote(f)) {
        printf("R"); // Remote frame doesn't have any data, len will be zero
    }
    for (uint32_t i = 0; i < len; i++) {
        printf("%02x", can_frame_get_data(f)[i]);
    }
    printf(" (%d)\n", timestamp);
}

// Wrapper to parameterize the API's IRQ handler with the handle to the single CAN
// controller on the CANPico (the structure is defined above and configured by
// binding to the SPI port and pins used on the CANPico).
//
// NB: The RP2040 with execute-in-place (XIP) flash suffers from very long cache
// miss delays, and these can cause significant delays to the interrupt handler.
// Generally the entire chain of interrupt handling (from vector table to first-level
// handler to device-specific handlers) should be located in RAM to avoid these
// cache delays. The CAN drivers are allocated to RAM with the "TIME_CRITICAL"
// attribute that causes the compiler to place the function in RAM.
//
// This problem of delaying interrupts also extends to critical sections: code that
// disables an interrupt around some function also should execute from RAM while in
// that function to avoid delaying an urgent interrupt handler. This "priority
// inversion" is discussed further here:
//
// https://kentindell.github.io/2021/03/05/pico-priority-inversion/
void TIME_CRITICAL irq_handler(void) {
    // Work out if this interrupt is from the the MCP25xxFD. The bound interface
    // defines the pin used for the interrupt line from the CAN controller.
    uint8_t spi_irq = controller.host_interface.spi_irq;
    uint32_t events = gpio_get_irq_event_mask(spi_irq);
//    printf(">>>> irq-hit 0x%x\n", events);

    if (events & GPIO_IRQ_LEVEL_LOW) {
        mcp25xxfd_irq_handler(&controller);
    }
}

const char *getCANError(can_errorcode_t error) {
    char *errs[] = {
            "CAN_ERC_NO_ERROR",                               // OK
            "CAN_ERC_BAD_BITRATE",                                // Baud rate settings are not legal
            "CAN_ERC_RANGE",                                      // Range error on parameters
            "CAN_ERC_BAD_INIT",                                   // Can't get the controller to initialize
            "CAN_ERC_NO_ROOM",                                    // No room
            "CAN_ERC_NO_ROOM_PRIORITY",                           // No room in the transmit priority queue
            "CAN_ERC_NO_ROOM_FIFO",                               // No room in the transmit FIFO queue
            "CAN_ERC_BAD_WRITE",                                  // Write to a controller register failed
            "CAN_ERC_NO_INTERFACE",                               // No interface binding set for the controller
    };
    if (error < 9)
        return errs[error];
    else
        return "NO CAN ERROR!";
}

void init() {
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);


    // This Pico SDK call binds the GPIO vector to calling the CAN controller ISR. If there are
    // other devices connected to GPIO interrupts (e.g. the WiFi chip on the Pico W)
    // then the appropriate handler has to be called.
    irq_add_shared_handler(IO_IRQ_BANK0, irq_handler, PICO_SHARED_IRQ_HANDLER_DEFAULT_ORDER_PRIORITY);

    // Example uses 500Kbit/sec, 75% sample point
    can_bitrate_t bitrate = {
            .profile = CAN_BITRATE_500K_75,
            .brp = -1,
            .tseg1 = 10,
            .tseg2 = 3,
            .sjw = 2,
    };

    // Bind the Pico SPI interface to the CANPico's pin layout
    mcp25xxfd_spi_bind_canpico(&controller.host_interface);

    can_id_filter_t filters[CAN_MAX_ID_FILTERS];
    can_id_filters_t all_filters = {.filter_list = filters, .n_filters = CAN_MAX_ID_FILTERS};

    can_options_t options = CAN_OPTION_RECV_ERRORS | CAN_OPTION_RECORD_TX_EVENTS;
    while (true) {
        can_errorcode_t rc = can_setup_controller(&controller, &bitrate, &all_filters, CAN_MODE_NORMAL, options);
        if (rc != CAN_ERC_NO_ERROR) {
            // This can fail if the CAN transceiver isn't powered up properly. That might happen
            // if the board had 3.3V but not 5V (the transceiver needs 5V to operate).
            printf("CAN setup error: %s (%d)\n", getCANError(rc), rc);
            // Try again after 1 second
            busy_wait_ms(1000);
        } else {
            break;
        }
    }

    can_status_t status = can_get_status(&controller);
    printf("[init] 0x%lx\n", status.status);
}

bool send(const uint8_t *data, size_t dataSize) {
    can_frame_t my_tx_frame;
    can_make_frame(&my_tx_frame, false, 0x04, dataSize, data, false);

    can_errorcode_t rc = can_send_frame(&controller, &my_tx_frame, false);
    if (rc != CAN_ERC_NO_ERROR) {
        // This can happen if there is no room in the transmit queue, which can
        // happen if the CAN controller is connected to a CAN bus but there are no
        // other CAN controllers connected and able to ACK a CAN frame, so the
        // transmit queue fills up and then cannot accept any more frames.
        printf("CAN send error: %s (%d), data=%lu\n", getCANError(rc), rc, *((uint32_t *) data));
        return false;
    } else {
        printf("CAN successfully sent: rc=%d, data=%lu\n", rc, *((uint32_t *) data));
        return true;
    }
}

int main() {
    stdio_init_all();
    while(!stdio_usb_connected()) {
        sleep_ms(100);
    }
    printf("============================== Boot complete ==============================\n");
    printf("Initializing CAN ...\n");
    init();
    printf("CAN Init finished :)\n");
    gpio_put(LED_PIN, 1);
    sleep_ms(500);

    uint32_t counter = 0;
    while (true) {
        gpio_put(LED_PIN, counter & 0x1);
        printf("Tick %lu\n", counter);
        counter++;
        send((const uint8_t *) &counter, sizeof(counter));

        // Check for errors
        can_rx_event_t event;
        int n = 0;
        while (can_recv(&controller, &event) && n < 10) {
            n++;
            if (event.event_type == CAN_EVENT_TYPE_CAN_ERROR) {
                if (can_error_is_ack(&event.event.error)) {
                    printf("[WARNING] package was not acknowledged (ACK Error)\n");
                } else if (event.event_type == CAN_EVENT_TYPE_OVERFLOW) {
                    printf("[WARNING] FIFO overflow\n");
                } else {
                    printf("[ERROR] %lx\n", event.event.error.details);
                }
            } else if (can_event_is_frame(&event)) {
                can_frame_t *f = can_event_get_frame(&event);
                print_frame(f, event.timestamp);
            } else {
                printf("Received unknown event (%d)\n", event.event_type);
            }
        }
        sleep_ms(10000);
    }
    return 0;
}
