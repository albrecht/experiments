# Pico CAN firmware samples

These are the example firmware samples to use the CAN controller of the CANPico board.

## Structure
* `echo-uart` -- Full CAN example, running a ping pong protocol
* `echo-usb` -- USB Variant of `echo` example
* `recv-uart` -- CAN example isolating the receive stage of the CAN controller
* `recv-usb` -- USB Variant of `recv` example
* `send-uart` -- CAN example isolating the sending stage of the CAN controller
* `send-usb` -- USB Variant of `send` example

## To Build

### Requirements:

* [Raspberry Pi Pico SDK](https://github.com/raspberrypi/pico-sdk) 
* Note: sometimes `libstdc++-arm-none-eabi-newlib` needs to be installed additionally

### Build instructions

```shell
$ mkdir build
$ cd build
build/ $ cmake ..
build/ $ make
```