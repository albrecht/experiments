#include "pico/stdlib.h"
#include "canapi.h"
#include <stdio.h>

#ifndef PICO_DEFAULT_LED_PIN
#error requires a board with a regular LED
#endif
#define LED_PIN PICO_DEFAULT_LED_PIN


const char *getCANError(can_errorcode_t error) {
    char *errs[] = {
            "CAN_ERC_NO_ERROR",                               // OK
            "CAN_ERC_BAD_BITRATE",                                // Baud rate settings are not legal
            "CAN_ERC_RANGE",                                      // Range error on parameters
            "CAN_ERC_BAD_INIT",                                   // Can't get the controller to initialize
            "CAN_ERC_NO_ROOM",                                    // No room
            "CAN_ERC_NO_ROOM_PRIORITY",                           // No room in the transmit priority queue
            "CAN_ERC_NO_ROOM_FIFO",                               // No room in the transmit FIFO queue
            "CAN_ERC_BAD_WRITE",                                  // Write to a controller register failed
            "CAN_ERC_NO_INTERFACE",                               // No interface binding set for the controller
    };
    if (error < 9)
        return errs[error];
    else
        return "NO CAN ERROR!";
}

// Utility function to print a CAN frame to stdout
void print_frame(can_frame_t *f, uint32_t timestamp) {
    uint8_t len = can_frame_get_data_len(f);
    printf(can_frame_is_extended(f) ? "0x%08lx " : "0x%03lx ", can_frame_get_arbitration_id(f));
    if (can_frame_is_remote(f)) {
        printf("R"); // Remote frame doesn't have any data, len will be zero
    }
    for (uint32_t i = 0; i < len; i++) {
        printf("%02x", can_frame_get_data(f)[i]);
    }
    printf(" (%lu)\n", timestamp);
}

// ================================================================================================
static can_controller_t controller;


// Wrapper to parameterize the API's IRQ handler with the handle to the single CAN
// controller on the CANPico (the structure is defined above and configured by
// binding to the SPI port and pins used on the CANPico).
//
// NB: The RP2040 with execute-in-place (XIP) flash suffers from very long cache
// miss delays, and these can cause significant delays to the interrupt handler.
// Generally the entire chain of interrupt handling (from vector table to first-level
// handler to device-specific handlers) should be located in RAM to avoid these
// cache delays. The CAN drivers are allocated to RAM with the "TIME_CRITICAL"
// attribute that causes the compiler to place the function in RAM.
//
// This problem of delaying interrupts also extends to critical sections: code that
// disables an interrupt around some function also should execute from RAM while in
// that function to avoid delaying an urgent interrupt handler. This "priority
// inversion" is discussed further here:
//
// https://kentindell.github.io/2021/03/05/pico-priority-inversion/
void TIME_CRITICAL irq_handler(void) {
    // Work out if this interrupt is from the the MCP25xxFD. The bound interface
    // defines the pin used for the interrupt line from the CAN controller.
    uint8_t spi_irq = controller.host_interface.spi_irq;
    uint32_t events = gpio_get_irq_event_mask(spi_irq);

    printf(">>>> irq-hit 0x%x\n", events);
    if (events & GPIO_IRQ_LEVEL_LOW) {
        mcp25xxfd_irq_handler(&controller);
    }
}

void init() {
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);


    // This Pico SDK call binds the GPIO vector to calling the CAN controller ISR. If there are
    // other devices connected to GPIO interrupts (e.g. the WiFi chip on the Pico W)
    // then the appropriate handler has to be called.
    irq_add_shared_handler(IO_IRQ_BANK0, irq_handler, PICO_SHARED_IRQ_HANDLER_DEFAULT_ORDER_PRIORITY);

    can_errorcode_t rc;

    // Example uses 500Kbit/sec, 75% sample point
    can_bitrate_t bitrate = {
            .profile = CAN_BITRATE_500K_75,
            .brp = 0,
            .tseg1 = 10,
            .tseg2 = 3,
            .sjw = 2
    };

    // Bind the Pico SPI interface to the CANPico's pin layout
    mcp25xxfd_spi_bind_canpico(&controller.host_interface);

    while (true) {
        rc = can_setup_controller(&controller, &bitrate, CAN_NO_FILTERS, CAN_MODE_NORMAL,
                                  CAN_OPTION_RECV_ERRORS | CAN_OPTION_HARD_RESET);
        if (rc != CAN_ERC_NO_ERROR) {
            // This can fail if the CAN transceiver isn't powered up properly. That might happen
            // if the board had 3.3V but not 5V (the transceiver needs 5V to operate).
            printf("CAN setup error: %s (%d)\n", getCANError(rc), rc);
            // Try again after 1 second
            busy_wait_ms(1000);
        } else {
            break;
        }
    }
}

int main() {
    stdio_init_all();
    while(!stdio_usb_connected()) {
        sleep_ms(100);
    }
    printf("============================== Boot complete ==============================\n");
    printf("Initializing CAN ...\n");
    init();
    printf("CAN Init finished :)\n");

    // Create a CAN frame with 11-bit ID of 0x123 and 5 byte payload of deadbeef00
    // Send my counter and wait for response
    bool led = 0;
    while (true) {
        // Reply with recv+1
        gpio_put(LED_PIN, led);

        // Print up to 10 received frames
        can_rx_event_t rx_event;
        uint32_t n = 0;
        while (n < 10) {
            can_rx_event_t *e = &rx_event;
            if (can_recv(&controller, e)) {
                printf("Received Event %d \n", e->event_type);
                if (can_event_is_frame(e)) {
                    can_frame_t *f = can_event_get_frame(e);
                    print_frame(f, e->timestamp);
                }
            }
            n++;
        }
        sleep_ms(100);
        led = !led;
    }
    return 0;
}
