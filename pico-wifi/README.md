# Pico-Wifi firmware samples

These are the example firmware samples for the Raspberry Pi W using the CYW43439 WiFi chip.

## Structure
* `blink` -- Standard blink example, the onboard LED is wired to the CYW43439, so the whole WiFi chip needs to operate propperly to blink the LED
* `server-counter` -- More elaborate HTTP server example

## To Build

### Requirements:

* [Raspberry Pi Pico SDK](https://github.com/raspberrypi/pico-sdk) 
* Note: sometimes `libstdc++-arm-none-eabi-newlib` needs to be installed additionally

### Build instructions

```shell
$ mkdir build
$ cd build
build/ $ cmake ..
build/ $ make
```