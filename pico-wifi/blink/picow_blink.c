#include "pico/stdlib.h"
#include <stdio.h>
#include "pico/cyw43_arch.h"

int main() {
    stdio_init_all();
    printf("============================== Boot complete ==============================\n");
    printf("Initializing Wi-Fi...\n");
    if (cyw43_arch_init()) {
        printf("Wi-Fi init failed");
        return -1;
    }
    cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
    printf("Initialized Wi-Fi!\n\n");

    int led = 0;
    while (true) {
        printf("Pulling LED to %d through CYW43\n", led);
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, led);
        led = (led == 0) ? 1 : 0;
        sleep_ms(1000);
    }
}