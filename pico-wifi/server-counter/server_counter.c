#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"
#include "lwip/pbuf.h"
#include "lwip/tcp.h"

#define DEBUG_printf printf

#define BUF_SIZE 2048

#define TCP_PORT 80
#define POLL_TIME_S 2

typedef struct {
    struct tcp_pcb *server_pcb;
    bool complete;
} SERVER_T;

static SERVER_T *newServer(void) {
    SERVER_T *server = calloc(1, sizeof(SERVER_T));
    memset(server, 0, sizeof(SERVER_T));
    if (!server) {
        printf("[ERROR] failed to allocate SERVER_T\n");
        return NULL;
    }
    return server;
}

typedef struct {
    struct tcp_pcb *pcb;
    uint32_t sent_len;
    uint32_t expected_len;
} CLIENT_CONNECTION_T;

static CLIENT_CONNECTION_T *newClientConnection(struct tcp_pcb *client_pcb) {
    CLIENT_CONNECTION_T *client = calloc(1, sizeof(CLIENT_CONNECTION_T));
    if (!client) {
        printf("[ERROR] failed to allocate CLIENT_CONNECTION_T\n");
        return NULL;
    }
    memset(client, 0, sizeof(CLIENT_CONNECTION_T));
    client->pcb = client_pcb;
    return client;
}

// =====================================================================================================================

static err_t tcp_client_close(CLIENT_CONNECTION_T *client) {
    err_t err = ERR_OK;
    if (client == NULL)
        return err;
    DEBUG_printf("Closing connection to 0x%p bye :)\n", client);

    if (client->pcb != NULL) {
        tcp_arg(client->pcb, NULL);
        tcp_poll(client->pcb, NULL, 0);
        tcp_sent(client->pcb, NULL);
        tcp_recv(client->pcb, NULL);
        tcp_err(client->pcb, NULL);
        err = tcp_close(client->pcb);
        if (err != ERR_OK) {
            DEBUG_printf("[ERROR] Close failed %d, calling abort\n", err);
            tcp_abort(client->pcb);
            err = ERR_ABRT;
        }
        client->pcb = NULL;
    }
    free(client);
    return err;
}

err_t tcp_server_send_data(CLIENT_CONNECTION_T *client, struct tcp_pcb *tpcb) {
    const char *responseHeaders = "HTTP/1.1 200 OK\nContent-Length: %d\nContent-Type: text/html; charset=utf-8\nConnection: close\n\n";
    const char *response = "<!DOCTYPE html><html><head><title>Test page</title></head><body>Hello World!</body></html>\n";

    char send_buffer[BUF_SIZE];
    int size = snprintf(send_buffer, BUF_SIZE, responseHeaders, strlen(response));
    size += snprintf(send_buffer + size, BUF_SIZE - size, response);
    client->expected_len = size;
    client->sent_len = 0;

    DEBUG_printf("Writing %u bytes to client\n", client->expected_len);
    // this method is callback from lwIP, so cyw43_arch_lwip_begin is not required, however you
    // can use this method to cause an assertion in debug mode, if this method is called when
    // cyw43_arch_lwip_begin IS needed
    cyw43_arch_lwip_check();
    err_t err = tcp_write(tpcb, send_buffer, client->expected_len, TCP_WRITE_FLAG_COPY);
    if (err != ERR_OK) {
        DEBUG_printf("[ERROR] Failed to write data %d\n", err);
        return tcp_client_close(client);
    }
    return ERR_OK;
}

err_t tcp_server_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err) {
    CLIENT_CONNECTION_T *client = arg;
    if (!p) {  // Client closed the connection
        return tcp_client_close(client);
    }
    // this method is callback from lwIP, so cyw43_arch_lwip_begin is not required, however you
    // can use this method to cause an assertion in debug mode, if this method is called when
    // cyw43_arch_lwip_begin IS needed
    cyw43_arch_lwip_check();
    if (p->tot_len > 0) {
        DEBUG_printf("tcp_server_recv %d err %d\n", p->tot_len, err);

        // Receive the buffer
        char buffer[BUF_SIZE];
        int recv_len = pbuf_copy_partial(p, buffer,
                                         p->tot_len > BUF_SIZE ? BUF_SIZE : p->tot_len, 0);
        if (recv_len == 0) {
            printf("[ERROR] Failed to read response");
            pbuf_free(p);
            return tcp_client_close(client);
        }
        tcp_recved(tpcb, p->tot_len);
        buffer[recv_len >= BUF_SIZE ? BUF_SIZE - 1 : recv_len] = 0; // ensure 0 termination

        printf("Received data from client:\n", buffer);
        char *current_line = buffer;
        for (int i = 0; i < recv_len; ++i) {
            if (buffer[i] == '\n') {
                buffer[i] = 0;
                printf("> %s\n", current_line);
                current_line = &buffer[i + 1];
            }
        }
        printf("> %s\n", current_line);
    }
    pbuf_free(p);
    // Send response
    return tcp_server_send_data(client, tpcb);
}


static err_t tcp_server_sent(void *arg, struct tcp_pcb *_, u16_t len) {
    CLIENT_CONNECTION_T *client = arg;
    client->sent_len += len;
    DEBUG_printf("tcp_server_sent %u/%u\n", client->sent_len, client->expected_len);
    return ERR_OK;
}

static err_t tcp_server_poll(void *arg, struct tcp_pcb *_) {
    CLIENT_CONNECTION_T *client = arg;
    DEBUG_printf("[WARNING] tcp_server_poll_fn\n");
    return tcp_client_close(client);
}

static void tcp_server_err(void *arg, err_t err) {
    CLIENT_CONNECTION_T *client = arg;
    if (err != ERR_ABRT) {
        DEBUG_printf("[WARNING] tcp_client_err_fn %d\n", err);
        tcp_client_close(client);
    }
}

static void tcp_server_close(SERVER_T *server, int err) {
    DEBUG_printf("[WARNING] Server closing err=%d\n", err);
    if (server->server_pcb != NULL) {
        tcp_close(server->server_pcb);
        server->server_pcb = NULL;
    }
    server->complete = true;
}

static err_t tcp_server_accept(void *arg, struct tcp_pcb *client_pcb, err_t err) {
    SERVER_T *state = (SERVER_T *) arg;
    if (err != ERR_OK || client_pcb == NULL) {
        DEBUG_printf("[ERROR] Failure in accept errno=%d\n", err);
        tcp_server_close(state, err);
        return ERR_VAL;
    }
    CLIENT_CONNECTION_T *client_connection = newClientConnection(client_pcb);
    DEBUG_printf("\nClient connected from %s (0x%p)\n", ip4addr_ntoa(&client_pcb->remote_ip), client_connection);

    tcp_arg(client_pcb, client_connection);
    tcp_sent(client_pcb, tcp_server_sent);
    tcp_recv(client_pcb, tcp_server_recv);
    tcp_poll(client_pcb, tcp_server_poll, POLL_TIME_S * 2);
    tcp_err(client_pcb, tcp_server_err);

    return ERR_OK;
}

static bool tcp_server_open(SERVER_T *state) {
    DEBUG_printf("Starting server at %s on port %u\n", ip4addr_ntoa(netif_ip4_addr(netif_list)), TCP_PORT);

    struct tcp_pcb *pcb = tcp_new_ip_type(IPADDR_TYPE_ANY);
    if (!pcb) {
        DEBUG_printf("[ERROR] Failed to create pcb\n");
        return false;
    }

    err_t err = tcp_bind(pcb, NULL, TCP_PORT);
    if (err) {
        DEBUG_printf("[ERROR] Failed to bind to port %u\n", TCP_PORT);
        return false;
    }

    state->server_pcb = tcp_listen_with_backlog(pcb, 1);
    if (!state->server_pcb) {
        DEBUG_printf("[ERROR] Failed to listen\n");
        tcp_close(pcb);
        return false;
    }

    tcp_arg(state->server_pcb, state);
    tcp_accept(state->server_pcb, tcp_server_accept);

    return true;
}


void run_server() {
    SERVER_T *server = newServer();
    if (!server) {
        return;
    }
    if (!tcp_server_open(server)) {
        tcp_server_close(server, -1);
        return;
    }

#if PICO_CYW43_ARCH_POLL
        DEBUG_printf("Running in polling mode.\n");
#else
    DEBUG_printf("Running in async mode.\n");
#endif
    bool led = 0;
    while (!server->complete) {
        // At top to make this loop easier to find in the assembly
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, led);
        led = !led;
        // the following #ifdef is only here so this same example can be used in multiple modes;
        // you do not need it in your code
#if PICO_CYW43_ARCH_POLL
        // if you are using pico_cyw43_arch_poll, then you must poll periodically from your
        // main loop (not from a timer) to check for Wi-Fi driver or lwIP work that needs to be done.
        cyw43_arch_poll();
        // you can poll as often as you like, however if you have nothing else to do you can
        // choose to sleep until either a specified time, or cyw43_arch_poll() has work to do:
        cyw43_arch_wait_for_work_until(make_timeout_time_ms(1000));
#else
        // if you are not using pico_cyw43_arch_poll, then WiFI driver and lwIP work
        // is done via interrupt in the background. This sleep is just an example of some (blocking)
        // work you might be doing.
        sleep_ms(1000);
#endif

    }
    free(server);
}


int main() {
    stdio_init_all();
    printf("Initializing Wi-Fi...\n");
    if (cyw43_arch_init()) {
        printf("Wi-Fi init failed");
        return -1;
    }
    printf("Initialized Wi-Fi!\n\n");
    cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);

    cyw43_arch_enable_sta_mode();

    printf("Connecting to Wi-Fi...\n");
    if (cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASSWORD, CYW43_AUTH_WPA2_AES_PSK, 30000)) {
        printf("[ERROR] Failed to connect.\n");
        return 1;
    } else {
        printf("Connected :)\n\n");
    }

    printf("Starting server...\n");
    run_server();
    printf("Server exited, closing down.\n");

    cyw43_arch_deinit();
}