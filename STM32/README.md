# STM32 experiment

This example is compiled with the arduino IDE.

It requires the STM32 Nucleo board setup for the arduino IDE, following [this tutorial](https://www.instructables.com/Quick-Start-to-STM-Nucleo-on-Arduino-IDE/)

The example has been developed and tested on the Nucleo-152RE board.